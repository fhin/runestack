﻿using RuneComp;
using System;
using System.Collections.Generic;
using Xunit;

namespace RuneTests
{
    public class SqlBuilderPredictionTests
    {
        Dictionary<string, string> propDict;
        Dictionary<string, string> tableMapping;
        SqlBuilder sqlBuilder;

        public SqlBuilderPredictionTests()
        {
            propDict = new Dictionary<string, string>
            {
                { "name", "item_name" },
                { "item id", "item_id" },
                { "icon", "icon" },
                { "description", "description" },
                { "price", "current_ge_price" },
                { "price change", "current_price_change" },
                { "high alch val", "high_alch_val" },
                { "low alch val", "low_alch_val" },
                { "const price", "const_price" },
                { "buying limit", "buying_limit" }
            };

            tableMapping = new Dictionary<string, string>
            {
                { "items", "items" },
                { "price regression models", "price_regression_models" }
            };
            sqlBuilder = new SqlBuilder("", propDict, tableMapping, '*');
        }

        [Fact]
        public void Query_PredictionForPast()
        {
            DateTime baseDate = DateTime.Now;
            DateTime timeToTest = baseDate.AddHours(-1);
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    FutureDate = timeToTest
                }
            };
            q.Props.Add(p);
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));

            timeToTest = baseDate.AddDays(-1);
            q.Props[0].Prediction.FutureDate = timeToTest;
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));

            timeToTest = baseDate.AddMonths(-1);
            q.Props[0].Prediction.FutureDate = timeToTest;
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Fact]
        public void Query_PredictionNoTableMapping()
        {
            if (!propDict.ContainsKey("item id"))
            {
                propDict.Add("item id", "item_id");
            }
            if (tableMapping.ContainsKey("price regression models"))
            {
                tableMapping.Remove("price regression models");
            }
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    FutureDate = DateTime.Now
                }
            };
            q.Props.Add(p);
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));
            tableMapping.Add("price regression models", "price_regression_models");
        }

        [Fact]
        public void Query_PredictionNoIdMapping()
        {
            if (!tableMapping.ContainsKey("price regression models"))
            {
                tableMapping.Add("price regression models", "price_regression_models");
            }
            string oldItemIdKey = "";
            if (propDict.ContainsKey("item id"))
            {
                oldItemIdKey = propDict["item id"];
                propDict.Remove("item id");
            }

            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    FutureDate = DateTime.Now
                }
            };
            q.Props.Add(p);
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));
            if (oldItemIdKey.Equals(""))
            {
                oldItemIdKey = "item_id";
            }
            propDict.Add("item id", oldItemIdKey);
        }

        [Theory]
        [InlineData("price")]
        public void Query_OnePredictionDate_NoFilter(string propertyName)
        {
            int numExpQueries = 2;
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = propertyName,
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    FutureDate = DateTime.Now
                }
            };
            q.Props.Add(p);
            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);

            Assert.Equal(numExpQueries, exPlan.Queries.Count);
            RuneSqlQuery predicitonQuery = exPlan.Queries[1];
            Assert.NotNull(predicitonQuery.SubstitutedProperty);
            Assert.Equal(p, predicitonQuery.SubstitutedProperty);

            string[] queryParamNames = new string[2] { "predictable_property_id", propDict["item id"] };
            (Type, object)[] expectedParamVals = new (Type, object)[2] {
                (typeof(int), -1),
                (typeof(List<int>), new List<int>())
            };
            Assert.True(CheckPrediction(p, new List<Prediction>() { p.Prediction }, predicitonQuery));
            Assert.True(CheckQuery(predicitonQuery, true, new string[0], true, queryParamNames, expectedParamVals));

            string expectedStmt = BuildExpectedQueryStmt(new Property[0], new string[2] { "predictable_property_id", propDict["item id"] },
                new (Operation, bool)[2] { (Operation.Equal, true), (Operation.Equal, true) }, true);
            string actualStmt = predicitonQuery.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);
        }

        [Fact]
        public void Query_OnePredictionInvalidTimeUnit()
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TUnit = PredictionTimeUnit.None,
                    TValue = 2
                }
            };
            q.Props.Add(p);
            Assert.Throws<ArgumentException>(() => sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Theory]
        [InlineData("price", 0, PredictionTimeUnit.Hour)]
        [InlineData("price", 2, PredictionTimeUnit.Hour)]
        [InlineData("price", 10, PredictionTimeUnit.Hour)]
        [InlineData("price", 0, PredictionTimeUnit.Day)]
        [InlineData("price", 2, PredictionTimeUnit.Day)]
        [InlineData("price", 10, PredictionTimeUnit.Day)]
        [InlineData("price", 0, PredictionTimeUnit.Month)]
        [InlineData("price", 2, PredictionTimeUnit.Month)]
        [InlineData("price", 10, PredictionTimeUnit.Month)]
        public void Query_OnePrediction_NoFilter(string propertyName, int timeValue, PredictionTimeUnit timeUnit)
        {
            int expNumQueries = 2;
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = propertyName,
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TValue = timeValue,
                    TUnit = timeUnit
                }
            };
            q.Props.Add(p);

            string[] queryParamNames = new string[2] { "predictable_property_id", propDict["item id"] };
            (Type, object)[] expectedParamVals = new (Type, object)[2]
            {
                (typeof(int), -1),
                (typeof(List<int>), new List<int>())
            };
            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Equal(expNumQueries, exPlan.Queries.Count);

            RuneSqlQuery iconQuery = exPlan.Queries[0];
            Assert.True(CheckQuery(iconQuery, false, new string[] { propDict["icon"], propDict["item id"] }, false, new string[0]
                , new (Type, object)[0]));

            RuneSqlQuery query = exPlan.Queries[1];
            Assert.True(CheckQuery(query, true, new string[0], true, queryParamNames, expectedParamVals));
            p.PropertyName = propertyName;
            IList<Prediction> expPredictions = new List<Prediction>() { p.Prediction };
            Assert.True(CheckPrediction(p, expPredictions, query));

            string expectedStmt = BuildExpectedQueryStmt(new Property[0], queryParamNames, new (Operation, bool)[2]{
                (Operation.Equal, true), (Operation.Equal, true)
            }, true);
            string actualStmt = query.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);
        }

        [Theory]
        [InlineData(new string[] { "name", "price" }, new bool[] { false, true }, PredictionTimeUnit.Hour, 2, 1)]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false }, PredictionTimeUnit.Hour, 2, 0)]
        [InlineData(new string[] { "name", "price" }, new bool[] { false, true }, PredictionTimeUnit.Day, 2, 1)]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false }, PredictionTimeUnit.Day, 2, 0)]
        [InlineData(new string[] { "name", "price" }, new bool[] { false, true }, PredictionTimeUnit.Month, 2, 1)]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false }, PredictionTimeUnit.Month, 2, 0)]
        [InlineData(new string[] { "name", "description", "price", "buying limit" }, new bool[] { false, false, true, true }, PredictionTimeUnit.Hour, 2, 2)]
        [InlineData(new string[] { "name", "description", "price", "low alch val" }, new bool[] { false, false, true, true }, PredictionTimeUnit.Day, 2, 2)]
        [InlineData(new string[] { "name", "description", "price", "high alch val" }, new bool[] { false, false, true, true }, PredictionTimeUnit.Month, 2, 2)]
        public void Query_OnePrediction_OneNormalProp(string[] propertyNames, bool[] isNumeric, PredictionTimeUnit timeUnit
            , int timeValue, int predictionPropIdx)
        {
            string[] expectedProps = new string[propertyNames.Length + 1];
            expectedProps[0] = propDict["icon"];
            expectedProps[1] = propDict["item id"];

            Property[] props = new Property[propertyNames.Length + 1];
            props[0] = new Property() { IsNumeric = false, PropertyName = propDict["icon"] };
            props[1] = new Property() { IsNumeric = true, PropertyName = propDict["item id"] };
            Query q = new Query(QueryOp.show);
            int currPropIdx = 2;
            Property predictionProp = null;
            for (int i = 0; i < propertyNames.Length; i++)
            {
                Property prop = new Property
                {
                    PropertyName = propDict[propertyNames[i]],
                    IsNumeric = isNumeric[i],
                    Prediction = null
                };
                if (i != predictionPropIdx)
                {
                    expectedProps[currPropIdx] = prop.ToString();
                    prop.PropertyName = propertyNames[i];
                    props[currPropIdx] = prop;
                    currPropIdx++;
                }
                else
                {
                    prop.PropertyName = propertyNames[i];
                    prop.Prediction = prop.Prediction = new Prediction()
                    {
                        TValue = timeValue,
                        TUnit = timeUnit
                    };
                    predictionProp = prop;
                }
                q.Props.Add(prop);
            }
            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            RuneSqlQuery query = exPlan.Queries[0];
            Assert.True(CheckQuery(query, false, expectedProps, true, new string[0],
                new (Type, object)[0]));

            string expectedStmt = BuildExpectedQueryStmt(props, new string[0], new (Operation, bool)[0], false);
            string actualStmt = query.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);

            Assert.Equal(2, exPlan.Queries.Count);
            RuneSqlQuery predictionQuery = exPlan.Queries[1];

            string[] expectedQueryParams = new string[] { "predictable_property_id", propDict["item id"] };
            (Type, object)[] expectedQueryValues = new (Type, object)[]
            {
                (typeof(int), -1),
                (typeof(List<int>), new List<int>())
            };
            Assert.True(CheckQuery(predictionQuery, true, new string[0], true, expectedQueryParams,
                expectedQueryValues));

            expectedStmt = BuildExpectedQueryStmt(new Property[0], expectedQueryParams,
                new (Operation, bool)[2] { (Operation.Equal, true), (Operation.Equal, true) }, true);
            actualStmt = predictionQuery.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);
        }

        [Theory]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false },
            PredictionTimeUnit.Hour, 2, 0, "name", false, ConditionValType.String,
            Operation.Equal, "Rune bandana")]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false },
            PredictionTimeUnit.Hour, 2, 0, "name", false, ConditionValType.String,
            Operation.Starting_With, "Rune*")]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false },
            PredictionTimeUnit.Hour, 2, 0, "name", false, ConditionValType.String,
            Operation.Ending_With, "*bandana")]
        [InlineData(new string[] { "price", "name" }, new bool[] { true, false },
            PredictionTimeUnit.Hour, 2, 0, "name", false, ConditionValType.String,
            Operation.Like, "*band*")]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Integer,
            Operation.Equal, 200)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Integer,
            Operation.Smaller, 200)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Integer,
            Operation.Smaller_Eq, 200)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Integer,
            Operation.Greater, 200)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Integer,
            Operation.Greater_Eq, 200)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Double,
            Operation.Equal, 200.75)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Double,
            Operation.Smaller, 200.75)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Double,
            Operation.Smaller_Eq, 200.75)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Double,
            Operation.Greater, 200.75)]
        [InlineData(new string[] { "description", "price" }, new bool[] { false, true },
            PredictionTimeUnit.Hour, 2, 1, "price", true, ConditionValType.Double,
            Operation.Greater_Eq, 200.75)]
        public void Query_OnePrediction_OneProp_OneFilter(string[] propertyNames, bool[] isNumeric,
            PredictionTimeUnit timeUnit, int timeValue, int predictionPropIdx,
            string filterPropName, bool isFilterPropNumeric, ConditionValType condValType, Operation filterOp, object filterValue)
        {
            string[] expectedProps = new string[propertyNames.Length + 1];
            Property[] props = new Property[propertyNames.Length + 1];
            int currPropIdx = 2;

            expectedProps[0] = propDict["icon"];
            expectedProps[1] = propDict["item id"];
            props[0] = new Property() { IsNumeric = false, PropertyName = propDict["icon"] };
            props[1] = new Property() { IsNumeric = true, PropertyName = propDict["item id"] };

            Query q = new Query(QueryOp.show);
            Property predictionProp = null;
            for (int i = 0; i < propertyNames.Length; i++)
            {
                Property prop = new Property
                {
                    PropertyName = propDict[propertyNames[i]],
                    IsNumeric = isNumeric[i],
                    Prediction = null
                };
                if (i != predictionPropIdx)
                {
                    expectedProps[currPropIdx] = prop.ToString();
                    prop.PropertyName = propertyNames[i];
                    props[currPropIdx] = prop;
                    currPropIdx++;
                }
                else
                {
                    prop.PropertyName = propertyNames[i];
                    prop.Prediction = prop.Prediction = new Prediction()
                    {
                        TValue = timeValue,
                        TUnit = timeUnit
                    };
                    predictionProp = prop;
                }
                q.Props.Add(prop);
            }
            Property filterProp = new Property()
            {
                PropertyName = filterPropName,
                IsNumeric = isFilterPropNumeric
            };
            Condition filterCond = new Condition()
            {
                CondType = condValType,
                Op = filterOp
            };
            (Type, object)[] queryParamValues = new (Type, object)[1];
            switch (condValType)
            {
                case ConditionValType.String:
                    filterCond.SVal = (string)filterValue;
                    queryParamValues[0] = (typeof(string), filterCond.SVal);
                    break;
                case ConditionValType.Integer:
                    filterCond.IVal = (int)filterValue;
                    queryParamValues[0] = (typeof(int), filterCond.IVal);
                    break;
                case ConditionValType.Double:
                    filterCond.DVal = (double)filterValue;
                    queryParamValues[0] = (typeof(double), filterCond.DVal);
                    break;
            }
            Filter filter = new Filter()
            {
                Property = filterProp,
                Condition = filterCond
            };
            q.Filters.Add(filter);

            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            RuneSqlQuery query = exPlan.Queries[0];
            Assert.True(CheckQuery(query, false, expectedProps, true, new string[1] { propDict[filterPropName] },
                queryParamValues));

            string expectedStmt = BuildExpectedQueryStmt(props, new string[] { propDict[filterPropName] },
                new (Operation, bool)[1] { (filterOp, isFilterPropNumeric) }, false);
            string actualStmt = query.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);

            Assert.Equal(2, exPlan.Queries.Count);
            RuneSqlQuery predictionQuery = exPlan.Queries[1];

            string[] expectedQueryParams = new string[] { "predictable_property_id", propDict["item id"] };
            (Type, object)[] expectedQueryValues = new (Type, object)[]
            {
                (typeof(int), -1),
                (typeof(List<int>), new List<int>())
            };
            Assert.True(CheckQuery(predictionQuery, true, new string[0], true, expectedQueryParams,
                expectedQueryValues));

            expectedStmt = BuildExpectedQueryStmt(new Property[0], expectedQueryParams,
                new (Operation, bool)[2] { (Operation.Equal, true), (Operation.Equal, true) }, true);
            actualStmt = predictionQuery.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);
        }

        [Theory]
        [InlineData(new string[] { "name", "price", "price" }, new bool[] { false, true, true },
            new PredictionTimeUnit[] { PredictionTimeUnit.Hour, PredictionTimeUnit.Day },
            new int[] { 2, 2 }, new int[] { 1, 2 }, 2, 1)]
        [InlineData(new string[] { "price", "name", "price" }, new bool[] { true, false, true },
            new PredictionTimeUnit[] { PredictionTimeUnit.Hour, PredictionTimeUnit.Day },
            new int[] { 2, 2 }, new int[] { 0, 2 }, 2, 1)]
        [InlineData(new string[] { "price", "name", "description", "buying limit" },
            new bool[] { true, false, false, true },
            new PredictionTimeUnit[] { PredictionTimeUnit.Hour },
            new int[] { 2 }, new int[] { 0 }, 2, 3)]
        public void Query_MultipleProps_MultiplePredictions_NoFilters(string[] propertyNames, bool[] isNumeric,
            PredictionTimeUnit[] timeUnits, int[] timeValues, int[] predictionsIdxs, int numExpQueries,
            int numNonPredQuery)
        {
            string[] expectedProps = new string[numNonPredQuery + 2];
            expectedProps[0] = propDict["icon"];
            expectedProps[1] = propDict["item id"];

            Property[] props = new Property[numNonPredQuery + 2];
            props[0] = new Property() { IsNumeric = false, PropertyName = propDict["icon"] };
            props[1] = new Property() { IsNumeric = true, PropertyName = propDict["item id"] };

            Property propertyToPredict = null;
            List<Prediction> predictionsForProp = new List<Prediction>();
            List<Property> propertiesToPredict = new List<Property>(propertyNames.Length - numNonPredQuery);
            int currPropIdx = 2;
            int currPredValueIdx = 0;
            bool currPropPred = false;

            Query q = new Query(QueryOp.show);
            for (int i = 0; i < propertyNames.Length; i++)
            {
                Property prop = new Property
                {
                    PropertyName = propertyNames[i],
                    IsNumeric = isNumeric[i],
                    Prediction = null
                };
                for (int j = 0; j < predictionsIdxs.Length; j++)
                {
                    int currPredIdx = predictionsIdxs[j];
                    if (i == currPredIdx)
                    {
                        Prediction prediction = new Prediction()
                        {
                            TValue = timeValues[currPredValueIdx],
                            TUnit = timeUnits[currPredValueIdx]
                        };
                        prop.Prediction = prediction;
                        propertiesToPredict.Add(prop);
                        currPredValueIdx++;
                        predictionsForProp.Add(prediction);
                        if (propertyToPredict == null)
                        {
                            propertyToPredict = new Property()
                            {
                                PropertyName = propDict[propertyNames[i]],
                                IsNumeric = isNumeric[i],
                                Prediction = null
                            };
                        }
                        currPropPred = true;
                        break;
                    }
                }
                if (!currPropPred)
                {
                    expectedProps[currPropIdx] = prop.ToString();
                    props[currPropIdx] = new Property
                    {
                        PropertyName = propDict[prop.PropertyName],
                        IsNumeric = isNumeric[i]
                    };
                    currPropIdx++;
                }
                currPropPred = false;
                q.Props.Add(prop);
            }

            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            RuneSqlQuery query = exPlan.Queries[0];
            Assert.Equal(numExpQueries, exPlan.Queries.Count);
            Assert.True(CheckQuery(query, false, expectedProps, true, new string[0],
                new (Type, object)[0]));

            string expectedStmt = BuildExpectedQueryStmt(props, new string[0],
                new (Operation, bool)[0], false);
            string actualStmt = query.GetPlainQuery();
            Assert.Equal(expectedStmt, actualStmt);

            for (int i = 1; i < numExpQueries; i++)
            {
                RuneSqlQuery predictionQuery = exPlan.Queries[1];
                string[] expectedQueryParams = new string[] { "predictable_property_id", propDict["item id"] };
                (Type, object)[] expectedQueryValues = new (Type, object)[] {
                    (typeof(int), -1),
                    (typeof(List<int>), new List<int>())
                };
                Assert.True(CheckQuery(predictionQuery, true, new string[0], true, expectedQueryParams,
                    expectedQueryValues));
                Assert.True(CheckPrediction(propertiesToPredict[i - 1], predictionsForProp, predictionQuery));

                expectedStmt = BuildExpectedQueryStmt(new Property[0], expectedQueryParams,
                    new (Operation, bool)[2] { (Operation.Equal, true), (Operation.Equal, true) }, true);
                actualStmt = predictionQuery.GetPlainQuery();
                Assert.Equal(expectedStmt, actualStmt);
            }
        }

        // TODO: Rewrite test to use test parameters
        //[Fact]
        public void QueryWithMultiplePredictionNoPropAndFilters()
        {
            sqlBuilder.SetDBSchema("testSchema");
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TValue = 2,
                    TUnit = PredictionTimeUnit.Hour
                }
            };
            Property p2 = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TValue = 5,
                    TUnit = PredictionTimeUnit.Day
                }
            };
            Property p3 = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TValue = 10,
                    TUnit = PredictionTimeUnit.Month
                }
            };
            q.Props.Add(p);
            q.Props.Add(p2);
            q.Props.Add(p3);

            RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Equal(3, exPlan.Queries.Count);


            string[] expectedQueryProps = new string[0];
            string[] expectedQueryParams = new string[0];
            (Type, object)[] expectedQueryValues = new (Type, object)[0];
            for (int i = 0; i < exPlan.Queries.Count; i++)
            {
                RuneSqlQuery query = exPlan.Queries[i];
                Assert.True(CheckQuery(query, true, expectedQueryProps, false, expectedQueryParams, expectedQueryValues));

                string expectedStmt = "select * from testSchema.price_regression_models";
                string actualStmt = query.GetPlainQuery();
                Assert.Equal(expectedStmt, actualStmt);
            }
        }

        // TODO: Rewrite test to use test parameters
        //[Fact]
        public void QueryWithPropsOnePredAndMixedFilters()
        {
            sqlBuilder.SetDBSchema("testSchema");
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true,
                Prediction = new Prediction()
                {
                    TValue = 2,
                    TUnit = PredictionTimeUnit.Hour
                }
            };
            Property p2 = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };

            Property p3 = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Filter nameFilter = new Filter()
            {
                Property = p3,
                Condition = new Condition()
                {
                    CondType = ConditionValType.String,
                    Op = Operation.None,
                    SVal = "Sword"
                }
            };
            Property p4 = new Property
            {
                PropertyName = "buying limit",
                IsNumeric = true
            };
            Filter buyingLimitFilter = new Filter()
            {
                Property = p4,
                Condition = new Condition()
                {
                    CondType = ConditionValType.None,
                    Op = Operation.None
                }
            };
            q.Props.Add(p);
            q.Props.Add(p2);
            q.Filters.Add(nameFilter);
            q.Filters.Add(buyingLimitFilter);
            string[] expectedResultProps = new string[] { propDict["name"], new Property(){
                PropertyName = propDict[p.PropertyName], IsNumeric = true, Prediction = p.Prediction
            }.ToString() };

            string[] expectedProps = new string[] { propDict["item id"], propDict["name"] };
            string[] expectedParams = new string[] { propDict["name"], propDict["buying limit"] };
            (Type, object)[] expectedVals = new (Type, object)[2];

            string[] expectedPropsPred = new string[0];
            string[] expectedParamsPred = new string[] { propDict["item id"] };
            (Type, object)[] expectedParamValsPred = new (Type, object)[] { (typeof(List<int>), new List<int>()) };
            Operation[] stringOps = new Operation[]
            {
                Operation.Starting_With, Operation.Ending_With, Operation.Equal, Operation.Like
            };
            string[] expectedNameValues = new string[]
            {
               "Sword*", "*Sword", "Sword", "*Sword*"
            };
            Operation[] ops = new Operation[]
            {
                Operation.Equal, Operation.Greater, Operation.Greater_Eq,
                Operation.Smaller, Operation.Smaller_Eq
            };
            string[] expectedOpString = new string[]
            {
                "=", ">", ">=", "<", "<="
            };
            int expectedIVal = 5;
            double expectedDVal = 10.75;

            for (int i = 0; i < stringOps.Length; i++)
            {
                expectedVals[0] = (typeof(string), expectedNameValues[i]);
                q.Filters[0].Condition.Op = stringOps[i];
                for (int k = 0; k < 2; k++)
                {
                    if (k == 0)
                    {
                        expectedVals[1] = (typeof(int), expectedIVal);
                        buyingLimitFilter.Condition.IVal = expectedIVal;
                        buyingLimitFilter.Condition.CondType = ConditionValType.Integer;
                    }
                    else
                    {
                        expectedVals[1] = (typeof(double), expectedDVal);
                        buyingLimitFilter.Condition.DVal = expectedDVal;
                        buyingLimitFilter.Condition.CondType = ConditionValType.Double;
                    }
                    for (int j = 0; j < ops.Length; j++)
                    {
                        q.Filters[1].Condition.Op = ops[j];
                        q.Filters[0].Property.PropertyName = "name";
                        q.Filters[1].Property.PropertyName = "buying limit";
                        q.Props[0].PropertyName = "price";
                        q.Props[1].PropertyName = "name";
                        RuneSqlExecutionPlan exPlan = sqlBuilder.BuildExecutionPlanFromQuery(q);
                        Assert.NotNull(exPlan);
                        Assert.Equal(2, exPlan.Queries.Count);
                        Assert.Equal(expectedResultProps.Length, exPlan.ResultProps.Count);
                        for (int r = 0; r < expectedResultProps.Length; r++)
                        {
                            Assert.True(exPlan.ResultProps.Contains(expectedResultProps[r]));
                        }

                        RuneSqlQuery query = exPlan.Queries[0];
                        Assert.True(CheckQuery(query, false, expectedProps, true, expectedParams, expectedVals));

                        string expectedStmt = "select item_id,item_name from testSchema.items where item_name"
                            + (stringOps[i] == Operation.Equal ? "=" : " like ") + "@" + propDict["name"] + "_0"
                            + " and buying_limit"
                            + expectedOpString[j] + "@" + propDict["buying limit"] + "_0";
                        string actualStmt = query.GetPlainQuery();
                        Assert.Equal(expectedStmt, actualStmt);

                        RuneSqlQuery predicitonQuery = exPlan.Queries[1];
                        Assert.True(CheckQuery(predicitonQuery, true, expectedPropsPred, true, expectedParamsPred,
                            expectedParamValsPred));
                        Assert.Equal(predicitonQuery.SubstitutedProperty, p);

                        expectedStmt = "select * from testSchema.price_regression_models where item_id in (@item_id)";
                        actualStmt = predicitonQuery.GetPlainQuery();
                        Assert.Equal(expectedStmt, actualStmt);
                    }
                }
            }
        }

        private string BuildExpectedQueryStmt(Property[] props, string[] filterPropNames, (Operation, bool)[] ops, bool isPrediction)
        {
            string queryFormat = "select {0} from " + (isPrediction ? "regression_models " : "items ") + "{1}";
            string defaultFilterFormat = "{0} {1} @{2}_{3}";
            string predictionFilterFormat = "{0} {1} @{2}";
            string predictionItemIdFilterformat = "{0} {1} any(@{2})";

            string propString = props.Length == 0 ? "*" : "";
            string filterString = "";
            for (int i = 0; i < props.Length; i++)
            {
                propString += props[i].ToString();
                if (i + 1 < props.Length)
                {
                    propString += ",";
                }
            }
            if (filterPropNames.Length > 0)
            {
                filterString = "where ";
                for (int j = 0; j < filterPropNames.Length; j++)
                {
                    if (isPrediction)
                    {
                        string format = filterPropNames[j].Equals("item_id") ? predictionItemIdFilterformat :
                            predictionFilterFormat;
                        filterString += string.Format(format, filterPropNames[j],
                            Enums.GetOperationAsStringSymbol(ops[j].Item1), filterPropNames[j]);
                    }
                    else
                    {
                        filterString += string.Format(defaultFilterFormat, filterPropNames[j],
                           Enums.GetOperationAsStringSymbol(ops[j].Item1), filterPropNames[j], j);
                    }
                    if (j + 1 < filterPropNames.Length)
                    {
                        filterString += " and ";
                    }
                }
            }
            return string.Format(queryFormat, propString, filterString).Trim();
        }

        private bool CheckQuery(RuneSqlQuery query, bool isPrediction, string[] queryProps,
            bool compareParamContent, string[] queryParams, (Type, object)[] queryParamValues)
        {
            if (query == null || query.PropertiesToFetch == null
                || query.PropertiesToFetch.Count != queryProps.Length)
            {
                return false;
            }
            if (isPrediction != query.IsPrediction) return false;
            if (compareParamContent)
            {
                if (query.QueryParams == null || queryParams.Length != query.QueryParams.Count) return false;
                for (int i = 0; i < queryParams.Length; i++)
                {
                    try
                    {
                        string expQueryParamName = isPrediction ? queryParams[i] : queryParams[i] + "_" + i.ToString();
                        Assert.True(query.QueryParams.ContainsKey(expQueryParamName));
                        (Type actualType, object paramValue) = query.QueryParams[expQueryParamName];

                        Assert.Equal(queryParamValues[i].Item1, actualType);
                        Assert.Equal(queryParamValues[i].Item2, paramValue);
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool CheckPrediction(Property substitutedProp, IList<Prediction> predictions, RuneSqlQuery query)
        {
            if (query == null || predictions == null || query.SubstitutedPropertyPredictions == null)
            {
                return false;
            }
            if (substitutedProp == null && query.SubstitutedProperty == null)
            {
                return true;
            }
            Assert.Equal(substitutedProp, query.SubstitutedProperty);
            Assert.Equal(predictions.Count, query.SubstitutedPropertyPredictions.Count);
            for (int i = 0; i < predictions.Count; i++)
            {
                Assert.Equal(predictions[i], query.SubstitutedPropertyPredictions[i]);
            }
            return true;
        }
    }
}
