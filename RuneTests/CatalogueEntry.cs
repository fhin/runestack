﻿using System.Collections.Generic;

namespace RuneTests
{
    public class CatalogueEntry
    {
        public Dictionary<char, List<string>> lettersDataStrings;
        public Dictionary<char, int> numItemsPerLetter;

        public CatalogueEntry()
        {
            lettersDataStrings = new Dictionary<char, List<string>>();
            numItemsPerLetter = new Dictionary<char, int>();
        }
    }
}
