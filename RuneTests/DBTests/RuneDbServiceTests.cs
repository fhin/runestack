﻿using RuneDB.Models;
using RuneDB.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace RuneTests.DBTests
{
    public class RuneDbServiceTests : IClassFixture<RuneDBContextFactoryFixture>, IDisposable
    {
        RuneDBContextFactoryFixture _dbContextFactoryFixture;

        public RuneDbServiceTests(RuneDBContextFactoryFixture dbFactoryFixture)
        {
            _dbContextFactoryFixture = dbFactoryFixture;
        }
 
        public void Dispose()
        {
            if (!_dbContextFactoryFixture.ResetDb())
            {
                throw new Exception("Could not reset database for RuneDBServiceTests");
            }
        }

        [Fact]
        public void AddCategoryNoItemEntries()
        {
            RuneCategory category = _dbContextFactoryFixture.GenerateRuneCategory("defaultCategory", null, false);
            RuneDbService service = new RuneDbService(_dbContextFactoryFixture);
            service.StoreCategory(category);
            using (var context = _dbContextFactoryFixture.GetContext())
            {
                RuneCategory dbCat = context.RuneCategories.First(x => x.Name.Equals("defaultCategory"));
                Assert.NotNull(dbCat);
                Assert.Equal(1, context.RuneCategories.Count());
                Assert.Equal(category.Name, dbCat.Name);
            }
        }

        [Fact]
        public void AddMultipleCategories()
        {
            int numCategories = 5;
            List<RuneCategory> categories = new List<RuneCategory>();
            for (int i = 0; i < numCategories; i++)
            {
                categories.Add(_dbContextFactoryFixture.GenerateRuneCategory("defaultCategory" + i, null, false));
            }
            RuneDbService service = new RuneDbService(_dbContextFactoryFixture);
            service.StoreCategories(categories);
            using (var context = _dbContextFactoryFixture.GetContext())
            {
                Assert.Equal(numCategories, context.RuneCategories.Count());
                for (int i = 0; i < numCategories; i++)
                {
                    RuneCategory dbCat = context.RuneCategories.First(x => x.Name.Equals("defaultCategory" + i));
                    Assert.NotNull(dbCat);
                    Assert.Equal(categories[i].Name, dbCat.Name);
                }
            }
        }

        [Fact]
        public void AddEmptyItemList()
        {
            RuneDbService service = new RuneDbService(_dbContextFactoryFixture);
            service.StoreItems(new List<RuneItem>());
            using (var context = _dbContextFactoryFixture.GetContext())
            {
                Assert.Equal(0, context.RuneItems.Count());
            }
        }

        [Fact]
        public void AddMultipleItemsSameStartingLetterOneCategory()
        {
            RuneDbService service = new RuneDbService(_dbContextFactoryFixture);
            RuneCategory defaultCategory = _dbContextFactoryFixture.GenerateRuneCategory("defaultCategory", null, true);

            IEnumerable<RuneItem> items = _dbContextFactoryFixture.GenerateItems(20, defaultCategory, false);
            service.StoreItems(items);
            IEnumerable<RuneItem> dbItems;
            using (var db = _dbContextFactoryFixture.GetContext())
            {
                dbItems = db.RuneItems.AsEnumerable();
                Assert.NotNull(dbItems);
                Assert.Equal(10, dbItems.Count());
                foreach (var item in items)
                {
                    Assert.NotNull(item);
                    Assert.Contains(dbItems, dbItem => CustomRuneItemEqualityChecker.AllPropertiesEqual(dbItem, item));
                }
            }
        }

        [Fact]
        public void AddMultipleItemsOneCategoryRandomValues()
        {
            RuneDbService service = new RuneDbService(_dbContextFactoryFixture);
            RuneCategory defaultCategory = _dbContextFactoryFixture.GenerateRuneCategory("defaultCategory", null, true);

            IEnumerable<RuneItem> items = _dbContextFactoryFixture.GenerateItems(20, defaultCategory, true);
            service.StoreItems(items);
            IEnumerable<RuneItem> dbItems;
            using (var db = _dbContextFactoryFixture.GetContext())
            {
                dbItems = db.RuneItems.AsEnumerable();
                Assert.NotNull(dbItems);
                Assert.Equal(10, dbItems.Count());
                foreach (var item in items)
                {
                    Assert.NotNull(item);
                    Assert.Contains(dbItems, dbItem => CustomRuneItemEqualityChecker.AllPropertiesEqual(dbItem, item));
                }
            }
        }
    }
}
