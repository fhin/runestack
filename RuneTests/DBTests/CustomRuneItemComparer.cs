﻿using RuneDB.Models;

namespace RuneTests.DBTests
{
    public class CustomRuneItemEqualityChecker
    {
        public static bool AllPropertiesEqual(RuneItem item1, RuneItem item2)
        {
            if (item1 == null)
            {
                return item2 != null ? false : true;
            }
            if (item2 == null) return false;
            bool areEqual = item1.Equals(item2);
            areEqual &= item1.BuyingLimit == item2.BuyingLimit;
            areEqual &= item1.ConstPrice == item2.ConstPrice;
            areEqual &= item1.CurrentGePrice == item2.CurrentGePrice;
            areEqual &= item1.CurrentPriceChange == item2.CurrentPriceChange;
            areEqual &= item1.RuneCategoryId == item2.RuneCategoryId;
            return areEqual;
        }
    }
}
