﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using RuneDB;
using RuneDB.Models;
using RuneDB.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace RuneTests.DBTests
{
    public class RuneDBContextFactoryFixture : IRuneContextFactory, IDisposable
    {
        // https://docs.microsoft.com/en-us/ef/core/miscellaneous/testing/sharing-databases
        private const string _connectionString = "DataSource=:memory:";
        private readonly SqliteConnection _connection;
        private DbContextOptions<RuneDBContext> _options;
        private const int RANDOM_STRING_LENGTH = 10;
        private const int RANDOM_INT_MIN_VAL = 0;
        private const int RANDOM_INT_MAX_VAL = 1500;
        public RuneDBContextFactoryFixture()
        {
            _connection = new SqliteConnection(_connectionString);
            _connection.Open();
            _options = new DbContextOptionsBuilder<RuneDBContext>().UseSqlite(_connection).Options;
        }

        public void Dispose() => _connection.Dispose();

        public bool ResetDb()
        {
            using (var context = new RuneDBContext(_options))
            {
                return context.Database.EnsureDeleted();
            }
        }

        public IRuneDB GetContext()
        {
            RuneDBContext context = new RuneDBContext(_options);
            context.Database.EnsureCreated();
            return context;
        }

        public IEnumerable<RuneItem> GenerateItems(int numItemsToCreate, RuneCategory runeCategory,
            bool generateRandomValues)
        {
            const string defaultName = "defaultName";
            const string defaultDescription = "This is a default description";

            List<RuneItem> items = new List<RuneItem>();
            Random randomGenerator = generateRandomValues ? new Random() : null;
            for (int i = 0; i < numItemsToCreate; i++)
            {
                RuneItem item;
                if (!generateRandomValues)
                {
                    item = new RuneItem
                    {
                        Name = defaultName + i.ToString(),
                        Description = defaultDescription + i.ToString(),
                        ConstPrice = i,
                        BuyingLimit = i,
                        CurrentGePrice = i,
                        RuneCategoryId = runeCategory.Id,
                        Icon = new byte[0]
                    };
                }
                else
                {
                    item = new RuneItem
                    {
                        Name = GenerateRandomString(RANDOM_STRING_LENGTH, randomGenerator),
                        Description = GenerateRandomString(RANDOM_STRING_LENGTH, randomGenerator),
                        ConstPrice = GenerateRandomInt(RANDOM_INT_MIN_VAL, RANDOM_INT_MAX_VAL, randomGenerator),
                        BuyingLimit = GenerateRandomInt(RANDOM_INT_MIN_VAL, RANDOM_INT_MAX_VAL, randomGenerator),
                        CurrentGePrice = GenerateRandomInt(RANDOM_INT_MIN_VAL, RANDOM_INT_MAX_VAL, randomGenerator),
                        RuneCategoryId = runeCategory.Id,
                        Icon = new byte[0]
                    };
                }
                items.Add(item);
            }
            return items;
        }

        private string GenerateRandomString(int stringLength, Random randomIndexGenerator)
        {
            if (randomIndexGenerator == null)
            {
                throw new ArgumentException("Random generator cannot be null");
            }
            string validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            return new string(Enumerable.Range(1, stringLength)
                .Select(_ => validChars[randomIndexGenerator.Next(0, validChars.Length - 1)]).ToArray());
        }

        private int GenerateRandomInt(int minVal, int maxVal, Random randomIndexGenerator)
        {
            if (randomIndexGenerator == null)
            {
                throw new ArgumentException("Random generator cannot be null");
            }
            return randomIndexGenerator.Next(minVal, maxVal);
        }

        public RuneCategory GenerateRuneCategory(string name, Dictionary<char, RuneCategoryEntry> letterEntries, bool storeCategory)
        {
            RuneCategory category = new RuneCategory()
            {
                Name = name,
                LetterEntries = letterEntries ?? new Dictionary<char, RuneCategoryEntry>()
            };
            if (!storeCategory) return category;
            using (var context = GetContext())
            {
                context.RuneCategories.Add(category);
                context.SaveChanges();
                category = context.RuneCategories.First(cat => cat.Name.Equals(category.Name));
                Assert.NotNull(category);
            }
            return category;
        }

        
    }
}
