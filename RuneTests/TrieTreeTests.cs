﻿using RuneDB.Utils;
using System;
using System.Collections.Generic;
using Xunit;

namespace RuneTests
{
    public class TrieTreeTests
    {
        [Fact]
        public void EmptyNameList()
        {
            string[] names = new string[0];
            TrieTree tree = new TrieTree();
            Assert.Throws<ArgumentException>(() => tree.AddWord(""));
            Assert.Throws<ArgumentException>(() => tree.AddWord(null));
        }

        [Fact]
        public void AddSingleWord()
        {
            string[] names = { "test_name" };
            TrieTree tree = new TrieTree();
            tree.AddWords(names);
            Assert.NotNull(tree.root);
            Assert.Equal(1, tree.root.numSummarizedItems);
            Assert.Single(tree.root.Nodes);
            tree = new TrieTree();
            tree.AddWord(names[0]);
            Assert.NotNull(tree.root);
            Assert.Equal(1, tree.root.numSummarizedItems);
            Assert.Single(tree.root.Nodes);
            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Single(preFixList);
            Assert.Equal(names[0], preFixList[0].Item1);
            Assert.Equal(1, preFixList[0].Item2);
        }

        [Fact]
        public void AddWordsNoCommonPrefix()
        {
            string[] names = { "test name", "other name" };
            TrieTree tree = new TrieTree();
            tree.AddWords(names);
            Assert.NotNull(tree.root);
            Assert.Equal(2, tree.root.numSummarizedItems);
            Assert.Equal(2, tree.root.Nodes.Count);
            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Equal(2, preFixList.Count);
            Assert.Equal(names[0].Split(' ')[0], preFixList[0].Item1);
            Assert.Equal(1, preFixList[0].Item2);
            Assert.Equal(names[1].Split(' ')[0], preFixList[1].Item1);
            Assert.Equal(1, preFixList[1].Item2);
        }

        [Fact]
        public void AddWordsWithCommonPrefix()
        {
            string[] names = { "test", "text" };
            TrieTree tree = new TrieTree();
            tree.AddWords(names);
            Assert.NotNull(tree.root);
            Assert.Equal(2, tree.root.numSummarizedItems);
            Assert.Single(tree.root.Nodes);
            Assert.True(tree.root.Nodes.ContainsKey('t'));

            TrieNode start = tree.root.Nodes['t'];
            Assert.Equal('t', start.Letter);
            Assert.Equal(2, start.numSummarizedItems);
            Assert.True(start.Nodes.ContainsKey('e'));
            Assert.Single(start.Nodes);

            start = start.Nodes['e'];
            Assert.Equal('e', start.Letter);
            Assert.Equal(2, start.numSummarizedItems);
            Assert.Equal(2, start.Nodes.Count);
            Assert.True(start.Nodes.ContainsKey('s') && start.Nodes.ContainsKey('x'));

            TrieNode node_left = start.Nodes['s'];
            Assert.Equal('s', node_left.Letter);
            Assert.Single(node_left.Nodes);
            Assert.Equal(1, node_left.numSummarizedItems);

            TrieNode node_right = start.Nodes['x'];
            Assert.Equal('x', node_right.Letter);
            Assert.Single(node_right.Nodes);
            Assert.Equal(1, node_right.numSummarizedItems);

            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Equal(2, preFixList.Count);
            Assert.Equal(names[0], preFixList[0].Item1);
            Assert.Equal(1, preFixList[0].Item2);
            Assert.Equal(names[1], preFixList[1].Item1);
            Assert.Equal(1, preFixList[1].Item2);
        }

        [Fact]
        public void AddWordsWithCommonPrefixNested()
        {
            string[] names = { "test text", "text", "tesa glue", "texor item" };
            string[] prefixes = { "test", "text", "tesa", "texor" };
            TrieTree tree = new TrieTree();
            tree.AddWords(names);
            Assert.NotNull(tree.root);
            Assert.Equal(4, tree.root.numSummarizedItems);
            Assert.Single(tree.root.Nodes);
            Assert.True(tree.root.Nodes.ContainsKey('t'));

            TrieNode start = tree.root.Nodes['t'];
            Assert.Equal('t', start.Letter);
            Assert.Equal(4, start.numSummarizedItems);
            Assert.True(start.Nodes.ContainsKey('e'));
            Assert.Single(start.Nodes);

            start = start.Nodes['e'];
            Assert.Equal('e', start.Letter);
            Assert.Equal(4, start.numSummarizedItems);
            Assert.Equal(2, start.Nodes.Count);
            Assert.True(start.Nodes.ContainsKey('s') && start.Nodes.ContainsKey('x'));

            TrieNode node_left = start.Nodes['s'];
            Assert.Equal('s', node_left.Letter);
            Assert.Equal(2, node_left.Nodes.Count);
            Assert.Equal(2, node_left.numSummarizedItems);

            TrieNode nested_left = node_left.Nodes['t'];
            TrieNode nested_right = node_left.Nodes['a'];
            Assert.Equal(1, nested_left.numSummarizedItems);
            Assert.Equal(1, nested_right.numSummarizedItems);
            Assert.Equal('t', nested_left.Letter);
            Assert.Equal('a', nested_right.Letter);

            TrieNode node_right = start.Nodes['x'];
            Assert.Equal('x', node_right.Letter);
            Assert.Equal(2, node_right.Nodes.Count);
            Assert.Equal(2, node_right.numSummarizedItems);

            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Equal(4, preFixList.Count);

            int matchCnt = 0;
            foreach (var (name, numItems) in preFixList)
            {
                if (name.Equals(prefixes[0]) 
                    || name.Equals(prefixes[1]) 
                    || name.Equals(prefixes[2]) 
                    || name.Equals(prefixes[3]))
                {
                    matchCnt++;
                }
                Assert.Equal(1, numItems);
            }
            Assert.Equal(4, matchCnt);
        }

        [Fact]
        public void AddWordsW()
        {
            /*
            string[] words =
            {
                "Vampyre dust",
                "Varrock teleport",
                "Veg ball",
                "Vegetable batta",
                "Verac's armour set",
                "Verac's brassard",
                "Verac's brassard 0",
                "Verac's flail",
                "Verac's helm",
                "Abyssal bludgeon",
                "Abyssal dagger",
                "Acorn"
            };
            */


            string[] words =
            {
                "Veg ball",
                "Vegetable batta"
            };

            TrieTree tree = new TrieTree();
            tree.AddWords(words);

            Assert.Equal(2, tree.root.numSummarizedItems);
            Assert.True(tree.root.Nodes.ContainsKey('V'));
            TrieNode nodeToCheck = tree.root.Nodes['V'].Nodes['e'].Nodes['g'];
            Assert.True(nodeToCheck.Nodes.ContainsKey(tree.wordEndChar));
            Assert.True(nodeToCheck.Nodes.ContainsKey('e'));
            Assert.Equal(2, nodeToCheck.numSummarizedItems);
            Assert.Equal(2, nodeToCheck.Nodes.Count);

            TrieNode nodeLeft = nodeToCheck.Nodes[tree.wordEndChar];
            TrieNode nodeRight = nodeToCheck.Nodes['e'];

            Assert.Equal(tree.wordEndChar, nodeLeft.Letter);
            Assert.Equal('e', nodeRight.Letter);
        }

        [Fact]
        public void GetPrefixesWithEnclosingWord()
        {
            string[] words =
            {
                "Veg ball",
                "Vegetable batta"
            };

            TrieTree tree = new TrieTree();
            tree.AddWords(words);
            words[0] = words[0].Split(' ')[0];
            words[1] = words[1].Split(' ')[0];

            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Equal(2, preFixList.Count);
            int matchCnt = 0;
            foreach (var (name, itemCnt) in preFixList)
            {
                if (name.Equals(words[0]) || name.Equals(words[1]))
                {
                    matchCnt++;
                }
                Assert.Equal(1, itemCnt);
            }
            Assert.Equal(2, matchCnt);
        }

        [Fact]
        public void GetPrefixes()
        {
            string[] words =
            {
                "Vampyre dust",
                "Varrock teleport",
                "Veg ball",
                "Vegetable batta",
                "Verac's armour set",
                "Verac's brassard",
                "Verac's brassard 0",
                "Verac's flail",
                "Verac's helm",
                "Abyssal bludgeon",
                "Abyssal dagger",
                "Acorn"
            };

            string[] prefixes =
            {
                "Vampyre",
                "Varrock",
                "Veg",
                "Vegetable",
                "Verac's",
                "Abyssal",
                "Acorn"
            };
            int[] numItems = { 1, 1, 1, 1, 5, 2, 1 };

            TrieTree tree = new TrieTree();
            tree.AddWords(words);

            List<(string, int)> preFixList = tree.GetItemPrefixes();
            Assert.Equal(7, preFixList.Count);
            int matchCnt = 0;
            foreach (var (name, itemCnt) in preFixList)
            {
                for (int i = 0; i < prefixes.Length; i++)
                {
                    if (name.Equals(prefixes[i]) && itemCnt == numItems[i])
                    {
                        matchCnt++;
                    }
                }
            }
            Assert.Equal(7, matchCnt);
        }
    }
}
