﻿using RuneDB.Utils;
using System;
using Xunit;

namespace RuneTests
{
    public class MatrixUtilsTests
    {
        [Fact]
        public void CreateEmptyIdentityMat()
        {
            Assert.Throws<ArgumentException>(() => MatrixUtils.CreateIdentityMat(0));
        }

        [Fact]
        public void CreateIdentityMatNegativeSize()
        {
            Assert.Throws<ArgumentException>(() => MatrixUtils.CreateIdentityMat(-1));
        }

        [Fact]
        public void CreateIdentityMatDifferentSizes()
        {
            int[] sizes = { 1, 2, 4, 6 };
            for (int i = 0; i < sizes.Length; i++)
            {
                double[,] identMatrix = MatrixUtils.CreateIdentityMat(sizes[i]);
                for (int j = 0; j < sizes[i]; j++)
                {
                    for (int k = 0; k < sizes[i]; k++)
                    {
                        Assert.Equal((k == j ? 1 : 0), identMatrix[j, k]);
                    }
                }
            }
        }

        [Fact]
        public void TransposeEmptyMatrix()
        {
            double[,] emptyMatrix = new double[0, 0];
            double[,] result = MatrixUtils.TransposeMatrix(emptyMatrix);
            Assert.Equal(0, emptyMatrix.GetLength(0));
            Assert.Equal(0, emptyMatrix.GetLength(1));
        }

        [Fact]
        public void TransposeMatrixOfSizeOne()
        {
            double[,] mat = MatrixUtils.CreateIdentityMat(1);
            double[,] result = MatrixUtils.TransposeMatrix(mat);
            Assert.Equal(1, result.GetLength(0));
            Assert.Equal(1, result.GetLength(1));
        }

        [Fact]
        public void TransposeMatrizes()
        {
            double[][,] matrizes = new double[3][,];
            matrizes[0] = new double[,]
            {
                {1,2},
                {3,4}
            };
            matrizes[1] = new double[,]
            {
                {1,2,3,4 },
                {5,6,7,8},
                {9,10,11,12 }
            };
            matrizes[2] = new double[,]
            {
                {1,2,3,4,5,6,7,8,9,10 },
                {11,12,13,14,15,16,17,18,19,20 }
            };

            double[][,] expected = new double[3][,];
            expected[0] = new double[,]
            {
                {1,3},
                {2,4}
            };
            expected[1] = new double[,]
            {
                {1,5,9},
                {2,6,10},
                {3,7,11},
                {4,8,12}
            };
            expected[2] = new double[,]
            {
                {1,11},
                {2,12},
                {3,13},
                {4,14},
                {5,15},
                {6,16},
                {7,17},
                {8,18},
                {9,19},
                {10,20}
            };

            for (int i = 0; i < matrizes.GetLength(0); i++)
            {
                double[,] transposedMat = MatrixUtils.TransposeMatrix(matrizes[i]);
                Assert.Equal(expected[i].GetLength(0), transposedMat.GetLength(0));
                Assert.Equal(expected[i].GetLength(1), transposedMat.GetLength(1));
                for (int j = 0; j < expected[i].GetLength(0); j++)
                {
                    for (int k = 0; k < expected[i].GetLength(1); k++)
                    {
                        Assert.Equal(expected[i][j, k], transposedMat[j, k]);
                    }
                }
            }
        }

        [Fact]
        public void MultiplyMatrizesInvalidSizes()
        {
            double[,] matLeft = new double[1, 3];
            double[,] matRight = new double[4, 1];
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMats(matLeft, matRight));
        }

        [Fact]
        public void MultiplyMatrizesSingleValue()
        {
            double[,] matLeft = new double[1, 1] { { 2 } };
            double[,] matRight = new double[1, 1] { { 2 } };
            double[,] result = new double[1, 1] { { 4 } };
            Assert.True(VerifyMatMultResult(matLeft, matRight, result));
        }

        [Fact]
        public void MultiplySquareMatrizes()
        {
            double[,] matLeft = MatrixUtils.CreateIdentityMat(2);
            double[,] matRight = MatrixUtils.CreateIdentityMat(2);
            double[,] result = MatrixUtils.MultMats(matLeft, matRight);
            Assert.True(VerifyMatMultResult(matLeft, matRight, result));

            matLeft = new double[3, 3]
            {
                {4,1,-4},{6,0,-6},{7,0,-7}
            };
            matRight = new double[3, 3]
            {
                {1,0,1},{1/2,1,1/2},{-3/4,1,-3/4}
            };
            result = MatrixUtils.MultMats(matLeft, matRight);
            Assert.True(VerifyMatMultResult(matLeft, matRight, result));
        }

        [Fact]
        public void MultiplyMatrizes()
        {
            double[,] matLeft = new double[1, 3]
            {
                {1,2,3 }
            };
            double[,] matRight = new double[3, 1]
            {
                {3},{2},{1}
            };
            double[,] result = MatrixUtils.MultMats(matLeft, matRight);
            Assert.True(VerifyMatMultResult(matLeft, matRight, result));
            matLeft = new double[3, 4]
            {
                {1,2,3,4},
                {5,6,7,8},
                {9,10,11,12}
            };
            matRight = new double[4, 3]
            {

                {1,5,9 },
                {2,6,10},
                {3,7,11},
                {4,8,12}
            };
            result = MatrixUtils.MultMats(matLeft, matRight);
            Assert.True(VerifyMatMultResult(matLeft, matRight, result));
        }

        [Fact]
        public void MultiplySimpleMatrixVectorInvalidSizes()
        {
            double[,] mat = new double[1, 4];
            double[] vector = new double[3];
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(mat, vector, true));
            mat = new double[4, 1];
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(mat, vector, false));
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(mat, new double[0], false));
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(new double[3, 0], vector, false));
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(new double[0, 3], vector, false));
        }

        [Fact]
        public void MultiplyMatrixVectorInvalidSizes()
        {
            double[,] mat = new double[3, 4];
            double[] vector = new double[3];
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(mat, vector, true));
            mat = new double[4, 1];
            Assert.Throws<ArgumentException>(() => MatrixUtils.MultMatVec(mat, vector, false));
        }

        [Fact]
        public void MultiplyMatrixRowVector()
        {
            double[,] mat = new double[3, 1]
            {
                {1},{2},{3}
            };
            double[] vec = new double[3] { 1, 2, 3 };
            double[,] result = MatrixUtils.MultMatVec(mat, vec, false);
            Assert.True(VerifyMatVecMultResult(mat, vec, false, result));
            mat = new double[6, 1]{
                 {1},{2},{3},{4},{5},{6}
            };
            vec = new double[6] { -2, 1, -4, 3, -6, 5 };
            result = MatrixUtils.MultMatVec(mat, vec, false);
            Assert.True(VerifyMatVecMultResult(mat, vec, false, result));
        }

        [Fact]
        public void MultiplayMatrixColVector()
        {
            double[,] mat = new double[1, 3]
            {
                {1,2,3}
            };
            double[] vec = new double[3] { 1, 2, 3 };
            double[,] result = MatrixUtils.MultMatVec(mat, vec, true);
            Assert.True(VerifyMatVecMultResult(mat, vec, true, result));
            mat = new double[1, 6]{
                {1,2,3,4,5,6}
            };
            vec = new double[6] { -2, 1, -4, 3, -6, 5 };
            result = MatrixUtils.MultMatVec(mat, vec, true);
            Assert.True(VerifyMatVecMultResult(mat, vec, true, result));
        }

        [Fact]
        public void LUPDecompose1x1Matrix()
        {
            double[,] mat = MatrixUtils.CreateIdentityMat(1);
            (double[,] LU, int[] P, int exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            double[,] actualLU = new double[1, 1] { { 1 } };
            Assert.True(VerifyMatrizes(LU, actualLU, 0));
            Assert.True(VerifyPermutationVectors(new int[1] { 0 }, P));
            Assert.Equal(0, exchangeCnt);
        }

        [Fact]
        public void LUPDecomposeIdentityMatrix()
        {
            double[,] mat = MatrixUtils.CreateIdentityMat(5);
            (double[,] LU, int[] P, int exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            double[,] actualLU = new double[5, 5]
            {
                { 1,0,0,0,0 }, { 0,1,0,0,0 }, { 0,0,1,0,0 }, { 0,0,0,1,0 }, { 0,0,0,0,1 }
            };
            int[] actualP = new int[5] { 0, 1, 2, 3, 4 };
            Assert.True(VerifyMatrizes(LU, actualLU, 0));
            Assert.True(VerifyPermutationVectors(actualP, P));
            Assert.Equal(0, exchangeCnt);
        }

        [Fact]
        public void LUPDecomposeNoSwitch()
        {
            double[,] mat = new double[3, 3]
             {
                {2,7,6},
                {9,3,1},
                {4,3,5}
            };
            (double[,] LU, int[] P, int exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            double[,] actualLU = new double[3, 3]
            {
                { 2,7,6 },
                { 4.5,-28.5,-26 },
                { 2,0.38596491228070173,3.0350877192982448 }
            };
            int[] actualP = new int[3] { 0, 1, 2 };
            Assert.True(VerifyMatrizes(LU, actualLU, 0));
            Assert.True(VerifyPermutationVectors(actualP, P));
            Assert.Equal(0, exchangeCnt);
        }

        [Fact]
        public void LUPDecomposeOneSwitch()
        {
            double[,] mat = new double[3, 3]
             {
                {2,7,6},
                {9,0,1},
                {4,3,5}
            };
            (double[,] LU, int[] P, int exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            double[,] actualLU = new double[3, 3]
            {
                { 2,7,6 },
                { 2,-11,-7 },
                { 4.5,2.8636363636363638,-5.9545454545454533 }
            };
            int[] actualP = new int[3] { 0, 2, 1 };
            Assert.True(VerifyMatrizes(LU, actualLU, 0));
            Assert.True(VerifyPermutationVectors(actualP, P));
            Assert.Equal(1, exchangeCnt);
        }

        [Fact]
        public void LUPDecomposeMultipleSwitches()
        {
            // 2 7 6 2 
            // 5 6 7 1
            // 4 3 0 4
            // 9 0 1 3
            double[,] mat = new double[4, 4]
            {
                {2,7,6,2 },
                {9,0,1,3 },
                {4,3,0,4 },
                {5,6,7,1 }
            };
            (double[,] LU, int[] P, int exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            double[,] actualLU = new double[4, 4]
            {
                { 2,7,6,2},
                { 2.5,-11.5,-8,-4 },
                { 4.5,2.73913043, -4.086956525, 4.95652174},
                { 2,0.95652174, 1.06382979, -1.44680851 }
            };
            int[] actualP = new int[4] { 0, 3, 1, 2 };
            Assert.True(VerifyMatrizes(LU, actualLU, 10E-5));
            Assert.True(VerifyPermutationVectors(actualP, P));
            Assert.Equal(2, exchangeCnt);

            mat = new double[4, 4]
            {
                {0,1,0,0},
                {0,0,1,0},
                {0,0,0,1},
                {1,0,0,0}
            };
            (LU, P, exchangeCnt) = MatrixUtils.LUPDecomposition(mat);
            actualLU = MatrixUtils.CreateIdentityMat(4);
            actualP = new int[4] { 3, 0, 1, 2 };
            Assert.True(VerifyMatrizes(LU, actualLU, 0));
            Assert.True(VerifyPermutationVectors(actualP, P));
            Assert.Equal(3, exchangeCnt);

        }

        [Fact]
        public void LUPDecomposeNoSwitchPossible()
        {
            double[,] mat = new double[4, 4]
            {
                {2,7,6,2 },
                {9,0,1,3 },
                {4,0,0,4 },
                {5,0,7,1 }
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.LUPDecomposition(mat));
        }

        [Fact]
        public void LUPDecomposeInvalidMatrix()
        {
            double[,] mat = new double[4, 4]
            {
                {2,7,6,2 },
                {9,5,1,3 },
                {4,0,2,4 },
                {0,0,0,0 }
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.LUPDecomposition(mat));
            mat = new double[4, 4]{

                { 0,0,0,0 },
                { 9,5,1,3 },
                { 4,0,2,4 },
                { 1,1,1,1 }
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.LUPDecomposition(mat));
        }

        [Fact]
        public void InvertInvalidSizeMatrizes()
        {
            double[,] mat = new double[0, 3];
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[3, 0];
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[2, 3];
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[3, 2];
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
        }

        [Fact]
        public void InvertIdentityMatrix()
        {
            double[,] mat = MatrixUtils.CreateIdentityMat(2);
            double[,] inverse = MatrixUtils.InvertMat(mat);
            Assert.True(VerifyMatrizes(mat, inverse, 0));
        }

        [Fact]
        public void InvertInvalidMatrizes()
        {
            double[,] mat = new double[2, 2] { { 2, 3 }, { 0, 0 } };
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[2, 2]
            {
                {-1,2},{2, -4}
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[3, 3]
            {
                {2,1,2},
                {4,2,4},
                {8,4,8}
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
            mat = new double[3, 3]
            {
                {0,0,0},{0,0,0},{0,0,0}
            };
            Assert.Throws<ArgumentException>(() => MatrixUtils.InvertMat(mat));
        }

        [Fact]
        public void InvertMatrizes()
        {
            double[,] mat = new double[3, 3]
            {
                {1,1,1},
                {2,5,4},
                {4,2,2}
            };
            double[,] result = MatrixUtils.InvertMat(mat);
            double[,] expected = new double[3, 3]
            {
                {-1,0,0.5},
                {-6,1,1},
                {8,-1,-1.5}
            };
            Assert.True(VerifyMatrizes(expected, result, 10E-5));
            mat = new double[3, 3]
            {
                {2,5,4},
                {1,1,1},
                {4,2,2}
            };
            result = MatrixUtils.InvertMat(mat);
            expected = new double[3, 3]
            {
                {0,-1,0.5},
                {1,-6,1},
                {-1,8,-1.5}
            };
            Assert.True(VerifyMatrizes(expected, result, 10E-5));
            mat = new double[3, 3] {
                { 2, 1, 4 },
                { 5, 1, 2 },
                { 4, 1, 2 }
            };
            result = MatrixUtils.InvertMat(mat);
            expected = new double[3, 3]
            {
                {0,1,-1},
                {-1,-6,8},
                {0.5,1,-1.5}
            };
            Assert.True(VerifyMatrizes(expected, result, 10E-5));
        }

        private bool VerifyPermutationVectors(int[] expP, int[] actP)
        {
            if (expP.Length != actP.Length) return false;
            for (int i = 0; i < expP.Length; i++)
            {
                Assert.Equal(expP[i], actP[i]);
            }
            return true;
        }

        private bool VerifyMatrizes(double[,] matExp, double[,] matActual, double allowedError)
        {
            if (matExp.GetLength(0) != matActual.GetLength(0) || matExp.GetLength(1) != matActual.GetLength(1)) return false;
            for (int i = 0; i < matExp.GetLength(0); i++)
            {
                for (int j = 0; j < matExp.GetLength(1); j++)
                {
                    if (allowedError > 0)
                    {
                        Assert.True(Math.Abs(matExp[i, j] - matActual[i, j]) <= allowedError);
                    }
                    else
                    {
                        Assert.Equal(matExp[i, j], matActual[i, j]);
                    }
                }
            }
            return true;
        }

        private bool VerifyMatVecMultResult(double[,] mat, double[] vec, bool colVec, double[,] result)
        {
            int rowCnt = mat.GetLength(0);
            int colCnt = mat.GetLength(1);
            if ((!colVec && (colCnt > 1 || rowCnt != vec.Length)) || (colVec && colCnt != vec.Length)) return false;
            if (colVec && (result.GetLength(1) > 1 || result.GetLength(0) != vec.Length)) return false;
            if (!colVec && (result.GetLength(0) > 1 || result.GetLength(1) != vec.Length)) return false;

            double tmpSum = 0.0;
            for (int i = 0; i < rowCnt; i++)
            {
                for (int j = 0; j < colCnt; j++)
                {
                    tmpSum += mat[i, j] * vec[j];
                }
                Assert.Equal(tmpSum, colVec ? result[i, 0] : result[0, i]);
                tmpSum = 0.0;
            }
            return true;
        }

        private bool VerifyMatMultResult(double[,] matLeft, double[,] matRight, double[,] result)
        {
            double tmpSum = 0.0;
            for (int i = 0; i < matLeft.GetLength(0); i++)
            {
                for (int k = 0; k < matRight.GetLength(1); k++)
                {
                    tmpSum = 0.0;
                    for (int j = 0; j < matLeft.GetLength(1); j++)
                    {
                        tmpSum += matLeft[i, j] * matRight[j, k];
                    }
                    Assert.Equal(tmpSum, result[i, k]);
                }
            }
            return true;
        }
    }
}
