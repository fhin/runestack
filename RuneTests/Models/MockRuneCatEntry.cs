﻿using System.Collections.Generic;

namespace RuneTests.Models
{
    public class MockRuneCatEntry
    {
        public char Letter { get; set; }
        public int NumItems { get; set; }
        public List<string> PageData { get; set; }

        public MockRuneCatEntry()
        {
            PageData = new List<string>();
        }

        public MockRuneCatEntry(char letter, int numItems)
            : this()
        {
            Letter = letter;
            NumItems = numItems;
        }
    }
}
