﻿using System.Collections.Generic;

namespace RuneTests.Models
{
    public class MockRuneCategory
    {
        public int Id { get; set; }
        public Dictionary<char, MockRuneCatEntry> LetterEntries { get; set; }
        public string CatalogueDataString;

        public MockRuneCategory()
        {
            CatalogueDataString = "";
            LetterEntries = new Dictionary<char, MockRuneCatEntry>();
        }

        public MockRuneCategory(int id, string catDataString)
            :this()
        {
            Id = id;
            CatalogueDataString = catDataString;
        }
    }
}
