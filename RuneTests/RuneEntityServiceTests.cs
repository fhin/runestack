using RuneDB.Models;
using System.Collections.Generic;
using Xunit;

namespace RuneTests
{
    public class RuneEntityServiceTests : IClassFixture<RuneEntityServiceFixture>,
        IClassFixture<RuneDataServiceFixture>, IClassFixture<RuneDbServiceFixture>
    {
        // Provides service fixtures with loaded mockup data
        readonly RuneEntityServiceFixture rEntityServFixture;
        readonly RuneDataServiceFixture rDServFixture;
        readonly RuneDbServiceFixture rDBFixture;

        public RuneEntityServiceTests(RuneEntityServiceFixture fixture, RuneDataServiceFixture dFixture
            , RuneDbServiceFixture dbFixture)
        {
            rEntityServFixture = fixture;
            rDServFixture = dFixture;
            rDServFixture.LoadMockUpData();
            rDBFixture = dbFixture;
            rEntityServFixture.AddDataService(rDServFixture, rDBFixture);
        }

        [Fact]
        public void CheckCategoryLoading()
        {
            List<RuneCategory> catList = rEntityServFixture._entityService.GetCategories();
            char[] lettersToCheck = new char[] { 'n', 'x', 'y' };
            int[][] expectedItemsPerClass = new[]{
                new int[] { 21, 4, 31 },
            };
            int numCatsToCheck = expectedItemsPerClass.GetLength(0);
            Assert.True(catList.Count >= numCatsToCheck);
            for (int k = 0; k < lettersToCheck.Length; k++)
            {
                RuneCategoryEntry catEntry = new RuneCategoryEntry();
                for (int i = 0; i < numCatsToCheck; i++)
                {
                    Assert.True(catList[i].LetterEntries != null);
                    Assert.True(catList[i].LetterEntries.ContainsKey(lettersToCheck[k]));
                    catList[i].LetterEntries.TryGetValue(lettersToCheck[k], out catEntry);
                    Assert.True(catEntry != null);
                    Assert.True(catEntry.NumItems == expectedItemsPerClass[i][k]);
                    catEntry = null;
                }
            }
        }

        [Fact]
        public void CheckItemsForCategoryLoading()
        {
            List<RuneCategory> catList = rEntityServFixture._entityService.GetCategories();
            
            Assert.True(catList != null);
            Assert.True(catList.Count >= rDServFixture.classesToCheck.Length);

            for (int i = 0; i < rDServFixture.classesToCheck.Length; i++)
            {
                RuneCategory catToCheck = catList[i];
                catToCheck.LetterEntries.Clear();

                int numItemsPerCategory = 0;
                for (int j = 0; j < rDServFixture.lettersToCheck.Length; j++)
                {
                    numItemsPerCategory += rDServFixture.expectedItemsPerClass[i][j];
                    RuneCategoryEntry entry = new RuneCategoryEntry
                    {
                        StartingLetter = rDServFixture.lettersToCheck[j],
                        NumItems = rDServFixture.expectedItemsPerClass[i][j]
                    };
                    catToCheck.LetterEntries.Add(entry.StartingLetter, entry);
                }
                List<RuneItem> items = rEntityServFixture._entityService.GetItemsForCategory(catToCheck);
                Assert.True(items != null);
                Assert.Equal(numItemsPerCategory, items.Count);
            }
        }

        [Fact]
        public void LoadPriceHist_EmptyPrefix()
        {
            RuneDataServiceFixture dataSFixture = new RuneDataServiceFixture();
            RuneDbServiceFixture dbFixture = new RuneDbServiceFixture();
            dbFixture.SetCustomData(new List<(string, int)>(), null);
            dataSFixture.SetCustomMockupData(null, new Dictionary<(string, int), string>(), null);
            RuneEntityServiceFixture entityFixture = new RuneEntityServiceFixture();
            entityFixture.AddDataService(dataSFixture, dbFixture);

            IEnumerable<RunePriceHistoryEntry> entries = entityFixture._entityService.FetchAllItemsGEPrices().GetAwaiter().GetResult();
            Assert.True(entries != null);
            Assert.Null(entries.GetEnumerator().Current);
            Assert.False(entries.GetEnumerator().MoveNext());
        }

        [Fact]
        public void LoadPriceHist_EmptyItemList()
        {
            RuneDataServiceFixture dataSFixture = new RuneDataServiceFixture();
            RuneDbServiceFixture dbFixture = new RuneDbServiceFixture();
            List<(string, int)> prefixes = new List<(string, int)>()
            {
                ("Test", 0)
            };
            dbFixture.SetCustomData(prefixes, new Dictionary<string, int>());
            dataSFixture.SetCustomMockupData(null, new Dictionary<(string, int), string>(), null);
            RuneEntityServiceFixture entityFixture = new RuneEntityServiceFixture();
            entityFixture.AddDataService(dataSFixture, dbFixture);

            IEnumerable<RunePriceHistoryEntry> entries = entityFixture._entityService.FetchAllItemsGEPrices().GetAwaiter().GetResult();
            Assert.True(entries != null);
            Assert.Null(entries.GetEnumerator().Current);
            Assert.False(entries.GetEnumerator().MoveNext());
        }

        [Fact]
        public void LoadPriceHist_InvalidPage()
        {
            // TODO:
        }

        [Fact]
        public void LoadPriceHist_SinglePage()
        {
            RuneDataServiceFixture dataSFixture = new RuneDataServiceFixture();
            dataSFixture.LoadPrefixData();
            RuneDbServiceFixture dbFixture = new RuneDbServiceFixture();
            List<(string, int)> prefixes = new List<(string, int)>()
            {
                (dataSFixture.prefixes[2], dataSFixture.prefixItemCnt[2])
            };
            string prefixName = dataSFixture.prefixes[2];
            int pageNum = dataSFixture.pageNumbers[2];
            int expectedItemCnt = dataSFixture.prefixItemCnt[2];

            dbFixture.SetCustomData(prefixes, dataSFixture.expectedPageData[(prefixName, pageNum)]);
            RuneEntityServiceFixture entityFixture = new RuneEntityServiceFixture();
            entityFixture.AddDataService(dataSFixture, dbFixture);

            IEnumerable<RunePriceHistoryEntry> entries = entityFixture._entityService.FetchAllItemsGEPrices().GetAwaiter().GetResult();
            Dictionary<string, int> expectedItems = dataSFixture.expectedPageData[(prefixName, pageNum)];
            string itemName = "";
            foreach(var histEntry in entries)
            {
                Assert.True(dataSFixture.itemIdLookUpDict.ContainsKey(histEntry.ItemId));
                itemName = dataSFixture.itemIdLookUpDict[histEntry.ItemId];
                Assert.True(expectedItems.ContainsKey(itemName));
                Assert.Equal(expectedItems[itemName], histEntry.Price);
            }
        }

        [Fact]
        public void LoadPriceHist_MultiplePages()
        {
            RuneDataServiceFixture dataSFixture = new RuneDataServiceFixture();
            dataSFixture.LoadPrefixData();
            RuneDbServiceFixture dbFixture = new RuneDbServiceFixture();
            RuneEntityServiceFixture entityFixture = new RuneEntityServiceFixture();
            entityFixture.AddDataService(dataSFixture, dbFixture);

            IEnumerable<RunePriceHistoryEntry> entries = entityFixture._entityService.FetchAllItemsGEPrices().GetAwaiter().GetResult();
            Dictionary<string, int> expectedItems = new Dictionary<string, int>();

            for (int i = 0; i < dataSFixture.prefixes.Length; i++)
            {
                foreach (var (itemName, price) in dataSFixture.expectedPageData[
                    (dataSFixture.prefixes[i], dataSFixture.pageNumbers[i])])
                {
                    expectedItems.Add(itemName, price);
                }
            }

            string actualItemName = "";
            foreach(var histEntry in entries)
            {
                Assert.True(dataSFixture.itemIdLookUpDict.ContainsKey(histEntry.ItemId));
                actualItemName = dataSFixture.itemIdLookUpDict[histEntry.ItemId];
                Assert.True(expectedItems.ContainsKey(actualItemName));
                Assert.Equal(expectedItems[actualItemName], histEntry.Price);
            }
        }
    }
}
