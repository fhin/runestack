﻿using RuneComp;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using RuneDB.Services;
using System;
using System.Collections.Generic;

namespace RuneTests
{
    public class RuneDbServiceFixture : IDisposable, IRuneDBService
    {
        private IEnumerable<(string, int)> _prefixes;
        private IDictionary<string, int> _itemDict;

        public RuneDbServiceFixture()
        {
            _prefixes = new List<(string, int)>();
            _itemDict = new Dictionary<string, int>();
        }

        public void SetCustomData(IEnumerable<(string, int)> itemPrefixes, IDictionary<string, int> itemDict)
        {
            if (itemPrefixes != null)
            {
                _prefixes = itemPrefixes;
            }
            if (itemDict != null)
            {
                _itemDict = itemDict;
            }
        }

        public void Dispose()
        {
        }
        /*
        Dictionary<string, int> IRuneDBService.FetchItemIDsAndName()
        {
            return _itemDict;
        }
        */

        public IEnumerable<(string, int)> FetchItemPrefixes()
        {
            return _prefixes;
        }

        public void StoreItems(IEnumerable<RuneItem> items)
        {
            throw new NotImplementedException();
        }

        public void StorePriceHistory(IDictionary<int, RunePriceHistoryEntry> entries, IEnumerable<int> itemIds)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(string, int)> FetchMultiItemPrefixes()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(string, int)> FetchItemsforSingleItemPrefixes()
        {
            throw new NotImplementedException();
        }

        public void StoreCategory(RuneCategory category)
        {
            throw new NotImplementedException();
        }

        public void StoreCategories(IEnumerable<RuneCategory> categories)
        {
            throw new NotImplementedException();
        }

        public bool ExistsCategory(RuneCategory category)
        {
            throw new NotImplementedException();
        }

        public void StoreRegressionModels(IEnumerable<RuneRegressionModel> models, PredictableProperty predictableProperty)
        {
            throw new NotImplementedException();
        }

        public ISet<int> FetchItemIdsHashSet()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> FetchItemIds()
        {
            throw new NotImplementedException();
        }


        public (IDictionary<string, int>, IList<object[]>) ExecuteRuneSqlQuery(RuneSqlQuery query)
        {
            throw new NotImplementedException();
        }

        public (IDictionary<string, int>, IList<object[]>) RunExecutionPlan(RuneSqlExecutionPlan plan)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, int> FetchPredictableProperties()
        {
            throw new NotImplementedException();
        }

        public (IEnumerable<(int, int)>, DateTime) FetchItemPricesAndCurrentTimeDiff(int itemId, int elemCount, bool takeRandom)
        {
            throw new NotImplementedException();
        }

        public void UpdateImageSprites(IList<int> itemIds, IList<byte[]> itemSprites)
        {
            throw new NotImplementedException();
        }
    }
}
