﻿using RuneDB;
using RuneDB.Services;
using System;

namespace RuneTests
{
    public class RuneEntityServiceFixture : IDisposable
    {
        public RuneEntityService _entityService;
        private IRuneDataService _dataService;
        private IRuneDBService _dbService;

        public RuneEntityServiceFixture()
        {
        }
        
        public void AddDataService(IRuneDataService dataService, IRuneDBService dbService)
        {
            _dataService = dataService ?? throw new ArgumentException("Cannot add dataservice");
            _dbService = dbService;
            _entityService = new RuneEntityService(_dataService, _dbService);
        }

        public void AddDBService(IRuneDBService dBService)
        {
            _dbService = dBService;
        }

        public void ClearDbService()
        {
            _dbService = null;
        }

        public void Dispose()
        {
            
        }
    }
}
