﻿using RuneDB.Models;
using RuneDB.Services;
using RuneTests.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RuneTests
{
    // Further infos: https://xunit.net/docs/shared-context
    public class RuneDataServiceFixture : IDisposable, IRuneDataService
    {
        private string categoryCatalogueData = Properties.Resources.CategoryCatalogue_testData;
        private string itemsForCategoriesData = Properties.Resources.Items_ForCategory_testData;
        private string webPageItemPrefixData = Properties.Resources.WebPageItemPrefix_testData;

        private Dictionary<int, MockRuneCategory> categoryData;
        private Dictionary<(string, int), string> itemPrefixPageData;
        private readonly int catalogueLetterEntries = 26;

        public int[] classesToCheck = new int[] { 1 };
        public char[] lettersToCheck = new char[] { 'n', 'x', 'y' };
        public int[][] expectedItemsPerClass = new[]{
                new int[] { 21, 4, 31 },
        };
     
        public string[] prefixes = new string[] {"Rune", "Rune", "Veg" };
        public int[] pageNumbers = new int[] { 1, 2, 1 };
        public int[] prefixItemCnt = new int[] { 20, 9, 3 };
        public Dictionary<(string, int), Dictionary<string, int>> expectedPageData;
        public Dictionary<int, string> itemIdLookUpDict;

        private readonly (string, int, int)[][] expectedItemsAndPricesPerPage = new[]
        {
            new (string, int, int)[] {
                ("Air rune", 556, 5), ("Dust rune",4696, 4), ("Earth rune", 557, 4), ("Fire rune", 554, 5),
                ("Lava rune", 4699, 4), ("Mist rune", 4695, 101), ("Mude rune", 4698, 285), ("Rune arrow", 892, 67),
                ("Rune brutal", 4803, 1034), ("Rune dart", 811, 1028), ("Rune dart(p)", 817, 1004), ("Rune dart(p+)", 5634, 1622),
                ("Rune dart(p++)", 5641, 1980), ("Rune defender ornament kit", 23227, 44200), ("Rune essence", 1436, 9),
                ("Rune full helm (g)", 2619, 92200), ("Rune full helm (t)", 2627, 29800), ("Rune halberd", 3202, 37800),
                ("Rune hasta(p)", 11414, 28700), ("Rune hasta(p+)", 11417, 23700),
            },
            new (string, int, int)[]
            {

                ("Rune hasta(p++)",11419, 19700), ("Rune helm (h1)", 10286, 41100), ("Rune javelin", 830, 179), ("Rune javelin(p)", 836, 379),
                ("Rune javelin(p+)", 5647, 448),("Rune javelin(p++)", 5653, 1051), ("Rune kiteshield (g)", 1201, 114500),
                ("Rune scimitar ornament kit (guthix)", 23321, 97900), ("Rune scimitar ornament kit (saradmonin)", 23324, 244700)
            },
            new (string, int, int)[] {
                ("Premade veg ball",2235, 44), ("Premade veg batta", 2227, 40), ("Veg ball", 2195, 979),
            },
        };

        public RuneDataServiceFixture()
        {
            categoryData = new Dictionary<int, MockRuneCategory>();
            itemPrefixPageData = new Dictionary<(string, int), string>();
            expectedPageData = new Dictionary<(string, int), Dictionary<string, int>>();
            itemIdLookUpDict = new Dictionary<int, string>();
        }

        private void SetExpectedPageData()
        {
            try
            {
                for (int i = 0; i < prefixes.Length; i++)
                {
                    Dictionary<string, int> entries = new Dictionary<string, int>();
                    foreach (var (itemName, itemId, price) in expectedItemsAndPricesPerPage[i])
                    {
                        entries.Add(itemName, price);
                        itemIdLookUpDict.Add(itemId, itemName);
                    }
                    expectedPageData.Add((prefixes[i], pageNumbers[i]), entries);
                }
            }
            catch (Exception)
            {

            }
        }

        public void SetCustomMockupData(Dictionary<int, MockRuneCategory> customCategoryData,
            Dictionary<(string, int), string> customPrefixData, Dictionary<(string, int), Dictionary<string, int>> expPageData)
        {
            if (customCategoryData != null)
            {
                categoryData = customCategoryData;
            }
            if (customPrefixData != null)
            {
                itemPrefixPageData = customPrefixData;
            }
            if (expPageData != null)
            {
                expectedPageData = expPageData;
            }
        }

        public void LoadMockUpData()
        {
            try
            {
                LoadCatalogueData();
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                LoadItemsForCategoryData();
            }
            catch (Exception e)
            {
                throw e;
            }
            
            try
            {
                LoadPrefixData();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private void LoadCatalogueData()
        {
            Regex categoryCatalogueRegex = new Regex(@"^(\d_)", RegexOptions.Compiled);
            // Original regex: ("[a-zA-Z]","items":(\d*))
            Regex categoryCatEntryRegex = new Regex(@"(""[a-z|A-Z]"",""items"":(\d+))", RegexOptions.Compiled);
            if (string.IsNullOrWhiteSpace(categoryCatalogueData)) return;
            string[] lines = Properties.Resources.CategoryCatalogue_testData.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                Match m = categoryCatalogueRegex.Match(lines[i]);
                if (m == null || m.Value.Length != 2) continue;

                string[] matchData = lines[i].Split('_');
                int classIdx = -1;
                int.TryParse(matchData[0], out classIdx);
                if (classIdx == -1) continue;

                if (!categoryData.ContainsKey(classIdx))
                {
                    categoryData.Add(classIdx, new MockRuneCategory(classIdx, matchData[1]));
                }

                MockRuneCategory cat = null;
                categoryData.TryGetValue(classIdx, out cat);
                if (cat == null) break;
                foreach (Match catEntryMatch in categoryCatEntryRegex.Matches(matchData[1]))
                {
                    matchData = catEntryMatch.Value.Split(',');
                    char startingLetter = matchData[0].Replace("\"", "")[0];
                    int numItems = 0;
                    int.TryParse(matchData[1].Replace("\"items\":", ""), out numItems);

                    if (!cat.LetterEntries.ContainsKey(startingLetter))
                    {
                        MockRuneCatEntry catEntry = new MockRuneCatEntry(startingLetter, numItems);
                        cat.LetterEntries.Add(startingLetter, catEntry);
                    }
                }
                categoryData[classIdx] = cat;
                cat = null;
            }
            VerifyCatalogueData();
        }

        private void LoadItemsForCategoryData()
        {
            if (string.IsNullOrWhiteSpace(itemsForCategoriesData)) return;
            Regex reg = new Regex(@"^(\d:[a-z|A-B]_)", RegexOptions.Compiled);
            string[] lines = itemsForCategoriesData.Split('\n');
            foreach (var line in lines)
            {
                int classIdx = -1;
                char startLetter = '-';
                try
                {
                    Match m = reg.Match(line);
                    if (m == null) continue;
                    string[] matchData = m.Value.Split(':');
                    int.TryParse(matchData[0], out classIdx);
                    startLetter = matchData[1][0];

                    if (classIdx >= 0 && startLetter != '-')
                    {
                        if (!categoryData.ContainsKey(classIdx))
                        {
                            categoryData.Add(classIdx, new MockRuneCategory(classIdx, ""));
                        }
                        if (!categoryData[classIdx].LetterEntries.ContainsKey(startLetter))
                        {
                            MockRuneCatEntry mockRuneCatEntry = new MockRuneCatEntry(startLetter, 0);
                            categoryData[classIdx].LetterEntries.Add(startLetter, mockRuneCatEntry);
                        }
                        // Remove classIdx:startLetter_ prefix from data string
                        string dataString = line.Remove(m.Index, m.Length);
                        categoryData[classIdx].LetterEntries[startLetter].PageData.Add(dataString);
                    }
                }
                catch (Exception)
                {
                }
            }
            VerifyItemsForCategoryData();
        }

        public void LoadPrefixData()
        {
            if (string.IsNullOrWhiteSpace(webPageItemPrefixData)) return;
            Regex prefixPageRegex = new Regex(@"^(\n*[a-z|A-Z]+:\d+:)", RegexOptions.Compiled);
            string delimiter = "##delimiter##";
            string entryDelimiter = "##entryEnd##";
            string[] lines = webPageItemPrefixData.Split(entryDelimiter);
            foreach (var line in lines)
            {
                string[] lineData = line.Split(delimiter);
                if (lineData == null || lineData.Length != 2) continue;
                Match m = prefixPageRegex.Match(lineData[0]);
                if (m == null) continue;
                string[] prefixData = m.Value.Split(':');

                var (prefixName, pageNum) = (prefixData[0], -1);
                int.TryParse(prefixData[1], out pageNum);

                if (pageNum == -1 || string.IsNullOrWhiteSpace(prefixName) ||
                    itemPrefixPageData.ContainsKey((prefixName, pageNum)))
                {
                    continue;
                }
                prefixName = prefixName.Replace("\n", "");
                itemPrefixPageData.Add((prefixName, pageNum), lineData[1]);
            }
            VerifyPrefixData();
            SetExpectedPageData();
        }

        private void VerifyItemsForCategoryData()
        {
            Debug.Assert(categoryData != null);
            Debug.Assert(categoryData.Count == 1);
            int classIdx = classesToCheck[0];
            Debug.Assert(categoryData[classIdx] != null);
            Debug.Assert(categoryData[classIdx].LetterEntries.Count >= 3);
            Debug.Assert(categoryData[classIdx].LetterEntries['n'].PageData.Count == 2);
            Debug.Assert(categoryData[classIdx].LetterEntries['x'].PageData.Count == 1);
            Debug.Assert(categoryData[classIdx].LetterEntries['y'].PageData.Count == 3);
        }

        private void VerifyCatalogueData()
        {
            Debug.Assert(categoryData != null && categoryData.Count == classesToCheck.Length);
            for (int i = 0; i < classesToCheck.Length; i++)
            {
                Debug.Assert(categoryData[classesToCheck[i]].LetterEntries.Count == catalogueLetterEntries);
            }
            for (int j = 0; j < lettersToCheck.Length; j++)
            {
                for (int k = 0; k < classesToCheck.Length; k++)
                {
                    int classIdx = classesToCheck[k];
                    try
                    {
                        Debug.Assert(categoryData[classIdx].LetterEntries.ContainsKey(lettersToCheck[j]));
                        MockRuneCatEntry catEntry = null;
                        categoryData[classIdx].LetterEntries.TryGetValue(lettersToCheck[j], out catEntry);
                        Debug.Assert(catEntry.NumItems == expectedItemsPerClass[k][j]);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }

        private void VerifyPrefixData()
        {
            string[] prefixNames = { "Rune", "Veg" };
            int[] numPages = { 2, 1 };
            Debug.Assert(itemPrefixPageData != null && itemPrefixPageData.Count == 3);
            for (int j = 0; j < prefixNames.Length; j++)
            {
                for (int i = 0; i < numPages[j]; i++)
                {
                    Debug.Assert(itemPrefixPageData.ContainsKey((prefixNames[j], i+1)));
                    Debug.Assert(!string.IsNullOrWhiteSpace(itemPrefixPageData[(prefixNames[j], i+1)]));
                }
            }
        }

        public void Dispose()
        {
        }

        public Task<string> FetchCategoryData(int categoryId)
        {
            if (!categoryData.ContainsKey(categoryId)) return Task.FromResult("");
            MockRuneCategory cat = null;
            categoryData.TryGetValue(categoryId, out cat);
            if (cat == null) return Task.FromResult("");
            return Task.FromResult(cat.CatalogueDataString);
        }

        public string FetchItemBasicData(int itemId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> FetchItemsBasicDataForCategory(RuneCategory category)
        {
            List<string> responseData = new List<string>();
            if (category == null || !categoryData.ContainsKey(category.Id)) return responseData;
            MockRuneCategory cat = null;
            categoryData.TryGetValue(category.Id, out cat);
            foreach (KeyValuePair<char, MockRuneCatEntry> kVEntry in cat.LetterEntries)
            {
                responseData.AddRange(kVEntry.Value.PageData);
            }
            return responseData;
        }

        public IEnumerable<string> FetchItemsBasicData(IEnumerable<int> itemIds)
        {
            throw new NotImplementedException();
        }

        public byte[] FetchItemBigSprite(int itemId)
        {
            return new byte[0];
        }

        public Task<string> FetchAsyncPageData(int categoryId, char letter, int page)
        {
            string response = "";
            try
            {
                response = categoryData[categoryId].LetterEntries[letter].PageData[page-1];
            }
            catch (Exception)
            {

            }
            return Task.FromResult(response);
        }

        public Task<string> FetchAsyncItemAdvData(int itemId)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> FetchAsyncItemBigSprite(int itemId)
        {
            return Task.FromResult(FetchItemBigSprite(itemId));
        }

        public Task<string> FetchAsyncWebPageData(string prefix, int page)
        {
            string response = "";
            if (itemPrefixPageData == null || itemPrefixPageData.Count == 0) return Task.FromResult(response);
            if (!string.IsNullOrWhiteSpace(prefix) && page >= 0)
            {
                itemPrefixPageData.TryGetValue((prefix, page), out response);
            }
            return Task.FromResult(response);
        }

        public int FetchItemGExPrice(int itemId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> FetchItemsGePrices(IEnumerable<int> itemIds)
        {
            throw new NotImplementedException();
        }

        public Task<string> FetchItemPrice(int itemId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Task<string>> FetchAsyncAllCategoryPageData(RuneCategory category)
        {
            throw new NotImplementedException();
        }
    }
}
