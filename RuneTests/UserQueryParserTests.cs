﻿using RuneComp;
using System;
using Xunit;

namespace RuneTests
{
    public class UserQueryParserTests
    {
        [Fact]
        public void Query_Empty()
        {
            UserQueryParser parser = new UserQueryParser();
            Assert.Throws<ArgumentException>(() => parser.ParseQuery(""));
        }

        [Theory]
        [InlineData("invalid_op")]
        [InlineData("")]
        [InlineData(" ")]
        public void Query_InvalidOperation(string queryOp)
        {
            string query = queryOp + " name of items";
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.False(parser.IsValidQuery);
            Assert.True(parser.UserQuery.QueryOp == QueryOp.None);
            Assert.NotNull(parser.Errors);
            Assert.True(parser.Errors.Count == 1);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("invalid_prop")]
        [InlineData("prIce")]
        [InlineData("1234")]
        public void Query_InvalidPropName(string propertyName)
        {
            string query = "show " + propertyName + " of items";
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.False(parser.IsValidQuery);
            Assert.True(parser.UserQuery.QueryOp == QueryOp.show);
            Assert.NotNull(parser.Errors);
            Assert.True(parser.Errors.Count == 1);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("\"Rune sword\"")]
        [InlineData("\"Rune*\"")]
        [InlineData("\"*Rune\"")]
        [InlineData("\"Ru*ne\"")]
        public void Query_InvalidPreSelector(string preSelector)
        {
            string query = "show item " + preSelector;
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.False(parser.IsValidQuery);
            Assert.NotNull(parser.Errors);
            Assert.True(parser.Errors.Count == 1);
            Assert.True(parser.UserQuery.QueryOp == QueryOp.show);
        }

        [Theory]
        [InlineData("\"Rune_sword\"", "Rune sword")]
        [InlineData("\"Rune\"", "Rune")]
        [InlineData("\"Rune_2h_long_sword\"", "Rune 2h long sword")]
        public void Query_ValidPreSelector_NoFilters(string preSelector, string expectedPreSelector)
        {
            string query = "show item " + preSelector;
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);
            Assert.NotNull(parser.Errors);
            Assert.True(parser.Errors.Count == 0);
            Assert.True(parser.UserQuery.QueryOp == QueryOp.show);
            Assert.Equal(expectedPreSelector, parser.UserQuery.PreSelector);
        }

        [Fact]
        public void Query_NoProps_NoPreSelector_NoFilters()
        {
            string query = "show items";
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);

            Query q = parser.UserQuery;
            Assert.NotNull(q);
            Assert.True(q.QueryOp == QueryOp.show);
            Assert.NotNull(parser.Errors);
            Assert.True(parser.Errors.Count == 0);

            Assert.Equal("", q.PreSelector);
            Assert.True(q.Props != null && q.Props.Count == 0);
            Assert.True(q.Filters != null && q.Filters.Count == 0);
        }

        [Theory]
        [InlineData("name", false)]
        [InlineData("description", false)]
        [InlineData("price", true)]
        [InlineData("high alch value", true)]
        [InlineData("low alch value", true)]
        public void Query_OneProp_NoPreSelector_NoFilters(string propertyName, bool isNumeric)
        {
            string query = "show " + propertyName + " of items";
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);

            Query q = parser.UserQuery;
            Assert.NotNull(q);
            Assert.True(q.QueryOp == QueryOp.show);
            Assert.Equal("", q.PreSelector);

            Assert.NotNull(q.Props);
            Assert.Single(q.Props);
            Assert.Equal(propertyName, q.Props[0].PropertyName);
            Assert.Equal(isNumeric, q.Props[0].IsNumeric);
        }

        [Theory]
        [InlineData("name", false, "\"Rune_sword\"")]
        [InlineData("description", false, "\"Rune_sword\"")]
        [InlineData("price", true, "\"Rune_sword\"")]
        [InlineData("high alch value", true, "\"Rune_sword\"")]
        [InlineData("low alch value", true, "\"Rune_sword\"")]
        public void Query_OneProp_ValidPreSelector_NoFilters(string propertyName, bool isNumeric, string preSelector)
        {
            string query = "show " + propertyName + " of item " + preSelector;
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);

            Query q = parser.UserQuery;
            Assert.NotNull(q);
            Assert.True(q.QueryOp == QueryOp.show);
            Assert.NotNull(q.Props);
            Assert.Single(q.Props);
            Assert.Equal(propertyName, q.Props[0].PropertyName);
            Assert.Equal(isNumeric, q.Props[0].IsNumeric);

            if (preSelector.Contains('_'))
            {
                preSelector = preSelector.Replace('_', ' ');
            }
            if (preSelector.Contains('"'))
            {
                preSelector = preSelector.Replace("\"", "");
            }
            Assert.Equal(preSelector, q.PreSelector);

        }

        [Theory]
        [MemberData(nameof(PredictionData))]
        public void Query_OnePropPrediction_NoFilters(string propertyName, string predictionUnitAsString,
            int value, PredictionTimeUnit tUnit, DateTime futureDate)
        {
            string query = "";
            string predictionAsString = "";
            switch (tUnit)
            {
                case PredictionTimeUnit.Day: case PredictionTimeUnit.Hour: case PredictionTimeUnit.Month:
                    predictionAsString = " in " + value.ToString() + predictionUnitAsString;
                    break;
                default:
                    predictionAsString = " on " + futureDate.ToString("dd:MM:yyyyTHH") + predictionUnitAsString;
                    break;
            }
            string queryFormat = "show {0} {1} of items";
            query = string.Format(queryFormat, propertyName, predictionAsString);
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);
            Assert.Equal(0, parser.Errors.Count);
            Assert.NotNull(parser.UserQuery);

            Query userQuery = parser.UserQuery;
            Assert.Equal("", userQuery.PreSelector);
            Assert.Equal(QueryOp.show, userQuery.QueryOp);
            Assert.True(userQuery.Props != null && userQuery.Props.Count == 1);
            Assert.True(userQuery.Filters != null && userQuery.Filters.Count == 0);
            
            Property predProp = userQuery.Props[0];
            Assert.Equal(propertyName, predProp.PropertyName);
            Assert.True(predProp.IsNumeric);

            Assert.NotNull(predProp.Prediction);
            Prediction prediction = predProp.Prediction;
            Assert.Equal(tUnit, prediction.TUnit);
            Assert.Equal(value, prediction.TValue);
            if (futureDate != null)
            {
                Assert.Equal(futureDate, prediction.FutureDate);
            }
        }

        public static System.Collections.Generic.IEnumerable<object[]> PredictionData =>
                new[]
                {
                    new object[] {"price", "h", 2, PredictionTimeUnit.Hour, DateTime.MinValue},
                    new object[] {"price", "hours", 2, PredictionTimeUnit.Hour, DateTime.MinValue},
                    new object[] {"price", "d", 2, PredictionTimeUnit.Day, DateTime.MinValue},
                    new object[] {"price", "day", 2, PredictionTimeUnit.Day, DateTime.MinValue},
                    new object[] {"price", "days", 2, PredictionTimeUnit.Day, DateTime.MinValue},
                    new object[] {"price", "mon", 2, PredictionTimeUnit.Month, DateTime.MinValue},
                    new object[] {"price", "months", 2, PredictionTimeUnit.Month, DateTime.MinValue},
                    new object[] {"price", "", 0, PredictionTimeUnit.None, new DateTime(2020,2,15,15,0,0)},
                    new object[] {"price", "h", 0, PredictionTimeUnit.None, new DateTime(2020,2,15,15,0,0)}
                };

        [Theory]
        [InlineData(new string[2] { "and", "," }, new string[2] { "name", "price" }, new bool[2] { false, true })]
        [InlineData(new string[2] { "and", "," }, new string[3] { "price", "description", "low alch value" }, new bool[3] { true, false, true })]
        [InlineData(new string[2] { "and", "," }, new string[3] { "low alch value", "high alch value", "low alch value" }, new bool[3] { true, true, true })]
        [InlineData(new string[2] { "and", "," },
            new string[6] { "name", "description", "price", "price change", "low alch value", "high alch value" },
            new bool[6] { false, false, true, true, true, true })]
        public void Query_MultipleValidProps_NoPreSelector_NoFilters(string[] propertySeperators,
            string[] propNames, bool[] propsNumeric)
        {
            int propsToCheck = propNames.Length;
            string query = "show {0} of items";

            for (int i = 0; i < propertySeperators.Length; i++)
            {
                string seperator = " " + propertySeperators[i] + " ";
                query = string.Format(query, string.Join(seperator, propNames));
                UserQueryParser parser = new UserQueryParser();
                parser.ParseQuery(query);
                Assert.True(parser.IsValidQuery);

                Query q = parser.UserQuery;
                Assert.NotNull(q);
                Assert.NotNull(q.Props);
                Assert.True(q.QueryOp == QueryOp.show);
                Assert.Equal("", q.PreSelector);
                Assert.True(q.Filters != null && q.Filters.Count == 0);
                Assert.Equal(propsToCheck, q.Props.Count);

                for (int j = 0; j < propsToCheck; j++)
                {
                    Assert.Equal(propNames[j], q.Props[j].PropertyName);
                    Assert.Equal(propsNumeric[j], q.Props[j].IsNumeric);
                }
            }
        }

        [Theory]
        [InlineData(new string[1] { "" }, new string[1] { "name" }, new bool[1] { false }, "\"Rune_sword\"", "Rune sword")]
        [InlineData(new string[1] { "" }, new string[1] { "price" }, new bool[1] { true }, "\"Rune_sword\"", "Rune sword")]
        [InlineData(new string[2] { "and", "," },
            new string[3] { "price", "description", "low alch value" },
            new bool[3] { true, false, true }, "\"Rune_sword\"", "Rune sword")]
        [InlineData(new string[2] { "and", "," },
            new string[3] { "low alch value", "high alch value", "low alch value" },
            new bool[3] { true, true, true }, "\"Rune_2h_long_sword\"", "Rune 2h long sword")]
        [InlineData(new string[2] { "and", "," },
            new string[6] { "name", "description", "price", "price change", "low alch value", "high alch value" },
            new bool[6] { false, false, true, true, true, true }, "\"Rune_sword\"", "Rune sword")]
        public void Query_Properties_ValidPreSelector_NoFilters(string[] propertySeperators,
            string[] propNames, bool[] propsNumeric, string preSelector, string expectedPreSelector)
        {
            int propsToCheck = propNames.Length;
            string query = "show {0} of item " + preSelector;

            for (int i = 0; i < propertySeperators.Length; i++)
            {
                string seperator = " " + propertySeperators[i] + " ";
                query = string.Format(query, string.Join(seperator, propNames));
                UserQueryParser parser = new UserQueryParser();
                parser.ParseQuery(query);
                Assert.True(parser.IsValidQuery);

                Query q = parser.UserQuery;
                Assert.NotNull(q);
                Assert.NotNull(q.Props);
                Assert.True(q.QueryOp == QueryOp.show);
                Assert.Equal(expectedPreSelector, q.PreSelector);
                Assert.True(q.Filters != null && q.Filters.Count == 0);
                Assert.Equal(propsToCheck, q.Props.Count);

                for (int j = 0; j < propsToCheck; j++)
                {
                    Assert.Equal(propNames[j], q.Props[j].PropertyName);
                    Assert.Equal(propsNumeric[j], q.Props[j].IsNumeric);
                }
            }
        }

        [Theory]
        [InlineData(new Operation[] { Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "price", 200)]
        [InlineData(new Operation[] { Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "price change", 200)]
        [InlineData(new Operation[] { Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "low alch value", 200)]
        [InlineData(new Operation[] { Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "high alch value", 200)]
        public void Query_NoProps_OneIntegerFilter(Operation[] operations, string propertyName, int filterValue)
        {
            string queryForamt = "show items with a {0} {1} {2}";
            foreach (Operation op in operations)
            {
                string query = string.Format(queryForamt, new object[]{propertyName, Enums.GetOperationAsStringSymbol(op),
                    filterValue.ToString() });

                UserQueryParser parser = new UserQueryParser();
                parser.ParseQuery(query);
                Assert.True(parser.IsValidQuery);

                Query q = parser.UserQuery;
                Assert.NotNull(q);
                Assert.True(q.QueryOp == QueryOp.show);
                Assert.NotNull(q.Props);
                Assert.Empty(q.Props);

                Assert.NotNull(q.Filters);
                Assert.Single(q.Filters);

                Filter f = q.Filters[0];
                Assert.NotNull(f);
                Assert.NotNull(f.Property);

                Property filterProp = f.Property;
                Assert.NotNull(filterProp);
                Assert.True(filterProp.IsNumeric);
                Assert.Equal(propertyName, filterProp.PropertyName);
                Assert.Null(filterProp.Prediction);

                Condition c = f.Condition;
                Assert.NotNull(c);
                Assert.Equal(ConditionValType.Integer, c.CondType);
                Assert.Equal(op, c.Op);
                Assert.Equal(filterValue, c.IVal);
                Assert.Equal("", c.SVal);
                Assert.Equal(0.0, c.DVal);
            }
        }

        [Theory]
        [InlineData(new Operation[] { Operation.Ending_With, Operation.Starting_With, Operation.Like,
            Operation.None}, "price", 200)]
        [InlineData(new Operation[] {  Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "price", 200.75)]
        [InlineData(new Operation[] {  Operation.Equal, Operation.Smaller,
            Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq}, "price", "Test_value")]
        public void Query_NoProps_InvalidIntegerFilter(Operation[] operations, string propertyName, object filterValue)
        {
            string queryFormat = "show items with a {0} {1} {2}";
            foreach (Operation op in operations)
            {
                string query = "";
                if (Enums.IsOperationNumeric(op))
                {
                    query = string.Format(queryFormat, new object[]{propertyName, Enums.GetOperationAsStringSymbol(op),
                    filterValue.ToString() });
                }
                else
                {
                    query = string.Format(queryFormat, new object[]{propertyName,
                        op.ToString().Replace("_", " ").ToLower(), filterValue.ToString() });
                }
                UserQueryParser parser = new UserQueryParser();
                parser.ParseQuery(query);
                Assert.False(parser.IsValidQuery);
            }
        }

        [Theory]
        [InlineData(Operation.Equal, "name", "\"Pirate_bandana\"", "Pirate_bandana")]
        [InlineData(Operation.Starting_With, "name", "\"Pirate\"", "Pirate")]
        [InlineData(Operation.Starting_With, "name", "\"Pirate*\"", "Pirate*")]
        [InlineData(Operation.Ending_With, "name", "\"bandana\"", "bandana")]
        [InlineData(Operation.Ending_With, "name", "\"*bandana\"", "*bandana")]
        [InlineData(Operation.Like, "name", "\"Pirate\"", "Pirate")]
        [InlineData(Operation.Like, "name", "\"*rate*\"", "*rate*")]
        [InlineData(Operation.Like, "name", "\"Rune_*_sword\"", "Rune_*_sword")]
        [InlineData(Operation.Like, "name", "\"Rune_*h_sword\"", "Rune_*h_sword")]
        public void Query_NoProp_OneStringFilter(Operation op,
            string filterPropertyName, string filterValue, string expectedFilterValue)
        {
            string queryFormat = "show items with a {0} {1} {2}";
            string operandAsString = op == Operation.Equal ? "=" : op.ToString().Replace("_", " ").ToLower();
            string query = string.Format(queryFormat, new object[] { filterPropertyName, operandAsString, filterValue.ToString() });

            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);
            Assert.Equal(0, parser.Errors.Count);

            Query q = parser.UserQuery;
            Assert.NotNull(q);
            Assert.NotNull(q.Props);
            Assert.True(q.QueryOp == QueryOp.show);

            Assert.NotNull(q.Filters);
            Assert.Single(q.Filters);
            Property p = q.Filters[0].Property;
            Condition c = q.Filters[0].Condition;
            Assert.NotNull(p);
            Assert.NotNull(c);

            Assert.False(p.IsNumeric);
            Assert.Equal(filterPropertyName, p.PropertyName);
            Assert.Null(p.Prediction);
            Assert.Equal(ConditionValType.String, c.CondType);
            Assert.Equal(op, c.Op);
            Assert.Equal(expectedFilterValue, c.SVal);
            Assert.Equal(0, c.IVal);
            Assert.Equal(0.0, c.DVal);
        }

        [Theory]
        [InlineData(Operation.Starting_With, "name", "\"*bandana\"")]
        [InlineData(Operation.Starting_With, "name", "\"band*ana\"")]
        [InlineData(Operation.Starting_With, "name", "\"****\"")]
        [InlineData(Operation.Starting_With, "name", "\"\"")]
        [InlineData(Operation.Ending_With, "name", "\"bandana*\"")]
        [InlineData(Operation.Ending_With, "name", "\"band*ana\"")]
        [InlineData(Operation.Ending_With, "name", "\"****\"")]
        [InlineData(Operation.Ending_With, "name", "\"\"")]
        [InlineData(Operation.Like, "name", "\"*test*value*\"")]
        [InlineData(Operation.Like, "name", "\"**\"")]
        [InlineData(Operation.Like, "name", "\"test***value\"")]
        [InlineData(Operation.Like, "name", "\"R*ne_2h_sw*\"")]
        public void Query_InvalidStringFilter_MissplacedWildcard(Operation op, string propertyName, string filterValue)
        {
            string queryFormat = "show items with a {0} {1} {2}";
            string query = string.Format(queryFormat, new object[]{propertyName,
                        op.ToString().Replace("_", " ").ToLower(), filterValue.ToString() });
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.False(parser.IsValidQuery);
        }

        [Theory]
        [InlineData(new Operation[] { Operation.Starting_With, Operation.Equal }, 
            new string[2] { "name", "price" }, new object[2] { "\"Pirate*\"", 200})]
        [InlineData(new Operation[] { Operation.Ending_With, Operation.Equal }, new string[2] { "name", "price" }, new object[2] { "\"*bandana\"", 200})]
        [InlineData(new Operation[] { Operation.Like, Operation.Equal }, new string[2] { "name", "price" }, new object[2] { "\"*rate*\"", 200})]
        [InlineData(new Operation[] { Operation.Equal, Operation.Equal }, new string[2] { "name", "price"}, new object[2] { "\"Pirate_bandana\"", 200})]
        public void Query_NoProp_PreSelector_OneStringfilter(Operation[] ops, string[] filterPropNames, object[] filterPropValues)
        {
            int numFilters = ops.Length;
            string query = "show items with a ";
            string queryFormat = "{0} {1} {2}";

            for (int i = 0; i < numFilters; i++)
            {
                string opAsString = "";
                if (Enums.IsOperationNumeric(ops[i]))
                {
                    opAsString = Enums.GetOperationAsStringSymbol(ops[i]);
                }
                else
                {
                    opAsString = ops[i].ToString().Replace("_", " ").ToLower();
                }
                query += string.Format(queryFormat, new object[]{filterPropNames[i]
                        , opAsString, filterPropValues.ToString() });

                if (i + 1 < ops.Length)
                {
                    query += " and a ";
                }
            }
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.False(parser.IsValidQuery);
            Assert.Equal(1, parser.Errors.Count);
        }

        [Theory]
        [InlineData(new Operation[]
            { Operation.Like, Operation.Smaller },
            new ConditionValType[] { ConditionValType.String, ConditionValType.Integer },
            new string[] { "name", "price" },
            new bool[] { false, true },
            new object[] { "\"Pirate_bandana\"", 200 },
            new object[] { "Pirate_bandana", 200 })]
        [InlineData(new Operation[]
            { Operation.Smaller, Operation.Like, Operation.Greater_Eq },
            new ConditionValType[] { ConditionValType.Integer, ConditionValType.String, ConditionValType.Double },
            new string[] { "price", "name", "price" },
            new bool[] { true, false, true },
            new object[] { 200, "\"Pirate_bandana\"", 100.75 },
            new object[] { 200, "Pirate_bandana", 100.75 })]
        [InlineData(new Operation[]
            { Operation.Starting_With, Operation.Smaller, Operation.Ending_With },
            new ConditionValType[] { ConditionValType.String, ConditionValType.Integer, ConditionValType.String },
            new string[] { "name", "price", "name" },
            new bool[] { false, true, false },
            new object[] { "\"Pirate*\"", 200, "\"*bandana\"" },
            new object[] { "Pirate*", 200, "*bandana" })]
        public void Query_MixedFilters(Operation[] ops, ConditionValType[] condValTypes,
            string[] propertyNames, bool[] isNumeric, object[] filterValues, object[] expectedFilterVals)
        {
            Assert.True(ops.Length == propertyNames.Length && ops.Length == filterValues.Length
                && ops.Length == isNumeric.Length && ops.Length == condValTypes.Length);
            int numFilters = ops.Length;
            string query = "show items with a ";
            string queryFormat = "{0} {1} {2}";

            for (int i = 0; i < numFilters; i++)
            {
                if (Enums.IsOperationNumeric(ops[i]))
                {
                    string filterValue = condValTypes[i] == ConditionValType.Double
                        ? ((double)filterValues[i]).ToString(System.Globalization.CultureInfo.InvariantCulture)
                        : filterValues[i].ToString();
                    query += string.Format(queryFormat, new object[]{propertyNames[i]
                        , Enums.GetOperationAsStringSymbol(ops[i]), filterValue });
                }
                else
                {
                    query += string.Format(queryFormat, new object[]{propertyNames[i],
                        ops[i].ToString().Replace("_", " ").ToLower(), filterValues[i].ToString() });
                }
                if (i + 1 < ops.Length)
                {
                    query += " and a ";
                }
            }
            UserQueryParser parser = new UserQueryParser();
            parser.ParseQuery(query);
            Assert.True(parser.IsValidQuery);
            Assert.NotNull(parser);
            Assert.NotNull(parser.UserQuery);

            Query userQuery = parser.UserQuery;
            Assert.True(userQuery.QueryOp == QueryOp.show);
            Assert.NotNull(userQuery.Filters);
            Assert.Equal(0, parser.Errors.Count);
            Assert.Equal(numFilters, userQuery.Filters.Count);

            Property currFilterProp;
            Condition currFilterCond;
            for (int i = 0; i < numFilters; i++)
            {
                currFilterProp = userQuery.Filters[i].Property;
                Assert.Equal(propertyNames[i], currFilterProp.PropertyName);
                Assert.Equal(isNumeric[i], currFilterProp.IsNumeric);
                Assert.Null(currFilterProp.Prediction);

                currFilterCond = userQuery.Filters[i].Condition;
                Assert.Equal(condValTypes[i], currFilterCond.CondType);
                Assert.Equal(ops[i], currFilterCond.Op);
                switch (condValTypes[i])
                {
                    case ConditionValType.String:
                        Assert.Equal(expectedFilterVals[i], currFilterCond.SVal);
                        Assert.Equal(0, currFilterCond.IVal);
                        Assert.Equal(0.0, currFilterCond.DVal);
                        break;
                    case ConditionValType.Integer:
                        Assert.Equal("", currFilterCond.SVal);
                        Assert.Equal(expectedFilterVals[i], currFilterCond.IVal);
                        Assert.Equal(0.0, currFilterCond.DVal);
                        break;
                    case ConditionValType.Double:
                        Assert.Equal("", currFilterCond.SVal);
                        Assert.Equal(0, currFilterCond.IVal);
                        Assert.Equal(expectedFilterVals[i], currFilterCond.DVal);
                        break;
                    default:
                        Assert.True(false);
                        break;
                }
            }
        }
    }
}
