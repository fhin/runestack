﻿using RuneDB.Models.RegressionModels;
using RuneDB.Utils;
using System;
using Xunit;

namespace RuneTests
{
    // Regression coefficients obtained from 
    // https://arachnoid.com/polysolve/
    //
    public class RegressionTests
    {
        [Fact]
        public void RegressionModelFromEmptyData()
        {
            double[] inputVals = new double[0];
            double[] outputVals = new double[2];
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(5, inputVals, outputVals));

            inputVals = new double[2];
            outputVals = new double[0];
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(5, inputVals, outputVals));
        }

        [Fact]
        public void RegressionModelForInvalidDegree()
        {
            double[] inputVals = new double[2];
            double[] outputVals = new double[5];
            int degree = -1;
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(degree, inputVals, outputVals));
            degree = 0;
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(degree, inputVals, outputVals));
        }

        [Fact]
        public void RegressionModelForInsufficientData()
        {
            int degree = 5;
            double[] inputVals = new double[2];
            double[] outputVals = new double[5];

            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(degree, inputVals, outputVals));
            outputVals = new double[2];
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(degree, inputVals, outputVals));
            inputVals = new double[5];
            Assert.Throws<ArgumentException>(() => RegressionUtils.GetRegressionCoeff(degree, inputVals, outputVals));
        }

        [Fact]
        public void RegressionModelDegreeOneIntegerData()
        {
            int degree = 1;
            double[] inputData = new double[6] { 2, 3, 6, 12, 13, 14 };
            double[] outputData = new double[6] { 4, 6, 12, 24, 26, 48 };
            double[] expCoeff = new double[2]
            {
                -3.3490566037735903,
                2.8018867924528306
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);

            outputData = new double[6] { 4, 0, 0, 0, 2, 4 };
            expCoeff = new double[2]
            {
                 1.2735849056603779,
                 4.7169811320754658E-2
            };
            coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void RegressionModelDegreeOneRationalNumberData()
        {
            int degree = 1;
            double[] inputData = new double[6] { 2.1, 3, 6.5, 12, 13.2, 14.001 };
            double[] outputData = new double[6] { 4.5, 6.2, 12.005, 24.2, 26, 48.6 };
            double[] expCoeff = new double[2]
            {
               -3.4512245112205058,
                2.7994005446216228
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void RegressionModelDegreeTwoIntegerData()
        {
            int degree = 2;
            double[] inputData = new double[6] { 2, 3, 6, 12, 13, 14 };
            double[] outputData = new double[6] { 4, 6, 12, 24, 26, 48 };
            double[] expCoeff = new double[3]
            {
                9.6571299672920539,
               -2.3125681416594790,
                3.1843302379788824E-1
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);

            outputData = new double[6] { 4, 0, 0, 0, 2, 4 };
            expCoeff = new double[3]
            {
                 6.7493890747772980,
                -2.1060942140682166,
                1.3406519042069365E-1
            };
            coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void RegressionModelDegreeTwoRationalNumberData()
        {
            int degree = 2;
            double[] inputData = new double[6] { 2.1, 3, 6.5, 12, 13.2, 14.001 };
            double[] outputData = new double[6] { 4.5, 6.2, 12.005, 24.2, 26, 48.6 };
            double[] expCoeff = new double[3]
            {
                1.0180226657066095E1,
               -2.4728184448880732,
                3.2643647890597749E-1
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void RegressionModelDegreeFourIntegerData()
        {
            int degree = 4;
            double[] inputData = new double[6] { 2, 3, 6, 12, 13, 14 };
            double[] outputData = new double[6] { 4, 6, 12, 24, 26, 48 };
            double[] expCoeff = new double[5]
            {
                1.7594784911115582E1,
               -1.3384840192158542E1,
                4.3302259404387371,
               -4.7841121632922212E-1,
                1.7709389293365605E-2
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);

            outputData = new double[6] { 4, 0, 0, 0, 2, 4 };
            expCoeff = new double[5]
            {
                2.3934726691506235E1,
               -1.5696468811810943E1,
                3.3955443613738288,
               -2.9501915259545336E-1,
                8.9560059115598341E-3
            };
            coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void RegressionModelDegreeFourRationalNumberData()
        {
            int degree = 4;
            double[] inputData = new double[6] { 2.1, 3, 6.5, 12, 13.2, 14.001 };
            double[] outputData = new double[6] { 4.5, 6.2, 12.005, 24.2, 26, 48.6 };
            double[] expCoeff = new double[5]
            {
                2.3319554138735938E1,
               -1.6843050040215239E1,
                4.9666060426591372,
               -5.2616801106560984E-1,
                1.8966643810029056E-2
            };
            double[] coeff = RegressionUtils.GetRegressionCoeff(degree, inputData, outputData);
            VerifyCoefficients(expCoeff, coeff, 1E-6);
        }

        [Fact]
        public void CreateModelInvalidItemId()
        {
            Assert.Throws<ArgumentException>(() => new RuneRegressionModel(-1, 2, DateTime.Now));
        }

        [Fact]
        public void CreateModelInvalidDegreee()
        {
            Assert.Throws<ArgumentException>(() => new RuneRegressionModel(2, -1, DateTime.Now));
            Assert.Throws<ArgumentException>(() => new RuneRegressionModel(2, 0, DateTime.Now));
        }

        [Fact]
        public void SetInvalidCoefficients()
        {
            RuneRegressionModel model = new RuneRegressionModel(2, 2, DateTime.Now) { PredictableProperty = PredictableProperty.Curr_Ge_Price };
            Assert.Throws<ArgumentException>(() => model.Coefficients = new double[0]);
            Assert.Throws<ArgumentException>(() => model.Coefficients = new double[1]);
        }

        [Fact]
        public void PredictInvalidDate()
        {
            DateTime baseDate = DateTime.Now;
            DateTime predicitonDate = baseDate;
            predicitonDate = predicitonDate.AddHours(-1);
            RuneRegressionModel model = new RuneRegressionModel(2, 1, baseDate) { PredictableProperty = PredictableProperty.Curr_Ge_Price };
            Assert.Throws<ArgumentException>(() => model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddHours(1);
            predicitonDate = predicitonDate.AddDays(-1);
            Assert.Throws<ArgumentException>(() => model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddDays(1);
            predicitonDate = predicitonDate.AddMonths(-1);
            Assert.Throws<ArgumentException>(() => model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddMonths(1);
            predicitonDate = predicitonDate.AddYears(-1);
            Assert.Throws<ArgumentException>(() => model.PredictValue(predicitonDate));
        }

        [Fact]
        public void PredictValueSameDateDegreeOne()
        {
            DateTime baseDate = DateTime.Now;
            DateTime predicitonDate = baseDate;
            RuneRegressionModel model = new RuneRegressionModel(2, 1, baseDate) { PredictableProperty = PredictableProperty.Curr_Ge_Price };

            model.Coefficients = new double[2] { 0, 1 };
            Assert.Equal(0, model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddHours(1);
            Assert.Equal(1, model.PredictValue(predicitonDate));

            model.Coefficients = new double[2] { 1, 0 };
            Assert.Equal(1, model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddHours(1);
            Assert.Equal(1, model.PredictValue(predicitonDate));
        }

        [Fact]
        public void PredictValueSameDateDegreeX()
        {
            DateTime baseDate = DateTime.Now;
            DateTime predicitonDate = baseDate;
            RuneRegressionModel model = new RuneRegressionModel(2, 3, baseDate) { PredictableProperty = PredictableProperty.Curr_Ge_Price };
            model.Coefficients = new double[4] { 0, 1, 2, 3 };
            Assert.Equal(0, model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddHours(1);
            Assert.Equal(6, model.PredictValue(predicitonDate));

            model.Coefficients = new double[4] { 1, 0, 0, 1 };
            Assert.Equal(2, model.PredictValue(predicitonDate));
            predicitonDate = predicitonDate.AddHours(1);
            Assert.Equal(9, model.PredictValue(predicitonDate));

            predicitonDate = baseDate.AddDays(1);
            // 2.5 + hours^2 + 2.5*hours^3 and hours = 24
            model.Coefficients = new double[4] { 2.5, 0, 1, 2.5 };
            Assert.Equal(35138, model.PredictValue(predicitonDate));
        }

        private static bool VerifyCoefficients(double[] expected, double[] actual, double allowedError)
        {
            if (expected.Length != actual.Length) return false;
            for (int i = 0; i < expected.Length; i++)
            {
                if (allowedError > 0)
                {
                    Assert.True(Math.Abs(expected[i] - actual[i]) <= allowedError);
                }
                else
                {
                    Assert.Equal(expected[i], actual[i]);
                }
            }
            return true;
        }
    }
}
