﻿using RuneComp;
using System;
using System.Collections.Generic;
using Xunit;

namespace RuneTests
{
    public class SqlBuilderNoPredictionTests
    {
        Dictionary<string, string> _propDict;
        Dictionary<string, string> _tableMapping;
        SqlBuilder _sqlBuilder;
        Operation[] _stringOps;
        Operation[] _numericOps;

        public SqlBuilderNoPredictionTests()
        {
            _stringOps = new Operation[] { Operation.Equal, Operation.Starting_With, Operation.Ending_With, Operation.Like };
            _numericOps = new Operation[] { Operation.Equal, Operation.Smaller, Operation.Smaller_Eq, Operation.Greater, Operation.Greater_Eq };
            _propDict = new Dictionary<string, string>
            {
                { "name", "item_name" },
                { "item id", "item_id" },
                { "icon", "icon" },
                { "description", "description" },
                { "price", "current_ge_price" },
                { "price change", "current_price_change" },
                { "high alch val", "high_alch_val" },
                { "low alch val", "low_alch_val" },
                { "const price", "const_price" },
                { "buying limit", "buying_limit" }
            };

            _tableMapping = new Dictionary<string, string>
            {
                { "items", "items" },
                { "price regression models", "price_regression_models" }
            };
            _sqlBuilder = new SqlBuilder("", _propDict, _tableMapping, '*');
        }

        [Fact]
        public void NoQuery()
        {
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(null));
        }

        [Fact]
        public void Query_InvalidOp()
        {
            Query q = new Query(QueryOp.None);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Fact]
        public void SqlBuilder_InvalidSchemaName()
        {
            Assert.Throws<ArgumentException>(() => new SqlBuilder(null, new Dictionary<string, string>(),
                new Dictionary<string, string>(), '*'));
        }

        [Fact]
        public void SqlBuilder_NoPropertyDictionary()
        {
            Assert.Throws<ArgumentException>(() => new SqlBuilder("testSchemaName", null,
                new Dictionary<string, string>(), '*'));
        }

        [Fact]
        public void SqlBuilder_NoTableMappingDictionary()
        {
            Assert.Throws<ArgumentException>(() => new SqlBuilder("testSchemaName",
                new Dictionary<string, string>(), null, '*'));
        }

        [Theory]
        [InlineData("test_string")]
        [InlineData("")]
        [InlineData(null)]
        public void Query_InvalidProperty(string propertyName)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = propertyName
            };
            q.Props.Add(p);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Fact]
        public void Query_No_TableMapping()
        {
            Dictionary<string, string> _tableMappings = new Dictionary<string, string>();
            SqlBuilder builder = new SqlBuilder("", _propDict, _tableMappings, '*');
            Assert.Throws<ArgumentException>(() => builder.BuildExecutionPlanFromQuery(new Query(QueryOp.show)));
            _tableMappings.Add("items_x", "items");
            builder = new SqlBuilder("", _propDict, _tableMappings, '*');
            Assert.Throws<ArgumentException>(() => builder.BuildExecutionPlanFromQuery(new Query(QueryOp.show)));
        }

        [Theory]
        [InlineData("", "test_value")]
        [InlineData(null, "test_value")]
        public void Query_InvalidPropertyName_StringFilter(string propertyName, string filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property()
            {
                PropertyName = "test",
                IsNumeric = false
            };
            Filter f = new Filter()
            {
                Condition = new Condition()
                {
                    SVal = filterValue,
                    CondType = ConditionValType.String,
                    Op = Operation.None
                },
                Property = new Property()
                {
                    PropertyName = propertyName,
                    IsNumeric = false
                }
            };
            q.Filters.Add(f);
            foreach (var op in _stringOps)
            {
                q.Filters[0].Condition.Op = op;
                Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
            }
        }

        [Theory]
        [InlineData("name", Operation.Smaller, "test")]
        [InlineData("name", Operation.Smaller_Eq, "test")]
        [InlineData("name", Operation.Greater, "test")]
        [InlineData("name", Operation.Greater_Eq, "test")]
        [InlineData("name", Operation.None, "test")]
        public void Query_InvalidConditionOp_StringFilter(string propertyName, Operation op, string filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property()
            {
                PropertyName = propertyName,
                IsNumeric = false
            };
            Filter f = new Filter()
            {
                Condition = new Condition()
                {
                    Op = op,
                    SVal = filterValue,
                    CondType = ConditionValType.String
                },
                Property = p
            };
            q.Filters.Add(f);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Theory]
        [InlineData("price", Operation.Starting_With, 2)]
        [InlineData("price", Operation.Ending_With, 2)]
        [InlineData("price", Operation.Like, 2)]
        [InlineData("price", Operation.None, 2)]
        public void Query_InvalidConditionOp_IntegerFilter(string propertyName, Operation op,
            int filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property()
            {
                PropertyName = propertyName,
                IsNumeric = false
            };
            Filter f = new Filter()
            {
                Condition = new Condition()
                {
                    Op = op,
                    IVal = filterValue,
                    CondType = ConditionValType.Integer
                },
                Property = p
            };
            q.Filters.Add(f);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Theory]
        [InlineData("price", Operation.Starting_With, 2.5)]
        [InlineData("price", Operation.Ending_With, 2.5)]
        [InlineData("price", Operation.Like, 2.5)]
        [InlineData("price", Operation.None, 2.5)]
        public void Query_InvalidConditionOp_DoubleFilter(string propertyName, Operation op, double filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property()
            {
                PropertyName = propertyName,
                IsNumeric = false
            };
            Filter f = new Filter()
            {
                Condition = new Condition()
                {
                    Op = op,
                    DVal = filterValue,
                    CondType = ConditionValType.Double
                },
                Property = p
            };
            q.Filters.Add(f);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Fact]
        public void Query_EmptyPropertyList()
        {
            Query q = new Query(QueryOp.show);
            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);
            RuneSqlQuery query = exPlan.Queries[0];
            string sqlStmt = query.GetPlainQuery();
            string expectedStmt = "select * from items";
            Assert.Equal(expectedStmt, sqlStmt);
            Assert.Empty(query.PropertiesToFetch);
            Assert.Empty(exPlan.ResultProps);
        }

        [Fact]
        public void Query_Schema_EmptyPropertyList()
        {
            Query q = new Query(QueryOp.show);
            _sqlBuilder.SetDBSchema("testSchema");
            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);
            RuneSqlQuery query = exPlan.Queries[0];
            string sqlStmt = query.GetPlainQuery();
            string expectedStmt = "select * from testSchema.items";
            Assert.Equal(expectedStmt, sqlStmt);
            Assert.Empty(query.PropertiesToFetch);
            Assert.Empty(exPlan.ResultProps);
        }

        [Theory]
        [InlineData("price", true, ConditionValType.Integer, Operation.Equal, 200)]
        [InlineData("price", true, ConditionValType.Integer, Operation.Smaller, 200)]
        [InlineData("price", true, ConditionValType.Integer, Operation.Smaller_Eq, 200)]
        [InlineData("price", true, ConditionValType.Integer, Operation.Greater, 200)]
        [InlineData("price", true, ConditionValType.Integer, Operation.Greater_Eq, 200)]
        [InlineData("price", true, ConditionValType.Double, Operation.Equal, 200.75)]
        [InlineData("price", true, ConditionValType.Double, Operation.Smaller, 200.75)]
        [InlineData("price", true, ConditionValType.Double, Operation.Smaller_Eq, 200.75)]
        [InlineData("price", true, ConditionValType.Double, Operation.Greater, 200.75)]
        [InlineData("price", true, ConditionValType.Double, Operation.Greater_Eq, 200.75)]
        public void Query_OneNumericFilter(string propertyName, bool isNumeric, ConditionValType condValType,
            Operation filterOp, object filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = propertyName,
                IsNumeric = isNumeric
            };
            q.Props.Add(p);
            Condition c = new Condition
            {
                CondType = condValType,
                Op = filterOp,
            };
            switch (condValType)
            {
                case ConditionValType.Integer:
                    c.IVal = (int)filterValue;
                    break;
                case ConditionValType.Double:
                    c.DVal = (double)filterValue;
                    break;
                case ConditionValType.String:
                    c.SVal = (string)filterValue;
                    break;
            }

            Filter f = new Filter
            {
                Property = new Property()
                {
                    PropertyName = propertyName,
                    IsNumeric = isNumeric
                },
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.True(CheckQueryIsNoPrediction(actualQuery));
            Assert.Equal(2, actualQuery.PropertiesToFetch.Count);
            Assert.Equal(2, exPlan.ResultProps.Count);

            string[] expectedQueryParams = new string[]
            {
                _propDict[propertyName]
            };
            string[] expectedQueryProps = new string[]
            {
                _propDict["icon"], _propDict[propertyName]
            };
            string expPropNamesAsString = string.Join(',', expectedQueryProps);
            Assert.True(CheckPropertiesOfQueries(expectedQueryProps, actualQuery, exPlan));
            Assert.True(CheckQueryParams(expectedQueryParams, new object[1] { filterValue }, actualQuery));

            string queryFormat = "select {0} from items where {1} {2} @{1}_0";
            string expectedQueryStmt = string.Format(queryFormat, expPropNamesAsString, expectedQueryParams[0], Enums.GetOperationAsStringSymbol(filterOp));
            string generatedStmt = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQueryStmt, generatedStmt);
        }

        [Theory]
        [InlineData("name", false, ConditionValType.String, Operation.Equal, "Pirate bandana")]
        [InlineData("name", false, ConditionValType.String, Operation.Like, "*band")]
        [InlineData("name", false, ConditionValType.String, Operation.Like, "band*")]
        [InlineData("name", false, ConditionValType.String, Operation.Like, "Pir*ana")]
        [InlineData("name", false, ConditionValType.String, Operation.Like, "*band*")]
        [InlineData("name", false, ConditionValType.String, Operation.Starting_With, "Pirate*")]
        [InlineData("name", false, ConditionValType.String, Operation.Ending_With, "*bandana")]
        public void Query_OneStringFilter(string propertyName, bool isNumeric, ConditionValType condValType,
            Operation filterOp, object filterValue)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = propertyName,
                IsNumeric = isNumeric
            };
            q.Props.Add(p);
            Condition c = new Condition
            {
                CondType = condValType,
                Op = filterOp,
            };
            switch (condValType)
            {
                case ConditionValType.String:
                    c.SVal = (string)filterValue;
                    break;
            }

            Filter f = new Filter
            {
                Property = new Property()
                {
                    PropertyName = propertyName,
                    IsNumeric = isNumeric
                },
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.True(CheckQueryIsNoPrediction(actualQuery));
            Assert.Equal(2, actualQuery.PropertiesToFetch.Count);
            Assert.Equal(2, exPlan.ResultProps.Count);

            string[] expectedQueryParams = new string[]
            {
                _propDict[propertyName]
            };
            string[] expectedQueryProps = new string[]
            {
                _propDict["icon"], _propDict[propertyName]
            };
            string expPropNamesAsString = string.Join(',', expectedQueryProps);
            Assert.True(CheckPropertiesOfQueries(expectedQueryProps, actualQuery, exPlan));
            Assert.True(CheckQueryParams(expectedQueryParams, new object[1] { filterValue }, actualQuery));

            string queryFormat = "select {0} from items where {1} {2} ";
            queryFormat += "@{1}_0";
            string expectedQueryStmt = string.Format(queryFormat, expPropNamesAsString, expectedQueryParams[0]
                , Enums.GetOperationAsStringSymbol(filterOp));
            string generatedStmt = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQueryStmt, generatedStmt);
        }

        [Theory]
        [InlineData(new string[] { "name", "description" }, new bool[] { false, false },
            new string[] { "icon", "item_name", "description" }, "")]
        [InlineData(new string[] { "price", "price change" }, new bool[] { true, true },
            new string[] { "icon", "current_ge_price", "current_price_change" }, "")]
        [InlineData(new string[] { "name", "low alch val", "high alch val", "description" },
            new bool[] { false, true, true, false },
            new string[] { "icon", "item_name", "low_alch_val", "high_alch_val", "description" }, "")]
        [InlineData(new string[] { "name", "description" },
            new bool[] { false, false },
            new string[] { "icon", "item_name", "description" }, "testSchema")]
        [InlineData(new string[] { "price", "price change" },
            new bool[] { true, true },
            new string[] { "icon", "current_ge_price", "current_price_change" }, "testSchema")]
        [InlineData(new string[] { "name", "low alch val", "high alch val", "description" },
            new bool[] { false, true, true, false },
            new string[] { "icon", "item_name", "low_alch_val", "high_alch_val", "description" }, "testSchema")]
        public void Query_MultipleProperties_NoFilters(string[] inputPropNames, bool[] isNumeric,
    string[] expectedPropNames, string dbSchemaName)
        {
            string queryFormat = "select {0} from " + (string.IsNullOrWhiteSpace(dbSchemaName) ? "items" : "{1}.items");
            string queryProps = "";
            string expectedQueryProps = "icon,";
            Query query = new Query(QueryOp.show);

            for (int i = 0; i < inputPropNames.Length; i++)
            {
                Assert.True(_propDict.ContainsKey(inputPropNames[i]));
                expectedQueryProps += _propDict[inputPropNames[i]];
                queryProps += inputPropNames[i];
                if (i + 1 < inputPropNames.Length)
                {
                    queryProps += ",";
                    expectedQueryProps += ",";
                }
                Property p = new Property()
                {
                    PropertyName = inputPropNames[i],
                    IsNumeric = isNumeric[i]
                };
                query.Props.Add(p);
            }
            _sqlBuilder.SetDBSchema(string.IsNullOrWhiteSpace(dbSchemaName) ? "" : dbSchemaName);
            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(query);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.True(CheckQueryIsNoPrediction(actualQuery));
            Assert.True(CheckPropertiesOfQueries(expectedPropNames, actualQuery, exPlan));

            string expectedStmt = "";
            if (string.IsNullOrWhiteSpace(dbSchemaName))
            {
                expectedStmt = string.Format(queryFormat, expectedQueryProps);
            }
            else
            {
                expectedStmt = string.Format(queryFormat, expectedQueryProps, dbSchemaName);
            }
            string sqlStmt = actualQuery.GetPlainQuery();
            Assert.Equal(expectedStmt, sqlStmt);
            // Reset sqlBuilder schema name
            _sqlBuilder.SetDBSchema("");
        }

        [Fact]
        public void Query_DifferentPropsInSelectAndFilter()
        {
            int numPropsToFetch = 2;
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "price",
                IsNumeric = true
            };
            q.Props.Add(p);
            Condition c = new Condition
            {
                CondType = ConditionValType.Integer,
                Op = Operation.Equal
            };

            Filter f = new Filter
            {
                Property = new Property()
                {
                    PropertyName = "buying limit",
                    IsNumeric = true
                },
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.False(actualQuery.IsPrediction);
            Assert.Null(actualQuery.SubstitutedProperty);
            Assert.Equal(numPropsToFetch, actualQuery.PropertiesToFetch.Count);
            Assert.Equal(numPropsToFetch, exPlan.ResultProps.Count);

            string[] expectedProps = new string[]
            {
                _propDict["icon"],
                _propDict["price"]
            };
            Assert.True(CheckPropertiesOfQueries(expectedProps, actualQuery, exPlan));

            Assert.True(actualQuery.ExpectedQueryParams == 1);
            Assert.True(actualQuery.QueryParams.Count == 1);
            string expectedQueryStmt = "select icon,current_ge_price from items where buying_limit = " +
                "@" + _propDict["buying limit"] + "_0";
            string generatedStmt = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQueryStmt, generatedStmt);
        }

        [InlineData(new string[2] { "name", "price" }, new bool[2] { false, true },
            new ConditionValType[2] { ConditionValType.String, ConditionValType.Integer },
            new Operation[2] { Operation.Like, Operation.Smaller_Eq }, new object[2] { "sword", 200 },
            new FilterConcatinationOp[2] { FilterConcatinationOp.And, FilterConcatinationOp.And },
            new string[2] { "*sword*", "200" })]
        [Theory]
        public void Query_MultipleFilters_AllProps_MixedFilterConcatOps(string[] filterPropNames, bool[] isNumeric,
            ConditionValType[] condValTypes, Operation[] filterOps, object[] filterValues,
            FilterConcatinationOp[] filterConcatOps, string[] expFilterValues)
        {
            Query q = new Query(QueryOp.show);
            int numFilters = filterPropNames.Length;
            Assert.Equal(numFilters, isNumeric.Length);
            Assert.Equal(numFilters, condValTypes.Length);
            Assert.Equal(numFilters, filterOps.Length);
            Assert.Equal(numFilters, filterValues.Length);
            Assert.Equal(numFilters, filterConcatOps.Length);

            string expFilterFormat = "{0} {1} {2}";
            string expFilterSqlQueryPart = "";
            string currFilterPropValuePlaceHolder = "";
            string[] expFilterPropNames = new string[numFilters];

            Dictionary<string, int> filterPropDict = new Dictionary<string, int>();

            for (int i = 0; i < numFilters; i++)
            {
                Property p = new Property
                {
                    PropertyName = filterPropNames[i],
                    IsNumeric = isNumeric[i]
                };
                if (filterPropDict.ContainsKey(p.PropertyName))
                {
                    filterPropDict[p.PropertyName]++;
                }
                else
                {
                    filterPropDict.Add(p.PropertyName, 0);
                }
                Condition c = new Condition
                {
                    CondType = condValTypes[i],
                    Op = filterOps[i]
                };
                switch (condValTypes[i])
                {
                    case ConditionValType.Integer:
                        c.IVal = (int)filterValues[i];
                        break;
                    case ConditionValType.Double:
                        c.DVal = (double)filterValues[i];
                        break;
                    case ConditionValType.String:
                        c.SVal = (string)filterValues[i];
                        break;
                }
                Filter f = new Filter()
                {
                    Property = p,
                    Condition = c,
                    FilterConcatOp = filterConcatOps[i]
                };
                q.Filters.Add(f);

                expFilterPropNames[i] = _propDict[p.PropertyName];
                currFilterPropValuePlaceHolder = "@" + expFilterPropNames[i] + "_" + filterPropDict[p.PropertyName];
                expFilterSqlQueryPart += string.Format(expFilterFormat, expFilterPropNames[i],
                    Enums.GetOperationAsStringSymbol(c.Op), currFilterPropValuePlaceHolder);

                if (i + 1 < numFilters)
                {
                    expFilterSqlQueryPart += " " + Enums.GetFilterConcatOpAsAsStringSymbol(f.FilterConcatOp) + " ";
                }
            }
            expFilterSqlQueryPart = expFilterSqlQueryPart.TrimEnd(' ');

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.False(actualQuery.IsPrediction);
            Assert.Null(actualQuery.SubstitutedProperty);

            string expectedQueryStmt = "select * from items where " + expFilterSqlQueryPart;
            string generatedStmt = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQueryStmt, generatedStmt);
            CheckQueryParams(expFilterPropNames, expFilterValues, actualQuery);
        }

        [Fact]
        public void Query_MissingStartWilcardFilter()
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Condition c = new Condition
            {
                CondType = ConditionValType.String,
                Op = Operation.Starting_With,
                SVal = "Rune"
            };

            Filter f = new Filter
            {
                Property = p,
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.False(actualQuery.IsPrediction);
            Assert.Null(actualQuery.SubstitutedProperty);

            string expectedQuery = "select * from items where item_name like " +
                "@" + _propDict["name"] + "_0";
            string actualSqlQuery = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQuery, actualSqlQuery);

            string[] props = new string[] { _propDict["name"] };
            string[] expQueryParamValues = new string[] { "Rune*" };
            CheckPropertiesOfQueries(new string[0], actualQuery, exPlan);
            CheckQueryParams(props, expQueryParamValues, actualQuery);
        }

        [Fact]
        public void Query_MissingEndingWithWildcardFilter()
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Condition c = new Condition
            {
                CondType = ConditionValType.String,
                Op = Operation.Ending_With,
                SVal = "sword"
            };

            Filter f = new Filter
            {
                Property = p,
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.False(actualQuery.IsPrediction);
            Assert.Null(actualQuery.SubstitutedProperty);

            string expectedQuery = "select * from items where item_name like " +
                "@" + _propDict["name"] + "_0";
            string actualSqlQuery = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQuery, actualSqlQuery);

            string[] props = new string[] { _propDict["name"] };
            string[] expQueryParamValues = new string[] { "*sword" };
            CheckPropertiesOfQueries(new string[0], actualQuery, exPlan);
            CheckQueryParams(props, expQueryParamValues, actualQuery);
        }

        [Fact]
        public void Query_MissingLikeWildcards()
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Condition c = new Condition
            {
                CondType = ConditionValType.String,
                Op = Operation.Like,
                SVal = "sword"
            };

            Filter f = new Filter
            {
                Property = p,
                Condition = c
            };
            q.Filters.Add(f);

            RuneSqlExecutionPlan exPlan = _sqlBuilder.BuildExecutionPlanFromQuery(q);
            Assert.NotNull(exPlan);
            Assert.Single(exPlan.Queries);

            RuneSqlQuery actualQuery = exPlan.Queries[0];
            Assert.False(actualQuery.IsPrediction);
            Assert.Null(actualQuery.SubstitutedProperty);

            string expectedQuery = "select * from items where item_name like " +
                "@" + _propDict["name"] + "_0";
            string actualSqlQuery = actualQuery.GetPlainQuery();
            Assert.Equal(expectedQuery, actualSqlQuery);

            string[] props = new string[] { _propDict["name"] };
            string[] expQueryParamValues = new string[] { "*sword*" };
            CheckPropertiesOfQueries(new string[0], actualQuery, exPlan);
            CheckQueryParams(props, expQueryParamValues, actualQuery);
        }

        [Theory]
        [InlineData("swo*ds")]
        [InlineData("sw*s*")]
        [InlineData("*swor**")]
        public void Query_MisplacedStartWildcards(string value)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Condition c = new Condition
            {
                CondType = ConditionValType.String,
                Op = Operation.Starting_With,
                SVal = value
            };

            Filter f = new Filter
            {
                Property = p,
                Condition = c
            };
            q.Filters.Add(f);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        [Theory]
        [InlineData("swo*ds")]
        [InlineData("sw*s*")]
        [InlineData("*swor**")]
        public void Query_MisplacedEndWildcards(string value)
        {
            Query q = new Query(QueryOp.show);
            Property p = new Property
            {
                PropertyName = "name",
                IsNumeric = false
            };
            Condition c = new Condition
            {
                CondType = ConditionValType.String,
                Op = Operation.Ending_With,
                SVal = value
            };

            Filter f = new Filter
            {
                Property = p,
                Condition = c
            };
            q.Filters.Add(f);
            Assert.Throws<ArgumentException>(() => _sqlBuilder.BuildExecutionPlanFromQuery(q));
        }

        private bool CheckQueryIsNoPrediction(RuneSqlQuery query)
        {
            if (query == null)
            {
                throw new ArgumentException("Cannot evaluate query that is null");
            }
            if (query.IsPrediction || query.SubstitutedProperty != null
                || query.SubstitutedPropertyPredictions.Count > 0)
            {
                return false;
            }
            return true;
        }

        private bool CheckPropertiesOfQueries(string[] props, RuneSqlQuery query, RuneSqlExecutionPlan plan)
        {
            if (query == null || plan == null || plan.ResultProps == null
                || (props.Length != query.PropertiesToFetch.Count)
                || (props.Length != plan.ResultProps.Count))
            {
                return false;
            }
            for (int i = 0; i < props.Length; i++)
            {
                Assert.True(query.PropertiesToFetch.Contains(props[i]));
                Assert.True(plan.ResultProps.Contains(props[i]));
            }
            return true;
        }

        private bool CheckQueryParams(string[] expectedParams, object[] expectedValues, RuneSqlQuery query)
        {
            if (expectedParams.Length != expectedValues.Length) return false;
            if (query == null || query.QueryParams == null) return false;
            if (query.QueryParams.Count != expectedParams.Length ||
                query.ExpectedQueryParams != expectedParams.Length) return false;
            for (int i = 0; i < expectedParams.Length; i++)
            {
                try
                {
                    object value = query.QueryParams[expectedParams[i] + "_" + i.ToString()].Item2;
                    Assert.True(Equals(expectedValues[i], value));
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
