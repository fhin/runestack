﻿using Newtonsoft.Json;
using System;

namespace RuneDB.Models
{
    public class RuneCategoryEntry
    {
        private int _numItems;

        [JsonProperty("letter")]
        public char StartingLetter { get; set; }

        [JsonProperty("items")]
        public int NumItems
        {
            get { return _numItems; }
            set
            {
                if (value < 0) throw new ArgumentException("Category entry must have at least 0 entries");
                _numItems = value;
            }
        }

        public RuneCategoryEntry()
        {

        }
    }
}
