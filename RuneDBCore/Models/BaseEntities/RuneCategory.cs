﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RuneDB.Models
{
    [Table("categories", Schema ="runeschema")]
    public class RuneCategory
    {
        [Key, Column("category_id"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, MaxLength(35), Column("category_name")]
        public string Name { get; set; }

        [NotMapped]
        public Dictionary<char, RuneCategoryEntry> LetterEntries { get; set; }

        public RuneCategory()
        {
            LetterEntries = new Dictionary<char, RuneCategoryEntry>();
        }

        public RuneCategory(int id, string name) : this()
        {
            Id = id;
            Name = name;
        }
    }
}
