﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RuneDB.Models
{
    //TODO: Define table name and correct property names and annotations
    //[Table("")]
    public class RuneHighAlchHistoryEntry
    {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            [Key]
            public int HighAlchHistoryId { get; set; }
            public int ItemId { get; set; }
            public int Price { get; set; }
            public System.DateTime Date { get; set; }

            public RuneHighAlchHistoryEntry()
            {

            }
        }
}
