﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RuneDB.Models
{ 
    [Table("pricehistory", Schema ="runeschema")]
    public class RunePriceHistoryEntry
    {
        private int _price;

        [Key, Column("price_history_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PriceHistoryId { get; set; }

        [Column("item_id")]
        public int ItemId { get; set; }

        [Column("price")]
        public int Price
        {
            get { return _price; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Price cannot be negative, check your inputs !");
                }
                _price = value;
            }
        }

        [Required, Column("date")]
        public DateTime Date { get; set; }

        public RunePriceHistoryEntry()
        {
            Date = DateTime.Now;
        }
    }
}
