﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RuneDB.Models
{
    [Table("items", Schema = "runeschema")]
    public class RuneItem
    {
        public const int MAX_NAME_LENGTH = 80;
        public const int MAX_DESCRIPTION_LENGTH = 200;

        [NotMapped]
        private string _name;

        [NotMapped]
        private int _rune_category_id;

        [NotMapped]
        private string _description;

        [NotMapped]
        private int _current_ge_price;

        [NotMapped]
        private int _const_price;

        [NotMapped]
        private int _buying_limit;

        [JsonProperty("id")]
        [Key, Column("item_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonProperty("name")]
        [Required, MaxLength(MAX_NAME_LENGTH), Column("item_name")]
        public string Name { get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    // TODO:
                    throw new ArgumentException();
                }
                if (value.Length > MAX_NAME_LENGTH)
                {
                    // TODO:
                    throw new ArgumentException();
                }
                _name = value;
            }
        }

        [Column("category_id")]
        public int RuneCategoryId { get { return _rune_category_id; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _rune_category_id = value;
            }
        }

        [JsonProperty("description")]
        [Required, MaxLength(MAX_DESCRIPTION_LENGTH), Column("description")]
        public string Description { get { return _description; }
            set
            {
                if (value.Length > MAX_DESCRIPTION_LENGTH)
                {
                    throw new ArgumentException();
                }
                _description = value;
            }
        }

        [Column("curr_ge_price")]
        public int CurrentGePrice { get { return _current_ge_price; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _current_ge_price = value;
            }
        }

        [JsonIgnore]
        [Required]
        [Column("icon")]
        public byte[] Icon { get; set; }

        [Column("const_price")]
        public int ConstPrice
        {
            get { return _const_price; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Constant price of item must be greater than zero !");
                }
                _const_price = value;
                try
                {
                    HighAlchVal = Convert.ToInt32(0.6 * _const_price);
                    LowAlchVal = Convert.ToInt32(0.4 * _const_price);
                }
                catch (Exception)
                {
                    throw new ArgumentException("Could not convert constant price to corresponding alchemy_values");
                }
            }
        }

        [Column("buying_limit")]
        public int BuyingLimit { get { return _buying_limit; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _buying_limit = value;
            }
        }

        [Column("low_alch_val")]
        public int LowAlchVal { get; set; }

        [Column("high_alch_val")]
        public int HighAlchVal { get; set; }

        [JsonProperty("members")]
        [Column("members")]
        public bool IsMember { get; set; }

        [Column("curr_price_change")]
        public int CurrentPriceChange { get; set; }

        public RuneItem()
        {

        }

        public override int GetHashCode()
        {
            var hashCode = 352033288;
            hashCode = hashCode * -1521134295 * Name.GetHashCode();
            hashCode = hashCode * -1521134295 * Description.GetHashCode();
            hashCode = hashCode * -1521134295 * Id.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object other)
        {
            if ((other == null) || GetType() != other.GetType()) return false;
            RuneItem otherItem = (RuneItem)other;
            return Id == otherItem.Id && Name.Equals(otherItem.Name) 
                && Description.Equals(otherItem.Description);
        }
    }
}
