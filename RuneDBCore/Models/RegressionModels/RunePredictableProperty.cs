﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public enum PredictableProperty
{
    Curr_Ge_Price,
    None
}

namespace RuneDB.Models
{
    [Table("predictable_properties", Schema = "runeschema")]
    public class RunePredictableProperty
    {
        [Key, Column("predictable_property_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PredictablePropertyId { get; set; }

        [Column("property_name"), Required]
        public string PredictablePropertyName { get; private set; }

        [NotMapped]
        private PredictableProperty _predictableProperty;

        [NotMapped]
        public PredictableProperty PredictableProperty
        {
            get { return _predictableProperty; }
            private set
            {
                _predictableProperty = value;
                PredictablePropertyName = EnumValToString(_predictableProperty);
            }
        }

        public RunePredictableProperty(PredictableProperty predictableProperty)
        {
            PredictableProperty = predictableProperty;
        }

        public RunePredictableProperty(string predictablePropertyName)
        {
            if (string.IsNullOrWhiteSpace(predictablePropertyName))
            {
                throw new ArgumentException("Cannot create predictable property with no name");
            }
            PredictableProperty predictableProperty = IsValidPredictablePropName(predictablePropertyName);
            if (predictableProperty == PredictableProperty.None)
            {
                throw new ArgumentException("Cannot create predictable property for invalid name " + predictablePropertyName);
            }
            PredictableProperty = predictableProperty;
        }

        public static string EnumValToString(PredictableProperty predictableProperty)
        {
            return predictableProperty.ToString().ToLower();
        }

        public static PredictableProperty IsValidPredictablePropName(string predictablePropName)
        {
            if (string.IsNullOrWhiteSpace(predictablePropName)) return PredictableProperty.None;
            try
            {
                return (PredictableProperty)System.Enum.Parse(typeof(PredictableProperty), predictablePropName,true);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error converting name to PredictableProperty for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                return PredictableProperty.None;
            }
        }
    }
}
