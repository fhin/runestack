﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RuneDB.Models.RegressionModels
{
    [Table("regression_models", Schema = "runeschema")]
    public class RuneRegressionModel : IRuneRegressionModel
    {
        private int _degree;
        // DBContext defines the primary key for this entity as a composite key of the properties (item_id, pred.prop.id)

        [Column("predictable_property_id")]
        public int PredictablePropertyId { get; private set; }

        [NotMapped]
        public PredictableProperty PredictableProperty { get; set; }

        [Column("item_id")]
        public int ItemId { get; set; }

        [Column("degree")]
        public int Degree { get { return _degree; } 
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Regression model must have a degree of at least 1");
                }
                _degree = value;
            }
        }

        [NotMapped]
        private string _coefficientString;

        [Column("coefficient_string"), Required]
        public string CoefficientsString
        {
            get { return _coefficientString; }
            private set
            {
                _coefficientString = value;
            }
        }

        [NotMapped]
        private double[] _coefficients;

        [NotMapped]
        public double[] Coefficients
        {
            get { return _coefficients; }
            set
            {
                if (value.Length < Degree + 1)
                {
                    throw new ArgumentException("Coefficients cannot be empty and must have length degree + 1");
                }
                _coefficients = value;
                CoefficientsString = ConvertCoefficientsToString();
            }
        }

        [Required, Column("creation_date")]
        public DateTime CreationDate { get; set; }

        public RuneRegressionModel(int itemId, int degree, DateTime creationDate)
        {
            if (itemId < 0 || degree <= 0) throw new ArgumentException("Error: itemID < 0 or degree <= 0, check your inputs !");
            ItemId = itemId;
            Degree = degree;
            _coefficients = new double[degree + 1];
            CoefficientsString = "";
            CreationDate = creationDate;
        }

        public RuneRegressionModel(string[] propNames, object[] values)
        {
            for (int i = 0; i < propNames.Length; i++)
            {
                switch (propNames[i])
                {
                    case "item_id":
                        ItemId = (int)values[i];
                        break;
                    case "creation_date":
                        CreationDate = (DateTime)values[i];
                        break;
                    case "coefficient_string":
                        CoefficientsString = (string)values[i];
                        break;
                    case "degree":
                        Degree = (int)values[i];
                        _coefficients = new double[Degree + 1];
                        break;
                }
            }
        }

        public void SetPredictablePropertyId(int predPropId)
        {
            if (predPropId <= 0) throw new ArgumentException("Cannot set predictable property id to value <= 0");
            PredictablePropertyId = predPropId;
        }

        public int PredictValue(DateTime predictionDate)
        {
            int diffInHours = Convert.ToInt32(Math.Ceiling(predictionDate.Subtract(CreationDate).TotalHours));
            if (diffInHours < 0)
            {
                throw new ArgumentException("Currently prediction data must be at least the same as the base date");
            }
            return Convert.ToInt32(PredictValue(diffInHours));
        }

        public void ExtractCoefficientsFromString()
        {
            if (string.IsNullOrWhiteSpace(CoefficientsString))
            {
                throw new ArgumentException("Cannot parse empty coefficients string");
            }
            string[] coefficientsParts = CoefficientsString.Split('|');
            if (coefficientsParts.Length != _coefficients.Length)
            {
                throw new ArgumentException("Expected " + _coefficients.Length + " but coefficient string had " + coefficientsParts.Length + " elements");
            }
            Coefficients = Array.ConvertAll(coefficientsParts, o => double.Parse(o));
        }

        protected double PredictValue(int value)
        {
            if (value < 0 || Coefficients.Length == 0 || Coefficients.Length != Degree + 1) throw new ArgumentException("Cannot predict negative value or invalid model, check your inputs !");
            try
            {
                double result = Coefficients[0];
                for (int i = 1; i < Coefficients.Length; i++)
                {
                    result += Math.Pow(value, i) * Coefficients[i];
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot predict value for given model for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                return -1;
            }
        }

        protected string ConvertCoefficientsToString()
        {
            string coefficientString = "";
            for (int i = 0; i < Coefficients.Length; i++)
            {
                coefficientString += Coefficients[i].ToString() + "|";
            }
            return coefficientString.TrimEnd('|');
        }
    }
}
