﻿using System;

namespace RuneDB.Models
{
    public interface IRuneRegressionModel
    {
        int ItemId { get; set; }
        int Degree { get; set; }
        double[] Coefficients { get; set; }
        DateTime CreationDate { get; set; }

        int PredictValue(DateTime predictionDate);
    }
}
