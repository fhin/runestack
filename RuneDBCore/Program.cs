﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RuneDB.Services;
using System;
using System.Configuration;

namespace RuneDB
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            IServiceProvider provider = serviceCollection.BuildServiceProvider();
            RuneEntityService rES = new RuneEntityService(provider.GetRequiredService<IRuneDataService>(),
                provider.GetRequiredService<IRuneDBService>());
            //List<RuneCategory> cats = rES.GetCategories();


            /*
            RuneCategory cat = new RuneCategory(1, "Default");
            RuneCategoryEntry entry = new RuneCategoryEntry();
            entry.StartingLetter = '#';
            entry.NumItems = 23;

            RuneCategoryEntry entry2 = new RuneCategoryEntry();
            entry2.StartingLetter = 'a';
            entry2.NumItems = 327;

            RuneCategoryEntry entry3 = new RuneCategoryEntry();
            entry3.StartingLetter = 'b';
            entry3.NumItems = 448;


            RuneCategoryEntry entry4 = new RuneCategoryEntry();
            entry4.StartingLetter = 'c';
            entry4.NumItems = 166;

            RuneCategoryEntry entry5 = new RuneCategoryEntry();
            entry5.StartingLetter = 'd';
            entry5.NumItems = 230;

            RuneCategoryEntry entry6 = new RuneCategoryEntry();
            entry6.StartingLetter = 'e';
            entry6.NumItems = 117;

            RuneCategoryEntry entry7 = new RuneCategoryEntry();
            entry7.StartingLetter = 'f';
            entry7.NumItems = 79;

            RuneCategoryEntry entry8 = new RuneCategoryEntry();
            entry8.StartingLetter = 'u';
            entry8.NumItems = 50;

 
            cat.LetterEntries.Add('#', entry);
            cat.LetterEntries.Add('a', entry2);
            cat.LetterEntries.Add('b', entry3);

            cat.LetterEntries.Add('c', entry4);
            cat.LetterEntries.Add('d', entry5);
            cat.LetterEntries.Add('e', entry6);

            cat.LetterEntries.Add('f', entry7);
            cat.LetterEntries.Add('u', entry8);
            /*
            cat.LetterEntries.Add('#', entry);

            List<RuneCategory> cats = new List<RuneCategory>() { cat };
            */

            //List<RuneCategory> cats = new List<RuneCategory>() { cat };

            /*
            RuneCategory cat = new RuneCategory(1, "Default");
            RuneCategoryEntry entry = new RuneCategoryEntry();
            entry.StartingLetter = 'v';
            entry.NumItems = 41;
            List<RuneCategory> cats = new List<RuneCategory>() { cat };
            List<RuneItem> items = rES.GetItemsForCategory(cats[0]);
            */

            /*
            using(RuneDBContext dbContext = provider.GetRequiredService<RuneDBContext>())
            {
                dbContext.RuneCategories.AddRange(cats);
                dbContext.RuneItems.AddRange(items);
                dbContext.SaveChanges();
            }
            */
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            HttpRequestFactory requestFactory = HttpRequestFactory.GetInstance;
            services.AddSingleton(requestFactory);

            services.AddSingleton<IRuneDataService, RuneHttpDataService>();
            services.AddHttpClient<IRuneDataService, RuneHttpDataService>();

            services.AddDbContext<IRuneDB, RuneDBContext>(options 
                => options.UseNpgsql(ConfigurationManager.ConnectionStrings["RuneDBConnection"].ConnectionString), 
                ServiceLifetime.Transient);
            services.AddSingleton<IRuneDBService, RuneDbService>();
        }
    }
}
