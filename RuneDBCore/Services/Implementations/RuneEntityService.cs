﻿using Newtonsoft.Json.Linq;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using RuneDB.Services;
using RuneDB.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RuneDB
{
    public class RuneEntityService
    {
        private IRuneDataService _runeDataService;
        private IRuneDBService _runeDBService;
        private Stopwatch stopWatch;

        public RuneEntityService(IRuneDataService runeDataService, IRuneDBService runeDBService)
        {
            _runeDataService = runeDataService;
            _runeDBService = runeDBService;
            stopWatch = new Stopwatch();
        }

        public List<RuneCategory> GetCategories()
        {
            List<RuneCategory> runeCategoryList = RuneItemCategoryHelpers.GetCategoryList();
            Dictionary<char, RuneCategoryEntry> catEntries = new Dictionary<char, RuneCategoryEntry>();
            try
            {
                for (int catIdx = 0; catIdx < runeCategoryList.Count; catIdx++)
                {
                    string result = _runeDataService.FetchCategoryData(runeCategoryList[catIdx].Id).GetAwaiter().GetResult();
                    try
                    {
                        foreach (var entry in JObject.Parse(result)["alpha"])
                        {
                            RuneCategoryEntry cat = entry.ToObject<RuneCategoryEntry>();
                            catEntries.Add(cat.StartingLetter, cat);
                        }
                        runeCategoryList[catIdx].LetterEntries = catEntries;
                        catEntries = new Dictionary<char, RuneCategoryEntry>();
                    }
                    catch (Exception e)
                    {
                        //TODO
                        Console.WriteLine(e.Message);
                    }
                }
            }
            catch (Exception)
            {

            }
            return runeCategoryList;
        }

        public List<RuneItem> GetItemsForCategory(RuneCategory category)
        {
            List<RuneItem> items = new List<RuneItem>();
            try
            {
                items = GetAsyncItemsForCategory(category).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching items for category ! " + e.Message);
            }
            return items;
        }

        public IEnumerable<RunePriceHistoryEntry> GetPriceForItems()
        {
            IEnumerable<RunePriceHistoryEntry> items = new List<RunePriceHistoryEntry>();
            try
            {
                items = FetchAllItemsGEPrices().GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching prices for items, reason: " + e.Message);
            }
            return items;
        }

        public IEnumerable<RuneRegressionModel> GetPriceRegressionModels(int degree, int numElems)
        {
            List<RuneRegressionModel> models = new List<RuneRegressionModel>();
            try
            {
                var regModelTask = Task.Run(() => FetchRegressionModels(degree, numElems, PredictableProperty.Curr_Ge_Price));
                models.AddRange(regModelTask.GetAwaiter().GetResult());
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching regression models synchronized for reason " + e.Message);
                Console.WriteLine("Stacktrace " + e.StackTrace);
            }
            return models;
        }

        public async Task<IEnumerable<RunePriceHistoryEntry>> FetchAllItemGEPricesForCat(RuneCategory category)
        {
            List<RunePriceHistoryEntry> histEntries = new List<RunePriceHistoryEntry>();
            HashSet<int> addedItemsHashSet = new HashSet<int>();
            DateTime fetchDate = DateTime.Now;

            if (category == null || category.Id < 0 || category.LetterEntries == null) return histEntries;
            try
            {
                Console.WriteLine("Fetching item prices for category " + (string.IsNullOrWhiteSpace(category.Name) ? category.Id.ToString() : category.Name));
                Console.Write("Progress [each | indicates one fetched and parsed page]: ");
                List<Task<string>> categoryPageData = new List<Task<string>>();
                List<Task<List<RuneItem>>> itemsForPageTaskL = new List<Task<List<RuneItem>>>();
                foreach (KeyValuePair<char, RuneCategoryEntry> letterEntry in category.LetterEntries)
                {
                    int numItems = 0;
                    int numPages = 0;
                    int numItemsLastPage = 0;
                    if (letterEntry.Value != null && letterEntry.Value.NumItems > 0)
                    {
                        numItems = letterEntry.Value.NumItems;
                        numPages = Convert.ToInt32(numItems / RuneRequestConsts.MAX_ITEMS_PER_PAGE);
                        numItemsLastPage = numItems % RuneRequestConsts.MAX_ITEMS_PER_PAGE;
                        if (numItemsLastPage > 0) numPages++;
                        for (int i = 1; i < numPages + 1; i++)
                        {
                            categoryPageData.Add(_runeDataService.FetchAsyncPageData(category.Id, letterEntry.Key, i));
                        }
                    }
                }
                while (categoryPageData.Count > 0)
                {
                    Task<string> catPageDataTask = await Task.WhenAny(categoryPageData);
                    categoryPageData.Remove(catPageDataTask);

                    string pageData = await catPageDataTask;
                    itemsForPageTaskL.Add(GetAsynctItemsFromDataString(pageData, category.Id, false, false));

                    while (itemsForPageTaskL.Count > 0)
                    {
                        Task<List<RuneItem>> itemsForPageTask = await Task.WhenAny(itemsForPageTaskL);
                        itemsForPageTaskL.Remove(itemsForPageTask);
                        List<RuneItem> itemsForPage = await itemsForPageTask;
                        if (itemsForPage != null)
                        {
                            foreach (var item in itemsForPage)
                            {
                                if (item == null || item.Id < 0 || string.IsNullOrWhiteSpace(item.Name) || item.CurrentGePrice < 0) continue;
                                if (addedItemsHashSet.Contains(item.Id))
                                {
                                    Console.WriteLine("AddedItems contains already: " + item.Name);
                                    continue;
                                }
                                addedItemsHashSet.Add(item.Id);
                                RunePriceHistoryEntry entry = new RunePriceHistoryEntry()
                                {
                                    ItemId = item.Id,
                                    Date = fetchDate,
                                    Price = item.CurrentGePrice
                                };
                                histEntries.Add(entry);
                            }
                            Console.Write("|");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching prices for items of category " + category.Name + " for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return histEntries;
        }

        public async Task<IEnumerable<RunePriceHistoryEntry>> FetchAllItemsGEPrices()
        {
            ISet<int> itemIds = new HashSet<int>();
            List<RunePriceHistoryEntry> histEntries = new List<RunePriceHistoryEntry>();
            HashSet<int> addedItemIds = new HashSet<int>();

            List<RuneCategory> categories = new List<RuneCategory>();
            int expectedNumItems = 0;
            int actualNumItems = 0;

            try
            {
                itemIds = _runeDBService.FetchItemIdsHashSet();
                categories = GetCategories();
                if (itemIds == null || itemIds.Count == 0 || categories == null) return histEntries;

                List<Task<IEnumerable<RunePriceHistoryEntry>>> priceHistEntriesForCat = new List<Task<IEnumerable<RunePriceHistoryEntry>>>();
                foreach (var cat in categories)
                {
                    foreach (var entry in cat.LetterEntries)
                    {
                        expectedNumItems += entry.Value != null ? entry.Value.NumItems : 0;
                        Console.WriteLine("Letter: " + entry.Key + " NumItems: " + entry.Value.NumItems);
                    }
                    priceHistEntriesForCat.Add(FetchAllItemGEPricesForCat(cat));
                }
                stopWatch.Reset();
                stopWatch.Start();

                try
                {
                    while (priceHistEntriesForCat.Count > 0)
                    {
                        Task<IEnumerable<RunePriceHistoryEntry>> finishedPriceHistForCatTask = await Task.WhenAny(priceHistEntriesForCat);
                        priceHistEntriesForCat.Remove(finishedPriceHistForCatTask);
                        try
                        {
                            IEnumerable<RunePriceHistoryEntry> priceHistEntries = await finishedPriceHistForCatTask;
                            foreach (var priceHistEntry in priceHistEntries)
                            {
                                if (priceHistEntry == null || priceHistEntry.ItemId < 0 || priceHistEntry.Price < 0)
                                {
                                    Console.WriteLine("Invalid price data for item with id: " + priceHistEntry.ItemId);
                                    continue;
                                }
                                else if (addedItemIds.Contains(priceHistEntry.ItemId))
                                {
                                    Console.WriteLine("Got duplicate price for item with id: " + priceHistEntry.ItemId);
                                    continue;
                                }
                                else if (!itemIds.Contains(priceHistEntry.ItemId))
                                {
                                    Console.WriteLine("Fetched price for item with id " + priceHistEntry.ItemId.ToString() +
                                        "  not existing in database");
                                    continue;
                                }
                                addedItemIds.Add(priceHistEntry.ItemId);
                                histEntries.Add(priceHistEntry);
                                actualNumItems++;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error fetching item prices for category for reason: " + e.Message);
                            Console.WriteLine("Stacktrace: " + e.StackTrace);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error fetching item prices for categories for reason: " + e.Message);
                    Console.WriteLine("Stacktrace: " + e.StackTrace);
                }
                Console.WriteLine("\nSummary: ");
                Console.WriteLine("Expected item count: " + expectedNumItems.ToString() + " | Actual unique item count: " + actualNumItems.ToString());
                Console.WriteLine("Elapsed Time [hh:mm:ss]: " + string.Format("{0:hh\\:mm\\:ss}", stopWatch.Elapsed));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            stopWatch.Stop();
            return histEntries;
        }

        public async Task<IEnumerable<RuneRegressionModel>> FetchRegressionModels(int degree, int numElems, PredictableProperty predictableProperty)
        {
            if (numElems <= 0)
            {
                throw new ArgumentException("Number of elements must be greate than 0");
            }
            if (degree <= 0)
            {
                throw new ArgumentException("Degree must be greater than zero");
            }
            if (predictableProperty == PredictableProperty.None)
            {
                throw new ArgumentException("Cannot create regression models for non-predictable property");
            }
            List<RuneRegressionModel> regModelList = new List<RuneRegressionModel>();
            try
            {
                Console.WriteLine("Creating regression model of degree " + degree.ToString() + " with " + numElems.ToString() + " data points");
                Console.Write("Progress [each | represents 5% of the models fetched]: ");
                List<Task<(int, double[], DateTime)>> regCoeffTaskList = new List<Task<(int, double[], DateTime)>>();
                stopWatch.Reset();
                stopWatch.Start();
                IEnumerable<int> itemIds = _runeDBService.FetchItemIds();
                int numItems = 0;
                foreach (var itemId in itemIds)
                {
                    numItems++;
                    regCoeffTaskList.Add(Task.Run(() =>
                    {
                        try
                        {
                            (IEnumerable<(int, int)> itemHistPrices, DateTime dbCreationDate) = _runeDBService.FetchItemPricesAndCurrentTimeDiff(itemId, numElems, false);
                            double[] priceVals = new double[numElems];
                            double[] timeDiff = new double[numElems];
                            int idx = 0;
                            foreach ((var price, var timeDiffInHours) in itemHistPrices)
                            {
                                priceVals[idx] = price;
                                timeDiff[idx] = timeDiffInHours;
                                idx++;
                            }
                            return (itemId, RegressionUtils.GetRegressionCoeff(degree, timeDiff, priceVals), dbCreationDate);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error creating regression model for itemId " + itemId.ToString()
                                + " for reason " + e.Message);
                            Console.WriteLine("Stacktrace: " + e.StackTrace);
                            if (e.InnerException != null)
                            {
                                Console.WriteLine("Inner exception reason: " + e.InnerException.Message);
                                Console.WriteLine("Inner exception stacktrace: " + e.InnerException.StackTrace);
                            }
                            return (itemId, new double[0], DateTime.MinValue);
                        }
                    }));
                }
                int numModelsFor5Percent = Convert.ToInt32(Math.Floor(numItems * 0.05));
                int printCounter = 0;
                while (regCoeffTaskList.Count > 0)
                {
                    Task<(int, double[], DateTime)> finishedRegTask = await Task.WhenAny(regCoeffTaskList);
                    regCoeffTaskList.Remove(finishedRegTask);
                    (int itemId, double[] regressionCoeffs, DateTime modelCreationDate) = await finishedRegTask;
                    RuneRegressionModel model = new RuneRegressionModel(itemId, degree, modelCreationDate)
                    {
                        Coefficients = regressionCoeffs,
                        PredictableProperty = predictableProperty
                    };
                    regModelList.Add(model);
                    printCounter++;
                    if (printCounter == numModelsFor5Percent)
                    {
                        Console.Write("|");
                        printCounter = 0;
                    }
                }
                stopWatch.Stop();
                Console.WriteLine("\nSummary: ");
                Console.WriteLine("Expected model count: " + numItems.ToString()
                    + " | Actual model count: " + regModelList.Count.ToString());
                Console.WriteLine("Elapsed Time [hh:mm:ss]: " + string.Format("{0:hh\\:mm\\:ss}", stopWatch.Elapsed));
                stopWatch.Reset();
                return regModelList;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error creating regression models for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return regModelList;
        }

        public Task<(IList<int>, IList<byte[]>)> FetchAllItemSprites()
        {
            Task<(IList<int>, IList<byte[]>)> itemSpriteListTask = Task.Run(async () =>
            {
                Console.WriteLine("Fetching sprites for items [Each | indicates 5% progress]: ");
                IList<byte[]> itemSpriteList = new List<byte[]>();
                Task<IEnumerable<int>> fetchItemIdsTask = Task.Run(() => _runeDBService.FetchItemIds());
                IList<int> itemIds = (await fetchItemIdsTask).ToList();
                int cnt = 0;
                int fivePercentItemCnt = Convert.ToInt32(Math.Round(itemIds.Count * 0.05));
                foreach (int itemId in itemIds)
                {
                    try
                    {
                        itemSpriteList.Add(await _runeDataService.FetchAsyncItemBigSprite(itemId));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error fetching item sprite for item with id: " + itemId + " for reason: " + e.Message);
                    }
                    cnt++;
                    if (cnt == fivePercentItemCnt)
                    {
                        Console.Write("|");
                        cnt = 0;
                    }
                }
                return (itemIds, itemSpriteList);
            });
            return itemSpriteListTask;
        }

        private async Task<List<RuneItem>> GetAsyncItemsForCategory(RuneCategory category)
        {
            List<RuneItem> items = new List<RuneItem>();
            if (category == null || category.LetterEntries == null) return null;

            stopWatch.Reset();
            stopWatch.Start();

            Console.WriteLine("Wait time in between each page data fetch: "
                 + RuneRequestConsts.JAGEX_API_PAGE_DELAY + "ms");
            Console.WriteLine("Fetching data for " + category.Name);
            int expectedNumItems = 0;

            try
            {
                if (!_runeDBService.ExistsCategory(category))
                {
                    _runeDBService.StoreCategory(category);
                }
                foreach (var letterEntry in category.LetterEntries)
                {
                    expectedNumItems += letterEntry.Value.NumItems;
                }

                Console.Write("Progress [each | represents one parsed page]: ");
                List<Task<string>> pageDataTasks = _runeDataService.FetchAsyncAllCategoryPageData(category).ToList();
                List<Task> itemsForPageTasks = new List<Task>();

                if (pageDataTasks == null) return items;

                while (pageDataTasks.Count > 0)
                {
                    Task<string> pageDataTask = await Task.WhenAny(pageDataTasks);
                    pageDataTasks.Remove(pageDataTask);
                    try
                    {
                        string pageData = await pageDataTask;
                        if (string.IsNullOrWhiteSpace(pageData)) continue;
                        itemsForPageTasks.Add(GetAsynctItemsFromDataString(pageData, category.Id, true, true).ContinueWith((r) =>
                        {
                            try
                            {
                                List<RuneItem> itemsForPage = r.Result;
                                if (itemsForPage == null || itemsForPage.Count == 0) return;

                                foreach (var item in itemsForPage)
                                {
                                    if (item == null || string.IsNullOrWhiteSpace(item.Name)) continue;
                                    items.Add(item);
                                }
                                Console.Write("|");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Error adding items for page for reason " + e.Message);
                                Console.WriteLine("Stacktrace: " + e.StackTrace);
                            }
                        }, TaskContinuationOptions.OnlyOnRanToCompletion));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error fetching items for page for reason: " + e.Message);
                        Console.WriteLine("Stacktrace: " + e.StackTrace);
                    }
                }
                await Task.WhenAll(itemsForPageTasks);
                Console.WriteLine("Elapsed Time [hh:mm:ss]: " + string.Format("{0:hh\\:mm\\:ss}", stopWatch.Elapsed));
                Console.WriteLine("Expected num. items: " + expectedNumItems + " | Actual: " + (items != null ? items.Count.ToString() : "0"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching items for category, reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            finally
            {
                stopWatch.Reset();
            }
            return items;
        }

        private async Task<List<RuneItem>> GetAsynctItemsFromDataString(string dataString, int categoryId, bool fetchIcon, bool fetchAdvData)
        {
            List<RuneItem> items = new List<RuneItem>();
            if (string.IsNullOrWhiteSpace(dataString)) return items;
            try
            {
                foreach (var itemEntry in JObject.Parse(dataString)["items"])
                {
                    RuneItem item = itemEntry.ToObject<RuneItem>();
                    Task<byte[]> itemIconTask = fetchIcon ? _runeDataService.FetchAsyncItemBigSprite(item.Id) : null;
                    Task<RuneItem> advDetailsTask = fetchAdvData ? CreateAsyncAdvItemDetails(item) : null;

                    int ge_price = ParsePriceString((string)itemEntry.SelectToken("current.price"));

                    if (ge_price <= 0) continue;
                    item.RuneCategoryId = categoryId;
                    item.CurrentGePrice = ge_price;
                    if (itemIconTask != null)
                    {
                        item.Icon = await itemIconTask;
                    }
                    if (advDetailsTask != null)
                    {
                        RuneItem itemWithAdvData = await advDetailsTask;
                        item = AddAdvDetails(item, itemWithAdvData);
                    }
                    items.Add(item);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return items;
        }

        private async Task<RuneItem> CreateAsyncAdvItemDetails(RuneItem item)
        {
            if (item == null || item.Id < 0) return item;
            try
            {
                string advDetailsResponse = await _runeDataService.FetchAsyncItemAdvData(item.Id);
                JObject responseJSONObj = JObject.Parse(advDetailsResponse);
                int propertyValue = 0;
                if (int.TryParse((string)responseJSONObj.SelectToken("buy_limit"), out propertyValue))
                {
                    item.BuyingLimit = propertyValue;
                }
                if (int.TryParse((string)responseJSONObj.SelectToken("lowalch"), out propertyValue))
                {
                    item.LowAlchVal = propertyValue;
                }
                if (int.TryParse((string)responseJSONObj.SelectToken("highalch"), out propertyValue))
                {
                    item.HighAlchVal = propertyValue;
                }
                if (int.TryParse((string)responseJSONObj.SelectToken("cost"), out propertyValue))
                {
                    item.ConstPrice = propertyValue;
                }
                return item;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return item;
            }
        }

        private RuneItem AddAdvDetails(RuneItem itemToUpdate, RuneItem itemWithAdvDetails)
        {
            if (itemToUpdate == null || itemWithAdvDetails == null ||
                itemToUpdate.Id != itemWithAdvDetails.Id) return itemToUpdate;
            itemToUpdate.ConstPrice = itemWithAdvDetails.ConstPrice;
            itemToUpdate.LowAlchVal = itemWithAdvDetails.LowAlchVal;
            itemToUpdate.HighAlchVal = itemWithAdvDetails.HighAlchVal;
            itemToUpdate.BuyingLimit = itemWithAdvDetails.BuyingLimit;
            return itemToUpdate;
        }

        private int ParsePriceString(string priceAsString)
        {
            int ge_price = -1;
            try
            {
                double factor = 1;
                CultureInfo numberCulturInfo = CultureInfo.CreateSpecificCulture("en-US");

                double ge_price_dec = 0;
                if (priceAsString.Contains("m"))
                {
                    priceAsString = priceAsString.Replace("m", string.Empty);
                    factor = Math.Pow(10, 6);
                }
                else if (priceAsString.Contains("k"))
                {
                    priceAsString = priceAsString.Replace("k", string.Empty);
                    factor = Math.Pow(10, 3);
                }
                double.TryParse(priceAsString,
                    (NumberStyles.AllowDecimalPoint
                    | NumberStyles.AllowThousands
                    | NumberStyles.Integer), numberCulturInfo, out ge_price_dec);
                ge_price_dec *= factor;
                ge_price = Convert.ToInt32(ge_price_dec);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ge_price;
        }
    }
}
