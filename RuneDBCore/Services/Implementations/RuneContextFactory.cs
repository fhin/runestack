﻿using Microsoft.EntityFrameworkCore;
using System;

namespace RuneDB.Services
{
    public class RuneContextFactory : IRuneContextFactory
    {
        DbContextOptionsBuilder<RuneDBContext> _options;

        public RuneContextFactory(DbContextOptionsBuilder<RuneDBContext> options)
        {
            _options = options;
        }

        public IRuneDB GetContext()
        {
            RuneDBContext context = null;
            try
            {
                context = new RuneDBContext(_options.Options);
                if (context.CanConnectToDB())
                {
                    return context;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error creating dbContext for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                context.Dispose();
                throw new Exception("Error creating dbContext for reason: " + e.Message);
            }
            return context;
        }
    }
}
