﻿using RuneComp;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using RuneDB.Utils.ErrorMessages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RuneDB.Services
{
    public class RuneDbService : IRuneDBService
    {
        // TODO: Consider refactoring methods to throw exceptions but wrap these into a wrapper exception with a more readable message
        // E.g. Connection to database failed -> innerexception: SqlConntectorException at 512 in test.cs ...
        // TODO: Consider refactoring methods to asnyc
        private IRuneContextFactory _contextFactory;
        public RuneDbService(IRuneContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public void StoreHighAlchHistory(IDictionary<int, RuneHighAlchHistoryEntry> entries, IEnumerable<int> itemIds)
        {
            throw new NotImplementedException();
        }

        public void StoreItems(IEnumerable<RuneItem> items)
        {
            if (items == null || !items.Any()) return;
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    context.RuneItems.AddRange(items);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error storing items for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(
                    DbGeneralErrors.BuildMessage(DbGeneralErrors.STORE_ENTITY, "items"), e);
            }
        }

        public void StoreLowAlchHistory(IDictionary<int, RuneLowAlchHistoryEntry> entries, IEnumerable<int> itemIds)
        {
            throw new NotImplementedException();
        }

        public void StorePriceHistory(IDictionary<int, RunePriceHistoryEntry> entries, IEnumerable<int> itemIds)
        {
            if (entries == null) return;
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    if (context != null)
                    {
                        var dbItemIdsAndPrices = context.RuneItems.Where(dbItem => entries.Keys.Contains(dbItem.Id)).Select(dbItem => new
                        {
                            dbItem.Id,
                            Price = dbItem.CurrentGePrice
                        });
                        foreach (var dbItemIdAndPrice in dbItemIdsAndPrices)
                        {
                            context.RunePriceHistoryEntries.Add(entries[dbItemIdAndPrice.Id]);
                            RuneItem item = new RuneItem() { Id = dbItemIdAndPrice.Id };
                            context.RuneItems.Attach(item);
                            item.CurrentPriceChange = dbItemIdAndPrice.Price - entries[dbItemIdAndPrice.Id].Price;
                            item.CurrentGePrice = entries[dbItemIdAndPrice.Id].Price;
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error storing price history for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(
                    DbGeneralErrors.STORE_ENTITY, "price history"), e);
            }
        }

        public void StoreCategory(RuneCategory category)
        {
            if (category == null) return;
            if (string.IsNullOrWhiteSpace(category.Name))
            {
                throw new ArgumentException(DbGeneralErrors.STORE_CAT_EMPTY_NAME);
            }
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    if (category != null && !ExistsCategory(category))
                    {
                        context.RuneCategories.Add(category);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException(
                    DbGeneralErrors.BuildMessage(DbGeneralErrors.STORE_ENTITY, "category"), e);
            }
        }

        public void StoreCategories(IEnumerable<RuneCategory> categories)
        {
            if (categories == null) return;
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    foreach (var category in categories)
                    {
                        if (category == null || ExistsCategory(category)) continue;
                        context.RuneCategories.Add(category);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error storing categories for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.STORE_ENTITY, "categories"), e);
            }
        }

        public void StoreRegressionModels(IEnumerable<RuneRegressionModel> models, PredictableProperty predictableProperty)
        {
            if (models == null) throw new ArgumentException(DbGeneralErrors.EMPTY_REGRESSION_MODEL);
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    int predictablePropId = context.PredictableProperties.AsEnumerable()
                        .Where(x => x.PredictableProperty == predictableProperty).Select(x => x.PredictablePropertyId).FirstOrDefault();
                    if (predictablePropId == 0)
                    {
                        throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.NO_PREDICTABLE_PROP_FOUND, predictableProperty.ToString()));
                    }

                    foreach (var model in models)
                    {
                        if (model.PredictableProperty != predictableProperty)
                        {
                            throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.PREDICTABLE_PROP_MISSMATCH, new object[]
                            {
                                predictableProperty.ToString(), model.PredictableProperty.ToString()
                            }));
                        }
                        RuneRegressionModel dbModel = context.RegressionModels.Where(x => x.ItemId == model.ItemId).FirstOrDefault();
                        if (model == null) continue;
                        if (dbModel == null)
                        {
                            model.SetPredictablePropertyId(predictablePropId);
                            context.RegressionModels.Add(model);
                        }
                        else if (dbModel.Degree == model.Degree)
                        {
                            dbModel.Degree = model.Degree;
                            dbModel.Coefficients = model.Coefficients;
                            context.RegressionModels.Update(dbModel);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error storing regression models for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.STORE_ENTITY, "regression models"), e);
            }
        }

        public void UpdateImageSprites(IList<int> itemIds, IList<byte[]> itemSprites)
        {
            if (itemIds == null || itemSprites == null || itemIds.Count != itemSprites.Count)
            {
                throw new ArgumentException(DbGeneralErrors.ITEMS_SPRITE_SIZE_MISSMATCH);
            }
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    var dbItemIds = context.RuneItems.Where(x => itemIds.Contains(x.Id)).Select(x => x.Id);
                    IEnumerator<byte[]> itemSpriteEnumerator = itemSprites.GetEnumerator();
                    itemSpriteEnumerator.MoveNext();
                    foreach (var dbItemId in dbItemIds)
                    {
                        RuneItem item = new RuneItem() { Id = dbItemId };
                        context.RuneItems.Attach(item);
                        item.Icon = itemSpriteEnumerator.Current;
                        itemSpriteEnumerator.MoveNext();
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.STORE_ENTITY, "item sprites"), e);
            }
        }

        public ISet<int> FetchItemIdsHashSet()
        {
            HashSet<int> itemData = new HashSet<int>();
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    if (context != null)
                    {
                        var itemIds = context.RuneItems.Select(x => x.Id).AsEnumerable();

                        foreach (var itemId in itemIds)
                        {
                            itemData.Add(itemId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching item ids and name for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return itemData;
        }

        public IEnumerable<int> FetchItemIds()
        {
            List<int> itemIds = new List<int>();
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    foreach (var itemId in context.RuneItems.Select(x => x.Id))
                    {
                        itemIds.Add(itemId);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching item ids for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(
                    DbGeneralErrors.BuildMessage(DbGeneralErrors.ERR_FETCHING_PROPERTY, "item ids"));
            }
            return itemIds;
        }

        public IEnumerable<(string, int)> FetchItemPrefixes()
        {
            List<(string, int)> itemPrefixes = new List<(string, int)>();
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    if (context != null)
                    {
                        itemPrefixes.AddRange(context.RuneItems.Select(x => x.Name).AsEnumerable()
                            .Select(result => result.Split(' ')[0].Length < 3 ? result : result.Split(' ')[0]).GroupBy(name => name)
                            .Select(result => (result.Key, result.Count())));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching item prefixes for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.ERR_FETCHING_PROPERTY, "item prefixes"), e);
            }
            return itemPrefixes;
        }

        public IEnumerable<(string, int)> FetchMultiItemPrefixes()
        {
            List<(string, int)> prefixList = new List<(string, int)>();
            try
            {
                prefixList.AddRange(FetchItemPrefixes().Where(x => x.Item2 > 1));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching multi item prefixes: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.ERR_FETCHING_PROPERTY, "multi item prefixes"), e);
            }
            //return prefixList.Take(6);
            return prefixList;
        }

        public IEnumerable<(string, int)> FetchItemsforSingleItemPrefixes()
        {
            List<(string, int)> itemIds = new List<(string, int)>();
            try
            {
                IEnumerable<(string, int)> itemPrefixes = FetchItemPrefixes();
                using (var context = _contextFactory.GetContext())
                {
                    HashSet<string> prefixList = new HashSet<string>();
                    IEnumerable<string> singleItemPrefixes = itemPrefixes.Where(x => x.Item2 == 1).AsEnumerable().Select(x => x.Item1);
                    foreach (var singleItemPrefix in singleItemPrefixes)
                    {
                        prefixList.Add(singleItemPrefix);
                    }
                    itemIds.AddRange(context.RuneItems.AsEnumerable().Where(x => CheckItemNameAndPrefix(x.Name, prefixList)).Select(y => (y.Name, y.Id)));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching item prefixes for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.ERR_FETCHING_PROPERTY, "item prefixes"), e);
            }
            return itemIds;
        }

        public (IEnumerable<(int, int)>, DateTime) FetchItemPricesAndCurrentTimeDiff(int itemId, int elemCount, bool takeRandom)
        {
            List<(int, int)> priceEntries = new List<(int, int)>();
            DateTime startingDate = DateTime.MinValue;
            if (itemId < 0 || elemCount <= 0) throw new ArgumentException(DbGeneralErrors.ERR_FETCHING_PRICES_ADV);
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    if (context.RuneItems.FirstOrDefault(x => x.Id == itemId) == null)
                    {
                        throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.ITEM_DOES_NOT_EXIST, itemId.ToString()));
                    }

                    var fetchedPriceEntries = context.RunePriceHistoryEntries.Where(x => x.ItemId == itemId)
                        .Select(x => new { x.Date, x.Price }).OrderByDescending(x => x.Date).AsEnumerable();
                    
                    //startingDate = fetchedPriceEntries.OrderBy(x => x.Date).First().Date;
                    if (!takeRandom)
                    {
                        fetchedPriceEntries = fetchedPriceEntries.Take(elemCount);
                        startingDate = fetchedPriceEntries.Last().Date;
                        int cnt = 0;
                        foreach (var entry in fetchedPriceEntries.Take(elemCount))
                        {
                            if (cnt >= elemCount) break;
                            priceEntries.Add((entry.Price, Convert.ToInt32(Math.Ceiling(entry.Date.Subtract(startingDate).TotalHours))));
                            cnt++;
                        }
                    }
                    else
                    {
                        throw new NotImplementedException(DbGeneralErrors.NO_RANDOM_SELECT_SUPPORT);
                    }
                }
                if (priceEntries.Count < elemCount)
                {
                    throw new ArgumentException(DbGeneralErrors.BuildMessage(
                        DbGeneralErrors.NOT_ENOUGH_PRICE_ENTRIES, new object[]
                        {
                            elemCount.ToString(), priceEntries.Count.ToString()
                        }));
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException(DbGeneralErrors.BuildMessage(DbGeneralErrors.ERR_FETCHING_PRICES, itemId.ToString()), e);
            }
            return (priceEntries, startingDate);
        }

        public (IDictionary<string, int>, IList<object[]>) ExecuteRuneSqlQuery(RuneSqlQuery query)
        {
            using (var context = _contextFactory.GetContext())
            {
                return context.ExecuteRuneSqlQuery(query);
            }
        }

        public (IDictionary<string, int>, IList<object[]>) RunExecutionPlan(RuneSqlExecutionPlan plan)
        {
            if (plan == null) throw new ArgumentException("Cannot execute empty plan !");
            IDictionary<string, int> resultPropDict = new Dictionary<string, int>();
            List<object[]> resultRows = new List<object[]>();

            IDictionary<string, int> predPropsDict = new Dictionary<string, int>();
            IDictionary<int, object[]> valuesToSubstitute = new Dictionary<int, object[]>();
            List<int> filteredItemIds = new List<int>();
            // Stores the index of the row in the endresult for every item id when predictions are made
            IDictionary<int, int> filterItemIdsDict = new Dictionary<int, int>();
            int filteredItemIdsColIdx = -1;

            int propIdx = 0;
            foreach (var resultProp in plan.ResultProps)
            {
                resultPropDict.Add(resultProp, propIdx);
                propIdx++;
            }
            (IDictionary<string, int> queryPropDict, IList<object[]> queryResults) = (new Dictionary<string, int>(), new List<object[]>());
            foreach (var query in plan.Queries)
            {
                if (query.IsPrediction)
                {
                    if (predPropsDict.Count == 0)
                    {
                        predPropsDict = FetchPredictableProperties();
                    }
                    if (query.QueryParams.ContainsKey("item_id"))
                    {
                        if (filteredItemIds == null || filteredItemIds.Count == 0)
                        {
                            throw new ArgumentException(DbGeneralErrors.NO_VALS_FOR_PRESELECTION);
                        }
                        query.UpdateQueryParam("item_id", filteredItemIds);
                    }
                    if (query.QueryParams.ContainsKey("predictable_property_id"))
                    {
                        if (query.SubstitutedProperty == null || string.IsNullOrWhiteSpace(query.SubstitutedProperty.PropertyName))
                        {
                            throw new ArgumentException(DbGeneralErrors.INVALID_SUBSTITUTION_PROP);
                        }
                        int predictablePropIdx = predPropsDict[query.SubstitutedProperty.PropertyName];
                        query.UpdateQueryParam("predictable_property_id", predictablePropIdx);
                    }
                }
                (queryPropDict, queryResults) = ExecuteRuneSqlQuery(query);
                if (queryPropDict == null || queryResults == null || queryPropDict.Count == 0)
                {
                    throw new ArgumentException(DbGeneralErrors.NO_PROPS_SELECTED);
                }

                if (!query.IsPrediction)
                {
                    int[] mappingIdx = new int[queryPropDict.Count];
                    filteredItemIdsColIdx = queryPropDict.ContainsKey("item_id") ? queryPropDict["item_id"] : -1;
                    foreach (KeyValuePair<string, int> propNameIdxPair in queryPropDict)
                    {
                        // Creates a mapping between the index of the query result and the overall query results
                        // e.g. over result should look like this [item_name, item_price, description] [0, 1, 2]
                        // First query result looks like [item_name, description] [0, 1]
                        // The mapping array for the first query would look like this: [0 -> 0, 1 -> 2]
                        // with x -> y and x being the idx of the query result and y the index in the end result
                        if (resultPropDict.ContainsKey(propNameIdxPair.Key))
                        {
                            mappingIdx[propNameIdxPair.Value] = resultPropDict[propNameIdxPair.Key];
                        }
                    }
                    object[] endResultRow;
                    int resultRowCnt = 0;
                    foreach (object[] queryResultRow in queryResults)
                    {
                        endResultRow = new object[propIdx];
                        for (int i = 0; i < queryResultRow.Length; i++)
                        {
                            endResultRow[mappingIdx[i]] = queryResultRow[i];
                            if (filteredItemIdsColIdx >= 0 && i == filteredItemIdsColIdx)
                            {
                                filteredItemIds.Add((int)queryResultRow[i]);
                                filterItemIdsDict.Add((int)queryResultRow[i], resultRowCnt);
                            }
                        }
                        resultRowCnt++;
                        resultRows.Add(endResultRow);
                    }
                }
                else
                {
                    int numResultRows = resultRows.Count;
                    int numPredictions = query.SubstitutedPropertyPredictions.Count;
                    int[] substitutedIdxArr = new int[numPredictions];
                    string[] queryResultProps = new string[queryPropDict.Count];
                    // Stores for each prediction the result for each row
                    // e.g. Prediction_1 -> result_per_row[]
                    // Prediction_2 -> result_per_row[]
                    object[][] predictionVals = new object[numPredictions][];
                    Property predProp = query.SubstitutedProperty;

                    foreach (KeyValuePair<string, int> kvPair in queryPropDict)
                    {
                        // Add mapping between the column name in result to the index in the result row
                        queryResultProps[kvPair.Value] = kvPair.Key;
                    }
                    for (int i = 0; i < numPredictions; i++)
                    {
                        predictionVals[i] = new object[numResultRows];
                    }
                    for (int resultRowIdx = 0; resultRowIdx < numResultRows; resultRowIdx++)
                    {
                        RuneRegressionModel model = new RuneRegressionModel(queryResultProps, queryResults[resultRowIdx]);
                        model.ExtractCoefficientsFromString();

                        for (int predictionIdx = 0; predictionIdx < numPredictions; predictionIdx++)
                        {
                            predProp.Prediction = query.SubstitutedPropertyPredictions[predictionIdx];
                            predictionVals[predictionIdx][filterItemIdsDict[model.ItemId]] = model.PredictValue(predProp.Prediction.ConverToDateTime());
                        }
                    }
                    int predictionNum = 0;
                    foreach (var prediction in query.SubstitutedPropertyPredictions)
                    {
                        predProp.Prediction = prediction;
                        valuesToSubstitute.Add(resultPropDict[predProp.ToString()], predictionVals[predictionNum]);
                        predictionNum++;
                    }
                }
            }
            // Substitute the prediction results into the final result
            if (valuesToSubstitute.Count > 0)
            {
                for (int i = 0; i < resultRows.Count; i++)
                {
                    foreach (var (endResultIdx, valsToSub) in valuesToSubstitute)
                    {
                        if (valsToSub == null)
                        {
                            throw new ArgumentException(DbGeneralErrors.NO_VALS_FOR_SUBSTITUTION);
                        }
                        resultRows[i][endResultIdx] = valsToSub[i];
                    }
                }
            }
            return (resultPropDict, resultRows);
        }

        public IDictionary<string, int> FetchPredictableProperties()
        {
            Dictionary<string, int> predictablePropDict = new Dictionary<string, int>();
            using (var context = _contextFactory.GetContext())
            {
                foreach (var entry in context.PredictableProperties.Select(x => new
                {
                    x.PredictablePropertyId,
                    x.PredictablePropertyName
                }))
                {
                    predictablePropDict.TryAdd(entry.PredictablePropertyName, entry.PredictablePropertyId);
                }
            }
            return predictablePropDict;
        }

        public bool ExistsCategory(RuneCategory category)
        {
            if (category == null || string.IsNullOrEmpty(category.Name) || category.Id < 0) return false;
            try
            {
                using (var context = _contextFactory.GetContext())
                {
                    return context.RuneCategories.FirstOrDefault(x => x.Name.Equals(category.Name)) != null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error checking if category already exists reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return false;
        }

        private bool CheckItemNameAndPrefix(string itemName, ISet<string> prefixNames)
        {
            if (string.IsNullOrWhiteSpace(itemName) || prefixNames == null) return false;
            try
            {
                string itemPrefix = itemName.Split(' ')[0];
                if (itemPrefix.Length < 3)
                {
                    itemPrefix = itemName;
                }
                return prefixNames.Contains(itemPrefix);
            }
            catch (Exception)
            {
            }
            return false;
        }
    }
}
