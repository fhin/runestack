﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using NpgsqlTypes;
using RuneComp;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using RuneDB.Services;
using System;
using System.Collections.Generic;

namespace RuneDB
{
    public class RuneDBContext : DbContext, IRuneDB
    {
        private IDictionary<Type, NpgsqlDbType> _dbTypeMapping;

        public DbSet<RuneItem> RuneItems { get; set; }
        public DbSet<RuneCategory> RuneCategories { get; set; }
        public DbSet<RunePriceHistoryEntry> RunePriceHistoryEntries { get; set; }
        public DbSet<RuneLowAlchHistoryEntry> LowAlchHistoryEntries { get; set; }
        public DbSet<RuneHighAlchHistoryEntry> HighAlchHistoryEntries { get; set; }
        public DbSet<RuneRegressionModel> RegressionModels { get; set; }
        public DbSet<RunePredictableProperty> PredictableProperties { get; set; }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(ConfigurationManager.ConnectionStrings["RuneDBConnection"].ConnectionString);
            }
        }
        */

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RuneItem>(rItem =>
            {
                rItem.HasOne<RuneCategory>().WithMany().HasForeignKey(c => c.RuneCategoryId);
            });
            modelBuilder.Entity<RunePriceHistoryEntry>(rHistEntry =>
            {
                rHistEntry.HasOne<RuneItem>().WithMany().HasForeignKey(c => c.ItemId);
            });
            modelBuilder.Entity<RuneLowAlchHistoryEntry>(rLHistentry =>
            {
                rLHistentry.HasOne<RuneItem>().WithMany().HasForeignKey(c => c.ItemId);
            });
            modelBuilder.Entity<RuneHighAlchHistoryEntry>(rHHistentry =>
            {
                rHHistentry.HasOne<RuneItem>().WithMany().HasForeignKey(c => c.ItemId);
            });
            modelBuilder.Entity<RuneCategory>(rCat =>
            {
                rCat.HasIndex(x => x.Name).IsUnique();
            });
            modelBuilder.Entity<RuneRegressionModel>(rRegModel =>
            {
                rRegModel.HasKey(c => new { c.ItemId, c.PredictablePropertyId });
                rRegModel.HasOne<RuneItem>().WithMany().HasForeignKey(c => c.ItemId);
                rRegModel.HasOne<RunePredictableProperty>().WithMany().HasForeignKey(c => c.PredictablePropertyId);
            });
            modelBuilder.Entity<RunePredictableProperty>(rPredProp =>
            {
                rPredProp.HasIndex(c => c.PredictablePropertyName).IsUnique();
            });
        }

        public bool CanConnectToDB()
        {
            return Database.CanConnect();
        }

        public (IDictionary<string, int>, IList<object[]>) ExecuteRuneSqlQuery(RuneSqlQuery query)
        {
            if (query == null || string.IsNullOrWhiteSpace(query.SqlQuery)) throw new ArgumentException("Cannot execute empty query !");

            Dictionary<string, int> resultColumns = new Dictionary<string, int>();
            List<object[]> resultData = new List<object[]>();
            using (var connection = (NpgsqlConnection)Database.GetDbConnection())
            {
                // Start a transaction as it is required to work with cursors in PostgreSQL
                //NpgsqlTransaction tran = conn.BeginTransaction();

                using (NpgsqlCommand cmd = new NpgsqlCommand(query.SqlQuery, connection))
                {
                    if (query.ExpectedQueryParams > 0)
                    {
                        if (query.QueryParams == null || query.ExpectedQueryParams != query.QueryParams.Count)
                        {
                            throw new ArgumentException("Expected " + query.ExpectedQueryParams + " query parameters but got "
                          + query.QueryParams == null ? "0" : query.QueryParams.Count.ToString());
                        }
                        foreach (KeyValuePair<string, (Type, object)> queryParam in query.QueryParams)
                        {
                            (Type paramType, object paramValue) = queryParam.Value;
                            if (string.IsNullOrWhiteSpace(queryParam.Key))
                            {
                                throw new ArgumentException("Cannot add query param with no key");
                            }
                            else if (paramType == null || paramValue == null)
                            {
                                throw new ArgumentException("Query param type and value cannot be null");
                            }
                            NpgsqlParameter param = null;
                            NpgsqlDbType paramDbType = NpgsqlDbType.Unknown;
                            if (paramType.IsPrimitive || paramType == typeof(string))
                            {
                                paramDbType = GetNpgsqlDbType(paramType);
                            }
                            else if (paramType.IsArray)
                            {
                                // See section write mappings: https://www.npgsql.org/doc/types/basic.html
                                paramDbType = NpgsqlDbType.Array | GetNpgsqlDbType(paramType.GetElementType());
                            }
                            else
                            {
                                foreach (var typeInterface in paramType.GetInterfaces())
                                {
                                    if (typeInterface.IsGenericType &&
                                        typeInterface.GetGenericTypeDefinition() == typeof(IList<>))
                                    {
                                        paramDbType = NpgsqlDbType.Array | GetNpgsqlDbType(typeInterface.GetGenericArguments()[0]);
                                        break;
                                    }
                                }
                            }
                            if (paramDbType == NpgsqlDbType.Unknown)
                            {
                                throw new ArgumentException("No mapping for given type " + paramType.ToString() + " was found");
                            }
                            param = new NpgsqlParameter(queryParam.Key, paramDbType)
                            {
                                Value = paramValue
                            };
                            if (param == null)
                            {
                                throw new ArgumentException("Could not create parameter for query param " + queryParam.Key);
                            }
                            cmd.Parameters.Add(param);
                        }
                    }
                    connection.Open();
                    cmd.Prepare();
                    using (NpgsqlDataReader reader = cmd.ExecuteReader())
                    {
                        int columnIdx = 0;
                        foreach (var column in reader.GetColumnSchema())
                        {
                            resultColumns.Add(column.ColumnName, columnIdx);
                            columnIdx++;
                        }
                        int columnCnt = 0;
                        while (reader.Read())
                        {
                            columnCnt = reader.VisibleFieldCount;
                            if (columnCnt != resultColumns.Count)
                            {
                                throw new Exception("Expected " + resultColumns.Count.ToString()
                                    + " columns per row but got " + columnCnt.ToString());
                            }
                            object[] resultRow = new object[columnCnt];
                            for (int i = 0; i < columnCnt; i++)
                            {
                                resultRow[i] = reader[i];
                            }
                            resultData.Add(resultRow);
                        }
                    }
                }
            }
            return (resultColumns, resultData);
        }

        public RuneDBContext()
            : base()
        {
            FillDbTypeMapping();
        }

        public RuneDBContext(DbContextOptions<RuneDBContext> options)
            : base(options)
        {
            FillDbTypeMapping();
        }

        private NpgsqlDbType GetNpgsqlDbType(Type type)
        {
            if (type == null) throw new ArgumentException("Null type has no npsqldbtype mapping");
            if (_dbTypeMapping == null || _dbTypeMapping.Count == 0 || !_dbTypeMapping.ContainsKey(type))
            {
                throw new ArgumentException("No mapping for the provided .net type !");
            }
            return _dbTypeMapping[type];
        }

        private void FillDbTypeMapping()
        {
            _dbTypeMapping = new Dictionary<Type, NpgsqlDbType>
            {
                { typeof(int), NpgsqlDbType.Integer },
                { typeof(double), NpgsqlDbType.Real },
                { typeof(string), NpgsqlDbType.Text },
                { typeof(DateTime), NpgsqlDbType.Date },
                { typeof(bool), NpgsqlDbType.Boolean }
            };
        }
    }
}
