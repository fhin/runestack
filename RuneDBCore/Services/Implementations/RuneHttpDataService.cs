﻿using RuneDB.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RuneDB.Services
{
    public class RuneHttpDataService : IRuneDataService
    {
        private HttpClient _httpClient;
        private HttpRequestFactory _httpRequestFactory;
        private HttpRequestType lastSentRequest;
        private SemaphoreSlim semaphore;

        private Dictionary<HttpRequestType, int> delayForRequest;
        private Dictionary<HttpRequestType, DateTime> lastTimeRequestSentDict;
        private Dictionary<HttpRequestType, bool> canSendRequest;

        public RuneHttpDataService(HttpClient httpClient, HttpRequestFactory httpRequestFactory)
        {
            _httpClient = httpClient;
            if (_httpClient != null)
            {
                _httpClient.DefaultRequestHeaders.Accept.Clear();
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            _httpRequestFactory = httpRequestFactory;
            lastSentRequest = HttpRequestType.NONE;
            semaphore = new SemaphoreSlim(1, 1);
            InitRequestDelays();
        }

        private void InitRequestDelays()
        {
            if (delayForRequest == null)
            {
                delayForRequest = new Dictionary<HttpRequestType, int>();
            }
            if (lastTimeRequestSentDict == null)
            {
                lastTimeRequestSentDict = new Dictionary<HttpRequestType, DateTime>();
            }
            if (canSendRequest == null)
            {
                canSendRequest = new Dictionary<HttpRequestType, bool>();
            }

            delayForRequest.Add(HttpRequestType.JAGEX_API_ICON, RuneRequestConsts.JAGEX_API_ICON_DELAY);
            delayForRequest.Add(HttpRequestType.JAGEX_API_PAGE, RuneRequestConsts.JAGEX_API_PAGE_DELAY);
            delayForRequest.Add(HttpRequestType.RUNEBOX_API_ITEM, RuneRequestConsts.RUNEBOX_ITEM_DELAY);
            delayForRequest.Add(HttpRequestType.NONE, 0);

            lastTimeRequestSentDict.Add(HttpRequestType.JAGEX_API_ICON, DateTime.MinValue);
            lastTimeRequestSentDict.Add(HttpRequestType.JAGEX_API_PAGE, DateTime.MinValue);
            lastTimeRequestSentDict.Add(HttpRequestType.RUNEBOX_API_ITEM, DateTime.MinValue);
            lastTimeRequestSentDict.Add(HttpRequestType.NONE, DateTime.MinValue);

            canSendRequest.Add(HttpRequestType.JAGEX_API_ICON, true);
            canSendRequest.Add(HttpRequestType.JAGEX_API_PAGE, true);
            canSendRequest.Add(HttpRequestType.RUNEBOX_API_ITEM, true);
            canSendRequest.Add(HttpRequestType.NONE, true);
        }

        public async Task<string> FetchCategoryData(int categoryId)
        {
            if (categoryId < 0) return "";
            try
            {
                RuneRequest request = _httpRequestFactory.GetRequestForType(RequestType.NumItemsForCategory);
                if (request.Parameters.Count == 0)
                {
                    request.AddHeaderValue("category", categoryId);
                }
                else
                {
                    request.Parameters["category"].Value = categoryId;
                }
                return await SendRequest(request.BuildQueryString(), true, false, HttpRequestType.JAGEX_API_PAGE);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public async Task<byte[]> FetchAsyncItemBigSprite(int itemId)
        {
            if (itemId < 0) throw new ArgumentOutOfRangeException();

            try
            {
                RuneRequest request = _httpRequestFactory.GetRequestForType(RequestType.ItemSprite);
                request.AddHeaderValue("id", itemId);
                Task<string> result = Task.Run(() => SendRequest(request.BuildQueryString(), false, true, HttpRequestType.JAGEX_API_ICON));
                return Convert.FromBase64String(await result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching item icon for reason: " + e.Message);
                return new byte[0];
            }
        }

        public async Task<string> FetchAsyncPageData(int categoryId, char letter, int page)
        {
            if (categoryId < 0 || page < 0) return "";
            try
            {
                RuneRequest request = _httpRequestFactory.GetRequestForType(RequestType.ItemsForCategory);
                request.AddHeaderValue("category", categoryId);
                request.AddHeaderValue("alpha", letter);
                request.AddHeaderValue("page", page);
                return await Task.Run(() => SendRequest(request.BuildQueryString(), true, false, HttpRequestType.JAGEX_API_PAGE));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "";
            }
        }

        public IEnumerable<Task<string>> FetchAsyncAllCategoryPageData(RuneCategory category)
        {
            List<Task<string>> pageDataTasks = new List<Task<string>>();
            if (category == null || category.Id < 0 || category.LetterEntries == null) return pageDataTasks;
            try
            {
                foreach (KeyValuePair<char, RuneCategoryEntry> letterEntry in category.LetterEntries)
                {
                    if (letterEntry.Value == null) continue;

                    int numItems = letterEntry.Value.NumItems;
                    int numPages = Convert.ToInt32(numItems / RuneRequestConsts.MAX_ITEMS_PER_PAGE);
                    int numItemsLastPage = numItems % RuneRequestConsts.MAX_ITEMS_PER_PAGE;
                    if (numItemsLastPage > 0)
                    {
                        numPages++;
                    }

                    for (var currPage = 1; currPage < numPages + 1; currPage++)
                    {
                        pageDataTasks.Add(FetchAsyncPageData(category.Id, letterEntry.Key, currPage));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error fetching pageData for category " + (string.IsNullOrWhiteSpace(category.Name) ? "<NO_NAME_CAT> with id" + category.Id
                    : category.Name));
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return pageDataTasks;
        }

        public Task<string> FetchAsyncItemAdvData(int itemId)
        {
            if (itemId < 0) return Task.FromResult("");

            try
            {
                RuneRequest request = HttpRequestFactory.GetInstance.GetRequestForType(RequestType.ItemAdvDetails);
                request.AddHeaderValue("item", itemId);
                return Task.Run(() => SendRequest(request.BuildQueryString(), true, false, HttpRequestType.RUNEBOX_API_ITEM));
            }
            catch (Exception)
            {
                return Task.FromResult("");
            }
        }

        public Task<string> FetchItemPrice(int itemId)
        {
            if (itemId < 0) return Task.FromResult("");

            try
            {
                RuneRequest request = HttpRequestFactory.GetInstance.GetRequestForType(RequestType.SingleItemPrice);
                request.AddHeaderValue("item", itemId);
                return SendRequest(request.BuildQueryString(), false, false, HttpRequestType.JAGEX_API_PAGE);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Task.FromResult("");
            }
        }

        private Task<string> SendRequest(string requestUrl, bool responseAsJson, bool responseIsImage, HttpRequestType httpRequestType)
        {
            Task<string> requestTask = Task.Run(async () =>
            {
                string result = "";
                try
                {
                    while (true)
                    {
                        if (lastSentRequest == httpRequestType)
                        {
                            await Task.Delay(RuneRequestConsts.GENERAL_BACKOFF_TIME);
                        }
                        //Console.WriteLine("Waiting for lock for request " + requestUrl);
                        await semaphore.WaitAsync();
                        //Console.WriteLine("Got lock for request " + requestUrl);
                        int msDelayForNextRequest = delayForRequest[httpRequestType];
                        TimeSpan ts = DateTime.Now - lastTimeRequestSentDict[httpRequestType];

                        if (ts.TotalMilliseconds < msDelayForNextRequest)
                        {
                            if (semaphore.CurrentCount == 0)
                            {
                                semaphore.Release();
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    result = await SendRequest(requestUrl, responseAsJson, responseIsImage);
                    lastTimeRequestSentDict[httpRequestType] = DateTime.Now;
                    lastSentRequest = httpRequestType;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Stacktrace: " + e.StackTrace);
                }
                finally
                {
                    semaphore.Release();
                }
                return result;
            });
            return requestTask;
        }

        private async Task<string> SendRequest(string requestUrl, bool responseAsJson, bool responseIsImage)
        {
            Uri requestUri = new Uri(requestUrl);
            string result = "";
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                if (responseIsImage)
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("image/gif"));
                }
                if (responseAsJson)
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                }

                //Console.WriteLine("Sending request " + requestUrl + " at: " + DateTime.Now.ToString());
                using (HttpResponseMessage msg = await _httpClient.SendAsync(request))
                {
                    //Console.WriteLine("Received response for request " + requestUrl + " at: " + DateTime.Now.ToString());
                    if (msg.IsSuccessStatusCode)
                    {
                        //result = await msg.Content.ReadAsStringAsync();

                        if (responseIsImage)
                        {
                            result = Convert.ToBase64String(await msg.Content.ReadAsByteArrayAsync());
                        }
                        else
                        {
                            result = await msg.Content.ReadAsStringAsync();
                        }

                        if (string.IsNullOrWhiteSpace(result))
                        {
                            Console.WriteLine("<WARNING> No data returned for request " + requestUrl);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error sending request ");
                        Console.Write("Status code: " + msg.StatusCode.ToString() + " | " + msg.ReasonPhrase);
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error sending request for reason: " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return result;
        }

    }
}
