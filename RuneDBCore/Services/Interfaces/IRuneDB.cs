﻿using Microsoft.EntityFrameworkCore;
using RuneComp;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using System;
using System.Collections.Generic;

namespace RuneDB.Services
{
    public interface IRuneDB : IDisposable
    {
        DbSet<RuneItem> RuneItems { get; set; }
        DbSet<RuneCategory> RuneCategories { get; set; }
        DbSet<RunePriceHistoryEntry> RunePriceHistoryEntries { get; set; }
        DbSet<RuneRegressionModel> RegressionModels { get; set; }
        DbSet<RunePredictableProperty> PredictableProperties { get; set; }

        int SaveChanges();
        bool CanConnectToDB();
        (IDictionary<string, int>, IList<object[]>) ExecuteRuneSqlQuery(RuneSqlQuery query);
    }
}
