﻿using RuneDB.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RuneDB.Services
{
    public interface IRuneDataService
    {
        Task<string> FetchAsyncPageData(int categoryId, char letter, int page);
        IEnumerable<Task<string>> FetchAsyncAllCategoryPageData(RuneCategory category);
        Task<string> FetchAsyncItemAdvData(int itemId);
        Task<byte[]> FetchAsyncItemBigSprite(int itemId);
        Task<string> FetchCategoryData(int categoryId);
        Task<string> FetchItemPrice(int itemId);
    }
}
