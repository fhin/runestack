﻿namespace RuneDB.Services
{
    public interface IRuneContextFactory
    {
        IRuneDB GetContext();
    }
}
