﻿using RuneComp;
using RuneDB.Models;
using RuneDB.Models.RegressionModels;
using System;
using System.Collections.Generic;

namespace RuneDB.Services
{
    public interface IRuneDBService
    {
        void StoreCategory(RuneCategory category);
        void StoreCategories(IEnumerable<RuneCategory> categories);
        void StoreItems(IEnumerable<RuneItem> items);
        void StorePriceHistory(IDictionary<int, RunePriceHistoryEntry> entries, IEnumerable<int> itemIds);
        void StoreRegressionModels(IEnumerable<RuneRegressionModel> models, PredictableProperty predictableProperty);
        void UpdateImageSprites(IList<int> itemIds, IList<byte[]> itemSprites);

        ISet<int> FetchItemIdsHashSet();
        IEnumerable<int> FetchItemIds();
        IEnumerable<(string, int)> FetchItemPrefixes();
        IEnumerable<(string, int)> FetchMultiItemPrefixes();
        IEnumerable<(string, int)> FetchItemsforSingleItemPrefixes();
        (IEnumerable<(int, int)>, DateTime) FetchItemPricesAndCurrentTimeDiff(int itemId, int elemCount, bool takeRandom);
        IDictionary<string, int> FetchPredictableProperties();
        (IDictionary<string, int>, IList<object[]>) ExecuteRuneSqlQuery(RuneSqlQuery query);
        (IDictionary<string, int>, IList<object[]>) RunExecutionPlan(RuneSqlExecutionPlan plan);
        bool ExistsCategory(RuneCategory category);
    }
}
