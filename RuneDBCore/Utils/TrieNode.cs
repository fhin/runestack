﻿using System.Collections.Generic;

namespace RuneDB.Utils
{
    public class TrieNode
    {
        public char Letter { get; set; }
        public Dictionary<char, TrieNode> Nodes { get; set; }
        public int numSummarizedItems { get; set; }

        public TrieNode(char letter)
        {
            Letter = letter;
            Nodes = new Dictionary<char, TrieNode>();
        }
    }
}
