﻿using System;

namespace RuneDB.Utils
{
    public class MatrixUtils
    {
        public static double[,] TransposeMatrix(double[,] mat)
        {
            if (mat.Length == 0) return mat;
            int rowCnt = mat.GetLength(0);
            int colCnt = mat.GetLength(1);

            double[,] transposedMat = new double[colCnt, rowCnt];
            try
            {
                for (int j = 0; j < colCnt; j++)
                {
                    for (int i = 0; i < rowCnt; i++)
                    {
                        transposedMat[j, i] = mat[i, j];
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error transposing matrix for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                throw e;
            }
            return transposedMat;
        }

        public static double[,] CreateIdentityMat(int size)
        {
            if (size <= 0) throw new ArgumentException("Size of identity matrix needs to be >= 1");
            double[,] identityMat = new double[size, size];
            for (int i = 0; i < size; i++)
            {
                identityMat[i, i] = 1;
            }
            return identityMat;
        }

        public static void PrintMatrix(double[,] mat)
        {
            if (mat.GetLength(0) == 0 || mat.GetLength(1) == 0) return;
            int rowCnt = mat.GetLength(0);
            int colCnt = mat.GetLength(1);
            Console.Write("{");
            for (int i = 0; i < rowCnt; i++)
            {
                Console.Write("{");
                for (int j = 0; j < colCnt; j++)
                {
                    Console.Write(mat[i, j].ToString() + (j + 1 == colCnt ? "" : " | "));
                }
                Console.Write("}");
                if (i + 1 < rowCnt)
                {
                    Console.Write(",");
                    Console.WriteLine();
                }
            }
            Console.Write("}");
            Console.WriteLine("");
        }

        public static double[,] MultMats(double[,] matLeft, double[,] matRight)
        {
            if (matLeft.GetLength(1) != matRight.GetLength(0)) throw new ArgumentException("Cannot multiplay matrizes because cols (matLeft) != rows (matRight) !");
            double[,] resultMat = new double[matLeft.GetLength(0), matRight.GetLength(1)];
            int rowCntLeft = matLeft.GetLength(0);
            int colCntLeft = matLeft.GetLength(1);
            int rowCntRight = matRight.GetLength(0);
            int colCntRight = matRight.GetLength(1);

            double tmpSum = 0.0;
            for (int colR = 0; colR < colCntRight; colR++)
            {
                for (int rowL = 0; rowL < rowCntLeft; rowL++)
                {
                    tmpSum = 0.0;
                    for (int colL = 0; colL < colCntLeft; colL++)
                    {
                        tmpSum += matLeft[rowL, colL] * matRight[colL, colR];
                    }
                    resultMat[rowL, colR] = tmpSum;
                }
            }
            return resultMat;
        }

        public static double[,] MultMatVec(double[,] mat, double[] vec, bool colVec)
        {
            int rowCnt = mat.GetLength(0);
            int colCnt = mat.GetLength(1);
            if (vec.Length == 0 || rowCnt == 0 || colCnt == 0) throw new ArgumentException("Cannot multiply empty matrix or vector, check your inputs !");
            if ((!colVec && (colCnt > 1 || rowCnt != vec.Length)) || (colVec && colCnt != vec.Length)) throw new ArgumentException("Dimension mismatch between NxM matrix and 1xV or Vx1 vector (M must be equal to V) !");
            double[,] result = colVec ? new double[colCnt, 1] : new double[1, rowCnt];
            for (int i = 0; i < rowCnt; i++)
            {
                double tmpSum = 0;
                for (int j = 0; j < colCnt; j++)
                {
                    tmpSum += mat[i, j] * vec[j];
                }
                if (colVec)
                {
                    result[i, 0] = tmpSum;
                }
                else
                {
                    result[0, i] = tmpSum;
                }
            }
            return result;
        }

        /// <summary>
        /// Performs a decomposition of a quadratic matrix A into its lower and upper triangular matrix (L and U) 
        /// as well as a permutation matrix P using the Dolittle algorithm
        /// P*A = L*U
        /// </summary>
        /// <param name="mat">The quadratic matrix to decompose</param>
        /// <returns>A tuple containing the L and U matrix as a single matrix as well a vector representing the permutation matrix 
        /// (each entry represent the position of the 1 per row)</returns>
        public static (double[,], int[], int) LUPDecomposition(double[,] mat)
        {
            if (mat.GetLength(0) != mat.GetLength(1)) throw new ArgumentException("Can only invert a square matrix, check dimensions !");
            int N = mat.GetLength(0);
            double[,] identMat = CreateIdentityMat(N);
            int[] P = new int[N];
            double[,] LU = identMat;
            bool pivotSet = false;
            int exchangeCnt = 0;
            for (int j = 0; j < N; j++)
            {
                P[j] = j;
            }
            for (int row = 0; row < N; row++)
            {
                if (!pivotSet)
                {
                    P[row] = row;
                }
                else
                {
                    pivotSet = false;
                }
                // Pivot diagonal entry if it is zero
                if (mat[row, row] == 0)
                {
                    bool foundExchangeElem = false;
                    double maxElem = 0;
                    int maxElemIdx = row;
                    for (int j = row; j < N; j++)
                    {
                        if (mat[j, row] > maxElem)
                        {
                            maxElem = mat[j, row];
                            maxElemIdx = j;
                            foundExchangeElem = true;
                        }
                    }
                    if (!foundExchangeElem)
                    {
                        throw new ArgumentException("Tried to replace diagonal element with value 0 at [row/col]: ["
                            + row.ToString() + "/" + row.ToString() + "] but found no valid replacement item");
                    }
                    else
                    {
                        exchangeCnt++;
                        int oldPivotEntry = P[maxElemIdx];
                        P[maxElemIdx] = P[row];
                        P[row] = oldPivotEntry;
                        pivotSet = true;
                        for (int g = 0; g < N; g++)
                        {
                            double oldElem = mat[row, g];
                            mat[row, g] = mat[maxElemIdx, g];
                            mat[maxElemIdx, g] = oldElem;
                        }
                    }
                }
                // Set first row of upper diagonal matrix
                if (row == 0)
                {
                    for (int j = 0; j < N; j++)
                    {
                        LU[row, j] = mat[row, j];
                    }
                }
                else
                {
                    for (int col = 0; col < N; col++)
                    {
                        // Set first column of upper diagonal matrix
                        if (col == 0)
                        {
                            LU[row, col] = mat[row, col] / LU[col, col];
                        }
                        else
                        {
                            double tmpSum = mat[row, col];
                            for (int k = 0; k < col; k++)
                            {
                                tmpSum -= LU[row, k] * LU[k, col];
                            }
                            if (col >= row)
                            {
                                LU[row, col] = tmpSum;
                            }
                            else
                            {
                                if (LU[col, col] == 0)
                                {
                                    throw new ArgumentException("Cannot create LU decomposition for given matrix as it would result in division by zero " +
                                        "for result in entry [Row:" + row.ToString() + " | Col: " + col.ToString());
                                }
                                LU[row, col] = tmpSum / LU[col, col];
                            }
                        }
                    }
                }
            }
            return (LU, P, exchangeCnt);
        }

        public static double[,] InvertMat(double[,] mat)
        {
            if (mat.GetLength(0) == 0 || mat.GetLength(1) == 0 || mat.GetLength(0) != mat.GetLength(1)) throw new ArgumentException("Can only invert a quadratic matrix, check dimensions !");

            int matSize = mat.GetLength(0);
            double[,] invertedMat = new double[matSize, matSize];
            (double[,] LU, int[] P, int exchangeCnt) = LUPDecomposition(mat);

            if (CalcDeterminantLUPMatrix(LU, exchangeCnt) == 0) throw new ArgumentException("Cannot invert matrix as its determinant is zero !");

            double[] y = new double[matSize];
            double[] x = new double[matSize];
            double tmpSum = 0;
            for (int identCol = 0; identCol < matSize; identCol++)
            {
                // Compute y^i vector foreach column e^i of the identity matrix
                // L * y^i = e^i
                for (int i = 0; i < matSize; i++)
                {
                    tmpSum = 0;
                    if (P[i] == identCol)
                    {
                        tmpSum = 1;
                    }
                    for (int k = 0; k < i; k++)
                    {
                        tmpSum -= k != i ? LU[i, k] * y[k] : y[k];
                    }
                    y[i] = tmpSum;
                }
                // Compute each column a^(-i) of the inverse matrix a^(-1) given the vector y^i
                // U * a^(-i) = y^i
                for (int j = matSize - 1; j >= 0; j--)
                {
                    tmpSum = y[j];
                    for (int k = j + 1; k < matSize; k++)
                    {
                        tmpSum -= LU[j, k] * x[k];
                    }
                    x[j] = tmpSum / LU[j, j];
                    invertedMat[j, identCol] = x[j];
                }
                Array.Clear(x, 0, matSize);
                Array.Clear(y, 0, matSize);
            }
            return invertedMat;
        }

        public static double CalcDeterminantLUPMatrix(double[,] LU, int exchangeCnt)
        {
            if (LU.GetLength(0) != LU.GetLength(1)) throw new ArgumentException("Can only calculate determinant of square LU matrix");
            double det = Math.Pow(-1, exchangeCnt);
            for (int i = 0; i < LU.GetLength(0); i++)
            {
                det *= LU[i, i];
            }
            return det;
        }
    }
}
