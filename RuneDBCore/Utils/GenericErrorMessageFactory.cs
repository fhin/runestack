﻿namespace RuneDB.Utils
{
    public static class GenericErrorMessageFactory
    {
        public static readonly string EMPTY_REQUEST_HEADER_PARAM_NAME = "";
        public static readonly string DUPLICATE_REQUEST_HEADER_PARAM = "";
        public static readonly string INVALID_HEADER_PARAM_TYPE = "";
        public static readonly string NO_HEADER_FOR_NAME = "";
        public static readonly string INVALID_HEADER_FORMAT = "";
        public static readonly string HEADER_VALUE_TYPE_MISSMATCH = "Type missmatch between header type {0} and header value type {1} !";
        public static readonly string NO_PRIMITIVE_HEADER_VALUE = "Can only use header value with primitive or double and string type !";
    }
}
