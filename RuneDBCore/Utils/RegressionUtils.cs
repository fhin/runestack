﻿using RuneDB.Models;
using System;

namespace RuneDB.Utils
{
    public class RegressionUtils
    {
        /// <summary>
        /// {\displaystyle {\vec {y}}=\mathbf {X} {\vec {\beta }}+{\vec {\varepsilon }}.\,} TODO
        /// </summary>
        /// <param name="modelDegree"></param>
        /// <param name="inputVals"></param>
        /// <param name="outputVals"></param>
        /// <returns></returns>
        public static double[] GetRegressionCoeff(int modelDegree, double[] inputVals, double[] outputVals)
        {
            double[] regCoeff = new double[modelDegree + 1];
            int N = inputVals.Length;
            if (N == 0 || outputVals.Length == 0 || modelDegree <= 0) throw new ArgumentException("Cannot fit regression model to empty model, check your inputs !");
            if (N <= modelDegree || outputVals.Length <= modelDegree) throw new ArgumentException("Num. of input and output values needs to be at least equal to model degree -1");
            if (modelDegree > N) throw new ArgumentException("Model degree m needs to smaller than number of input samples n");

            double[,] X = new double[N, modelDegree + 1];
            double currX = 0.0;
            for (int j = 0; j < N; j++)
            {
                X[j, 0] = 1;
                currX = inputVals[j];
                for (int i = 1; i < modelDegree + 1; i++)
                {
                    X[j, i] = X[j, i - 1] * currX;
                }
            }
            double[,] XTranspose = MatrixUtils.TransposeMatrix(X);
            double[,] result = MatrixUtils.MultMatVec(MatrixUtils.MultMats(MatrixUtils.InvertMat(MatrixUtils.MultMats(XTranspose, X)), XTranspose), outputVals, true);
            for (int i = 0; i < modelDegree + 1; i++)
            {
                regCoeff[i] = result[i, 0];
            }
            return regCoeff;
        }
    }
}
