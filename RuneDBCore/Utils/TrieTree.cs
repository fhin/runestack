﻿using System;
using System.Collections.Generic;

namespace RuneDB.Utils
{
    /*
     * TODO: Keep TrieTree and TrieNode for now, refactoring needs to be done 
     *  (maybe use it item name recommendation in web client)
     * 
     *  - Remove prefix building
     *  + Add searching
    */
    public class TrieTree
    {
        public TrieNode root;
        private int currPrefixIdx = 0;
        public readonly char wordEndChar = '!';

        public TrieTree()
        {
            root = new TrieNode('#');
        }

        public void AddWord(string word)
        {
            if (string.IsNullOrEmpty(word)) throw new ArgumentException("Cannot add empty word");
            if (root == null) return;
            if (word.Contains(' '))
            {
                word = word.Split(' ')[0];
            }
            TrieNode curr = root;
            for (int i = 0; i < word.Length; i++)
            {
                char currChar = word[i];
                if (curr.Nodes.ContainsKey(currChar))
                {
                    curr.Nodes[currChar].numSummarizedItems++;
                    curr = curr.Nodes[currChar];
                }
                else
                {
                    TrieNode node = new TrieNode(currChar);
                    node.numSummarizedItems++;
                    curr.Nodes.Add(currChar, node);
  
                    if (curr.numSummarizedItems > curr.Nodes.Count)
                    {
                        TrieNode wordEndNode = new TrieNode(wordEndChar);
                        wordEndNode.numSummarizedItems++;
                        curr.Nodes.Add(wordEndChar, wordEndNode);
                    }
                    curr = node;
                }
            }
            root.numSummarizedItems++;
            if (curr != null && curr.Nodes.Count > 0)
            {
                TrieNode wordEndNode = new TrieNode(wordEndChar);
                wordEndNode.numSummarizedItems++;
                curr.numSummarizedItems++;
                curr.Nodes.Add(wordEndChar, wordEndNode);
            }
        }

        public void AddWords(IEnumerable<string> words)
        {
            if (words == null) return;
            foreach (string word in words)
            {
                AddWord(word);
            }
        }

        public List<(string, int)> GetItemPrefixes()
        {
            List<(string, int)> prefixL = new List<(string, int)>();
            if (root == null) return prefixL;
            return GetPrefixes(root, ref prefixL, true, 0);
        }

        private List<(string, int)> GetPrefixes(TrieNode currNode, ref List<(string, int)> items, bool isRoot, int currIdx)
        {
            if (currNode == null || currNode.Nodes == null || currIdx < 0 || currIdx > items.Count) return items;
            try
            {
                if (!isRoot)
                {
                    if (items.Count == 0)
                    {
                        items.Add((currNode.Letter.ToString(), 1));
                    }
                    else
                    {
                        for (int i = currIdx; i < items.Count; i++)
                        {
                            items[i] = (items[i].Item1 + currNode.Letter, currNode.numSummarizedItems);
                        }
                    }
                }
                if (currNode.Nodes == null || currNode.Nodes.Count == 0) return items;
                List<(string, int)> other = new List<(string, int)>();
                
                bool helperFlag = false;

                string currPrefix = "";
                if (items.Count > 0)
                {
                    currPrefix = items[items.Count - 1].Item1;
                }
                foreach (var node in currNode.Nodes)
                {
                    if (node.Key == wordEndChar)
                    {
                        items.Add((currPrefix, 1));
                        currPrefixIdx++;
                        other = items;
                    }
                    else
                    {
                        if (!helperFlag)
                        {
                            helperFlag = true;
                            other.AddRange(GetPrefixes(node.Value, ref items, false, currPrefixIdx));
                        }
                        else
                        {
                            items.Add((currPrefix, node.Value.numSummarizedItems));
                            //items.Add((currPrefix, 1));
                            currPrefixIdx++;
                            other = GetPrefixes(node.Value, ref items, false, currPrefixIdx);
                        }
                    }
                }
                return other;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return items;
            }
        }

        /*
        private List<(string, int)> GetPrefixes(TrieNode currNode, List<(string, int)> items, bool isRoot)
        {
            if (currNode == null || currNode.Nodes == null) return items;
            List<(string, int)> newItems = new List<(string, int)>();

            try
            {
                if (!isRoot)
                {
                    if (items.Count == 0)
                    {
                        newItems.Add((currNode.Letter.ToString(), 1));
                    }
                    else
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            newItems.Add((items[i].Item1 + currNode.Letter, currNode.numSummarizedItems));
                        }
                    }
                }
                if (currNode.Nodes == null || currNode.Nodes.Count == 0) return newItems;
                List<(string, int)> other = new List<(string, int)>();
                foreach (var node in currNode.Nodes)
                {
                    other.AddRange(GetPrefixes(node.Value, newItems, false));
                }
                return other;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return newItems;
            }
        }
        */
    }
}
