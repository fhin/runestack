﻿using System;

namespace RuneDB.Utils.ErrorMessages
{
    public class DbGeneralErrors
    {
        public const string STORE_CAT_EMPTY_NAME = "Cannot store category with empty name";
        public const string STORE_ENTITY = "Cannot store {0}, see inner exception for further infos";
        public const string EMPTY_REGRESSION_MODEL = "Cannot store empty regression model";
        public const string NO_PREDICTABLE_PROP_FOUND = "No predictable property found for given name {0}";
        public const string PREDICTABLE_PROP_MISSMATCH = "Cannot add model with prediction for different property"
                                + " <Expected | Actual>: <{0} | {1}>";
        public const string ITEMS_SPRITE_SIZE_MISSMATCH = "To update the item spires supply the same number of icons as well as images !";
        public const string ERR_FETCHING_PROPERTY = "Error fetching {0}, see inner exception for further infos";
        public const string NO_RANDOM_SELECT_SUPPORT = "Random sample selection currently not supported !";
        public const string ITEM_DOES_NOT_EXIST = "Item with id {0} does not exist !";
        public const string NOT_ENOUGH_PRICE_ENTRIES = "Could not fetch the required number of price entries." +
                    " Fetched [exp/actual]: {0} / {1}";
        public const string ERR_FETCHING_PRICES = "Error fetching item prices for item with id: {0}, see inner exception for further infos";
        public const string ERR_FETCHING_PRICES_ADV = "ItemId needs to be > 0 and elemCount must be > 0";
        public const string EMPTY_EXEC_PLAN = "Cannot execute empty execution plan";
        public const string NO_VALS_FOR_PRESELECTION = "Query expected parameter item_id but got no values for it";
        public const string INVALID_SUBSTITUTION_PROP = "Tried to execute query that substitutes no property in the end result";
        public const string NO_PROPS_SELECTED = "Executing query resulted in no selected properties";
        public const string NO_VALS_FOR_SUBSTITUTION = "Tried to add values for substituted property but got nothing !";

        public static string BuildMessage(string message, object arg)
        {
            try
            {
                return string.Format(message, arg);
            }
            catch (Exception e)
            {
                return message;
            }
        }
        // TODO: 
        public static string BuildMessage(string message, object[] args)
        {
            try
            {
                return string.Format(message, args);
            }
            catch (Exception e)
            {
                return message;
            }
        }
    }
}
