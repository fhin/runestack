﻿
namespace RuneDB
{
    public static class RuneRequestConsts
    {
        //TODO: Fix delay constants -> add 200ms ?
        public const int JAGEX_API_PAGE_DELAY = 3700;
        public const int JAGEX_API_ICON_DELAY = 2000;
        public const int RUNEBOX_ITEM_DELAY = 1000;
        public const int MAX_ITEMS_PER_PAGE = 12;
        public const int MAX_ITEMS_PER_PAGE_WEB = 20;
        public const int GENERAL_BACKOFF_TIME = 500;
    }

    public enum HttpRequestType
    {
        JAGEX_API_ICON,
        JAGEX_API_PAGE,
        RUNEBOX_API_ITEM,
        NONE
    }
}
