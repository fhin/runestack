﻿using RuneDB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace RuneDB
{
    public class RuneRequest
    {
        public string BaseUrl { get; set; }
        public Dictionary<string, RuneRequestParam> Parameters { get; set; }
        public bool AreParamsAdded { get; set; }

        public RuneRequest()
        {
            BaseUrl = "";
            Parameters = new Dictionary<string, RuneRequestParam>();
            AreParamsAdded = true;
        }

        public RuneRequest(string baseUrl) : this()
        {
            if (!string.IsNullOrWhiteSpace(baseUrl))
            {
                BaseUrl = baseUrl;
            }
        }

        public int AddHeader(string headerParam, Type headerParamType)
        {
            if (string.IsNullOrWhiteSpace(headerParam))
            {
                throw new ArgumentException(GenericErrorMessageFactory.EMPTY_REQUEST_HEADER_PARAM_NAME);
            }
            if (Parameters.ContainsKey(headerParam))
            {
                throw new ArgumentException(GenericErrorMessageFactory.DUPLICATE_REQUEST_HEADER_PARAM);
            }
            if (headerParamType == null)
            {
                throw new ArgumentException(GenericErrorMessageFactory.INVALID_HEADER_PARAM_TYPE);
            }
            Parameters.Add(headerParam, new RuneRequestParam(headerParamType));
            return 1;
        }

        public int AddHeaderValue(string headerParam, object value)
        {
            if (value == null) return -1;
            try
            {
                RuneRequestParam param = null;
                if (Parameters.ContainsKey(headerParam))
                {
                    Parameters.TryGetValue(headerParam, out param);
                    if (param != null)
                    {
                        param.Value = value;
                        Parameters[headerParam] = param;
                        return 1;
                    }
                    throw new ArgumentException(GenericErrorMessageFactory.NO_HEADER_FOR_NAME);
                }
                else
                {
                    throw new ArgumentException(GenericErrorMessageFactory.NO_HEADER_FOR_NAME);
                }
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public string BuildQueryString()
        {
            if (AreParamsAdded)
            {
                if (Parameters == null || Parameters.Count == 0) return BaseUrl;
                return BaseUrl + AddRequestParams();
            }
            else
            {
                if (Parameters == null || Parameters.Count == 0 || Parameters.Count > 1) throw new Exception("");
                string param = Parameters.Keys.First();
                try
                {
                    RuneRequestParam requestParam = null;
                    Parameters.TryGetValue(param, out requestParam);
                    return string.Format(BaseUrl, ConvertValueForUriHeader(requestParam.Type, requestParam.Value));
                }
                catch (Exception e)
                {
                    throw new Exception("Exception when building query string !" + e.Message);
                }
            }
        }

        private string AddRequestParams()
        {
            int cnt = 0;
            if (Parameters == null || Parameters.Count == 0) return "";

            string requestUrl = "";
            foreach (KeyValuePair<string, RuneRequestParam> kVPair in Parameters)
            {
                try
                {
                    RuneRequestParam param = kVPair.Value;
                    requestUrl += string.Format("{0}={1}", WebUtility.UrlEncode(kVPair.Key),
                        WebUtility.UrlEncode(ConvertValueForUriHeader(param.Type, param.Value)));
                    if (cnt < Parameters.Count - 1)
                    {
                        requestUrl += "&";
                    }
                }
                catch (Exception)
                {
                    throw new Exception(string.Format(GenericErrorMessageFactory.INVALID_HEADER_FORMAT, kVPair.Key));
                }
                cnt++;
            }
            return "?" + requestUrl;
        }

        private string ConvertValueForUriHeader(Type headerType, object value)
        {
            if (value == null) throw new ArgumentNullException();
            Type valueType = value.GetType();
            if (headerType != valueType)
            {
                throw new ArgumentException(string.Format(GenericErrorMessageFactory.HEADER_VALUE_TYPE_MISSMATCH,
                    new object[] { headerType.ToString(), valueType.ToString()}));
            }
            if (valueType.IsPrimitive || valueType == typeof(decimal) || valueType == typeof(string))
            {
                if (headerType == typeof(char))
                {
                    return char.ToString((char)value);
                }
                return value.ToString();
            }
            else
            {
                throw new ArgumentException(GenericErrorMessageFactory.NO_PRIMITIVE_HEADER_VALUE);
            }
        }
    }
}
