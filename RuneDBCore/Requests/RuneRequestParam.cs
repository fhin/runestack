﻿using System;

namespace RuneDB
{
    public class RuneRequestParam
    {
        private Type _type;
        public Type Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (value != null)
                {
                    _type = value;
                }
            }
        }
        public object Value { get; set; }

        public RuneRequestParam(Type type)
        {
            Type = type;
        }

        public RuneRequestParam(Type type, object value)
            : this(type)
        {
            Value = value;
        }
    }
}
