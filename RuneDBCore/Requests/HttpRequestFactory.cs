﻿public enum RequestType
{
    NumItemsForCategory,
    ItemsForCategory,
    ItemBasicDetails,
    ItemAdvDetails,
    ItemSprite,
    SingleItemPrice
}

namespace RuneDB
{
    public class HttpRequestFactory
    {
        private static HttpRequestFactory instance = null;
        private static readonly object padlock = new object();

        HttpRequestFactory()
        {
        }

        public static HttpRequestFactory GetInstance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new HttpRequestFactory();
                    }
                    return instance;
                }
            }
        }
        
        public RuneRequest GetRequestForType(RequestType type)
        {
            RuneRequest request = new RuneRequest();
            switch (type)
            {
                case RequestType.NumItemsForCategory:
                    request.BaseUrl = "http://services.runescape.com/m=itemdb_oldschool/api/catalogue/category.json";
                    request.AddHeader("category", typeof(int));
                    break;
                case RequestType.ItemsForCategory:
                    request.BaseUrl = "http://services.runescape.com/m=itemdb_oldschool/api/catalogue/items.json";
                    request.AddHeader("category", typeof(int));
                    request.AddHeader("alpha", typeof(char));
                    request.AddHeader("page", typeof(int));
                    break;
                case RequestType.ItemBasicDetails :
                case RequestType.SingleItemPrice:
                    request.BaseUrl = "http://services.runescape.com/m=itemdb_oldschool/api/catalogue/detail.json";
                    request.AddHeader("item", typeof(int));
                    break;
                case RequestType.ItemAdvDetails:
                    request.BaseUrl = "https://www.osrsbox.com/osrsbox-db/items-json/{0}.json";
                    request.AddHeader("item", typeof(int));
                    request.AreParamsAdded = false;
                    break;
                case RequestType.ItemSprite:
                    request.BaseUrl = "http://services.runescape.com/m=itemdb_oldschool/obj_big.gif";
                    request.AddHeader("id", typeof(int));
                    break;
                default:
                    request = null;
                    break;
            }
            return request;
        }
    }
}
