﻿using RuneDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RuneDB
{
    public static class RuneItemCategoryHelpers
    {
        private static Dictionary<int, string> categoryDict;
        private const string itemCategoryFilePath = @".\item_categories.txt";

        static RuneItemCategoryHelpers()
        {
            categoryDict = GetItemCategories();
        }

        private static Dictionary<int, string> GetItemCategories()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            if (!File.Exists(itemCategoryFilePath)) return dict;
            try
            {
                string[] lines = File.ReadAllLines(itemCategoryFilePath);
                foreach (var line in lines)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        Console.WriteLine("Skipping empty line for category definition !");
                        continue;
                    }
                    string[] lineData = line.Split('.');
                    int itemId = -1;
                    string itemName = "";
                    string dictItemName = "";
                    try
                    {
                        if (lineData.Length != 2)
                        {
                            // TODO: Logging 
                            Console.WriteLine("Item category data needs to be of format itemCatId.ItemCatName");
                            continue;
                        }
                        else
                        {
                            int.TryParse(lineData[0], out itemId);
                            itemName = lineData[1];
                        }
                        if (itemId == -1 || string.IsNullOrWhiteSpace(itemName))
                        {
                            Console.WriteLine("Item category data needs to be of format itemCatId.ItemCatName");
                            continue;
                        }
                        else if (dict.ContainsKey(itemId) || itemName.Equals(dict.TryGetValue(itemId, out dictItemName)))
                        {
                            Console.WriteLine("There already exists an entry with the given data, key: {0} - itemName: {1}", itemId, itemName);
                        }
                        else
                        {
                            dict.Add(itemId, itemName);
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Item category data needs to be of format itemCatId.ItemCatName");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading item categories " + e.Message);
            }
            return dict;
        }

        public static int GetIdForItemCategoryName(string name)
        {
            if (categoryDict.Count == 0 || string.IsNullOrWhiteSpace(name)) return -1;
            KeyValuePair<int, string> kVPair = categoryDict.FirstOrDefault(x => x.Value.Equals(name));
            if (kVPair.Equals(default(KeyValuePair<int, string>)))
            {
                return -1;
            }
            return kVPair.Key;
        }

        public static string GetCategoryNameForId(int id)
        {
            if (id < 0 || categoryDict.Count == 0) return "";
            string categoryName = "";
            categoryDict.TryGetValue(id, out categoryName);
            return categoryName;
        }

        public static IEnumerable<int> GetCategoryIds()
        {
            if (categoryDict == null || categoryDict.Count == 0) return new int[0];
            try
            {
                return categoryDict.Keys.ToArray<int>();
            }
            catch (Exception)
            {
                return new int[0];
            }
        }

        public static List<RuneCategory> GetCategoryList()
        {
            List<RuneCategory> categoryList = new List<RuneCategory>();
            try
            {
                foreach(KeyValuePair<int, string> kVPair in categoryDict)
                {
                    categoryList.Add(new RuneCategory(kVPair.Key, kVPair.Value));
                }
            }
            catch (Exception)
            {

            }
            return categoryList;
        }
    }
}
