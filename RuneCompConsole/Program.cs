﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RuneComp;
using RuneDB;
using RuneDB.Models;
using RuneDB.Services;
using System;
using System.Collections.Generic;
using System.IO;

namespace Runnable
{
    class Program
    {
        public static void Main(string[] args)
        {
            string inputString = "";
            IServiceProvider provider = InitServices();
            SqlBuilder sqlBuilder = null;
            RuneEntityService rES = new RuneEntityService(provider.GetRequiredService<IRuneDataService>(),
                               provider.GetRequiredService<IRuneDBService>());
            IRuneDBService dbService = provider.GetRequiredService<IRuneDBService>();
            /*
            RuneSqlQuery Rquery = new RuneSqlQuery();
            Rquery.SqlQuery = "select item_id, item_name from runeschema.items where item_name = 'Pirate bandana'";
            var test = dbService.ExecuteRuneSqlQuery(Rquery);
            */
            if (args.Length >= 1)
            {
                inputString = args[0];
            }
            if (args.Length == 2)
            {
                inputString += args[1];
            }

            while (true)
            {
                try
                {
                    string[] cmdData = inputString.Split(" ");
                    if (cmdData.Length == 1)
                    {
                        if (cmdData[0].Equals("-i"))
                        {
                            List<RuneItem> items = new List<RuneItem>();
                            List<RuneCategory> categories = rES.GetCategories();
                            foreach (var cat in categories)
                            {
                                items.AddRange(rES.GetItemsForCategory(cat));
                            }
                            dbService.StoreItems(items);
                        }
                        else if (cmdData[0].Equals("-p"))
                        {
                            IEnumerable<RunePriceHistoryEntry> entries = rES.GetPriceForItems();
                            IDictionary<int, RunePriceHistoryEntry> priceHistEntriesDict = new Dictionary<int, RunePriceHistoryEntry>();
                            List<int> itemIds = new List<int>();
                            if (entries != null)
                            {
                                foreach (var entry in entries)
                                {
                                    priceHistEntriesDict.Add(entry.ItemId, entry);
                                    itemIds.Add(entry.ItemId);
                                }
                                dbService.StorePriceHistory(priceHistEntriesDict, itemIds);
                            }
                        }
                    }
                    if (cmdData[0].Equals("-q"))
                    {
                        if (cmdData.Length > 1)
                        {
                            string query = inputString.Split("\"", 2)[1];
                            query = query.EndsWith("\"") ? query.Substring(0, query.Length - 1) : query;
                            if (sqlBuilder == null)
                            {
                                Dictionary<string, string> propDict = new Dictionary<string, string>
                                {
                                    { "item id", "item_id" },
                                    { "name", "item_name" },
                                    { "description", "description" },
                                    { "price", "curr_ge_price" },
                                    { "price change", "curr_price_change" },
                                    { "high alch value", "high_alch_val" },
                                    { "low alch value", "low_alch_val" },
                                    { "const price", "const_price" },
                                    { "buying limit", "buying_limit" },
                                    { "icon", "icon" }
                                };

                                Dictionary<string, string> tableMapping = new Dictionary<string, string>
                                {
                                    { "items", "items" },
                                    { "price regression models", "price_regression_models" }
                                };
                                sqlBuilder = new SqlBuilder("runeSchema", propDict, tableMapping, '%');
                            }
                            UserQueryParser parser = new UserQueryParser();
                            try
                            {
                                parser.ParseQuery(query);
                                if (!parser.IsValidQuery)
                                {
                                    foreach (var error in parser.Errors)
                                    {
                                        Console.WriteLine(error.ToString());
                                    }
                                    throw new Exception("Error parsing query !");
                                }
                                RuneSqlExecutionPlan plan = sqlBuilder.BuildExecutionPlanFromQuery(parser.UserQuery);
                                foreach (var runeQuery in plan.Queries)
                                {
                                    runeQuery.WriteToConsole();
                                }
                                (var propDict, var results) = dbService.RunExecutionPlan(plan);
                                Console.WriteLine("Results: \n");
                                string[] resultAsString = new string[propDict.Count];
                               
                                foreach (KeyValuePair<string, int> kvPair in propDict)
                                {
                                    resultAsString[propDict[kvPair.Key]] = kvPair.Key;
                                }
                                string columnHeaderRow = string.Join(" | ", resultAsString);
                                Console.WriteLine(columnHeaderRow);
                                Console.WriteLine(new string('=', columnHeaderRow.Length));
                                foreach(var result in results)
                                {
                                    foreach (KeyValuePair<string, int> kvPair in propDict)
                                    {
                                        resultAsString[propDict[kvPair.Key]] = result[propDict[kvPair.Key]].ToString();
                                    }
                                    Console.WriteLine(string.Join(" | ", resultAsString));
                                }
                                // TODO: Update to current implementation of execution plan
                                /*
                                RuneSqlQuery rQuery = sqlBuilder.BuildSqlStatmentFromQuery(parser.UserQuery);
                                Console.WriteLine("Generated SQL query: ");
                                Console.WriteLine(rQuery.GetPlainQuery());
                                Console.WriteLine("Query params: ");
                                Console.WriteLine("==============");
                                foreach (var param in rQuery.QueryParams)
                                {
                                    Console.WriteLine(param.ToString());
                                }
                                */
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Error parsing query !");
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Required format for this command: -q \"<query>\"");
                        }
                    }
                    else if (cmdData[0].Equals("-u"))
                    {
                        try
                        {
                           (IList<int> itemIds, IList<byte[]> itemSprites) = rES.FetchAllItemSprites().GetAwaiter().GetResult();
                            dbService.UpdateImageSprites(itemIds, itemSprites);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error updating item sprites for reason " + e.Message);
                            Console.WriteLine("Stacktrace: " + e.StackTrace);
                        }
                    }
                    else if (cmdData[0].Equals("-r"))
                    {
                        if (cmdData.Length != 3)
                        {
                            Console.WriteLine("Required format for this command: -r <degree> <numElems>");
                        }
                        else
                        {
                            int degree = Convert.ToInt32(cmdData[1]);
                            int numElems = Convert.ToInt32(cmdData[2]);
                            dbService.StoreRegressionModels(rES.GetPriceRegressionModels(degree, numElems), PredictableProperty.Curr_Ge_Price);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error parsing command for reason " + e.Message);
                    Console.WriteLine("Stracktrace " + e.StackTrace);
                }

                Console.WriteLine();
                Console.WriteLine("Options available (Format: -LaunchParam <Option>) \n ===============================================");
                Console.WriteLine("Fetch all item data: -i ");
                Console.WriteLine("Fetch all item prices: -p");
                Console.WriteLine("Convert query to sql: -q <query>");
                Console.WriteLine("Update image sprites: -u");
                Console.WriteLine("Create and store regression models: -r <degree> <numElems>");
                Console.Write("Enter an option (TAB + ENTER to exit): ");
                inputString = Console.ReadLine();
                if (inputString.Equals("\t"))
                {
                    return;
                }
                Console.WriteLine();
            }


            //IEnumerable<RunePriceHistoryEntry> histEntries = rES.FetchAllItemsGEPrices().GetAwaiter().GetResult();
            //int x = 0;
            /*
            List<RuneCategory> cats = rES.GetCategories();
            List<RuneItem> items = new List<RuneItem>();
            */

            //List<RuneCategory> cats = rES.GetCategories();
            //List<RuneItem> items = new List<RuneItem>();

            /*
            RuneCategory cat = new RuneCategory(1, "Default");
            RuneCategoryEntry entry7 = new RuneCategoryEntry();
            entry7.StartingLetter = 'f';
            entry7.NumItems = 79;
            RuneCategoryEntry entry8 = new RuneCategoryEntry();
            entry8.StartingLetter = 'u';
            entry8.NumItems = 50;
            
            cat.LetterEntries.Add('f', entry7);
            cat.LetterEntries.Add('u', entry8);

            items.AddRange(rES.GetItemsForCategory(cat));
            */

            /*
            foreach (var cat in cats)
            {
                items.AddRange(rES.GetItemsForCategory(cat));
            }
            */
            /*
            var categories = rES.GetCategories();
            var items = new List<RuneItem>();
            foreach(var cat in categories)
            {
                items.AddRange(rES.GetItemsForCategory(cat));
            }
            dbService.StoreItems(items);
            */

            /*
            List<string> words = new List<string>()
            {
                "Vampyre dust",
                "Varrock teleport",
                "Veg ball",
                "Vegetable batta",
                "Verac's armour set",
                "Verac's brassard",
                "Verac's brassard 0",
                "Verac's flail",
                "Verac's helm",
                "Abyssal bludgeon",
                "Abyssal dagger",
                "Acorn"
            };
            words.Add("Rune");
            words.Add("Rym");
             words.Add("Ruby");
            words.Add("Staff");
            words.Add("");
            TrieTree tree = new TrieTree();
            tree.AddWords(words);
            List<(string, int)> items = new List<(string, int)>();
            items = tree.GetItemPrefixes();
            int x = 0;
            /*
            /*
            Dictionary<string, string> propDict = new Dictionary<string, string>();
            propDict.Add("name", "name");
            propDict.Add("description", "description");
            propDict.Add("price", "current_ge_price");
            propDict.Add("price change", "current_price_change");
            propDict.Add("high alch val", "high_alch_val");
            propDict.Add("low alch val", "low_alch_val");
            propDict.Add("const price", "const_price");
            propDict.Add("buying limit", "buying_limit");

            Dictionary<string, string>  tableMapping = new Dictionary<string, string>();
            tableMapping.Add("items", "items");
            SqlBuilder sqlBuilder = new SqlBuilder("runeSchema", propDict, tableMapping);

            while (true)
            {

                Console.Write("Enter your query: ");
                string query = Console.ReadLine();
                UserQueryParser parser = new UserQueryParser(query);
                try
                {
                    parser.ParseQuery();
                    RuneSqlQuery rQuery = sqlBuilder.BuildSqlStatmentFromQuery(parser.UserQuery);
                    Console.WriteLine("Generated SQL query: ");
                    Console.WriteLine(rQuery.GetPlainQuery());
                    Console.WriteLine("Query params: ");
                    Console.WriteLine("==============");
                    foreach (var param in rQuery.QueryParams)
                    {
                        Console.WriteLine(param.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error parsing query !");
                    Console.WriteLine(e.Message);
                }

                Console.WriteLine("\n Press any key to resume (TAB to exit)...");
                ConsoleKeyInfo keyInfo = Console.ReadKey(false);
                if (keyInfo.Key == ConsoleKey.Tab)
                {
                    break;
                }
            }
            */
            /*
            string query = "show name and price of items with a high alch value = 1000 and a price <= 100 and a price > 20";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(query);
            writer.Flush();
            stream.Position = 0;
            //Buffer b = new Buffer(stream, true);
            Scanner s = new Scanner(stream);
            Parser p = new Parser(s);
            p.Parse();
            Query q = p.query;
            */

            /*
            if (args.Length > 0)
            {
                if (!File.Exists(args[0]))
                {
                    Console.WriteLine("File " + args[0] + " does not exist");
                    Console.WriteLine("\n Press any key to stop...");
                    Console.ReadKey();
                    return;
                }

                int dumpTreeIdx = FindArg("-t", args);
                string outputFilePath = "";
                if (dumpTreeIdx != -1 && dumpTreeIdx < args.Length)
                {
                    if (dumpTreeIdx + 1 < args.Length)
                    {
                        outputFilePath = args[dumpTreeIdx + 1] != null ? args[dumpTreeIdx + 1] : "";
                    }
                    if (outputFilePath.Length > 0)
                    {
                        try
                        {
                            using (FileStream fs = File.Open(outputFilePath, FileMode.Open))
                            {
                                fs.SetLength(0); // Clear previous file contents
                            }
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("Error truncating outputfile for the reason below !");
                            Console.WriteLine(e.Message);
                        }
                    }
                    for(var i = 0; i < dumpTreeIdx; i++)
                    {
                        try
                        {   
                            ParseInput(args[i], outputFilePath);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error parsing input in file" + args[i]);
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("-- No source file/s specified");
                }
            }
            */
        }

        /*
        private static int FindArg(string argName, string[] argArr)
        {
            if (string.IsNullOrEmpty(argName) || argArr.Length == 0) return -1;
            for (int i = 0; i < argArr.Length; i++)
            {
                if (argName.Equals(argArr[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        private static void ParseInput(string inputFilePath, string outputFilePath)
        {
            if (string.IsNullOrEmpty(inputFilePath)) return;
            if (string.IsNullOrEmpty(outputFilePath))
            {
                Console.WriteLine("Warning no output file specified !");
                return;
            }

            Scanner scanner = new Scanner(inputFilePath);
            Parser parser = new Parser(scanner);
            parser.Parse();
            if (parser.errors.count == 0)
            {
                if (File.Exists(outputFilePath))
                {
                    using (FileStream fs = File.Open(outputFilePath, FileMode.Append))
                    {
                        string treeDump = null;
                        try
                        {
                            treeDump = Utils.Dump(parser.query);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error creating tree structure for query !\n");
                            Console.WriteLine(e.Message);
                        }
                        if (treeDump != null)
                        {
                            byte[] info = new UTF8Encoding(true).GetBytes(treeDump);
                            fs.Write(info, 0, info.Length);
                            Console.WriteLine("Dumped tree structure into file: " + outputFilePath);
                        }
                    }
                }
                else
                {
                    using (FileStream fs = File.Create(outputFilePath))
                    {
                        string treeDump = null;
                        try
                        {
                            treeDump = Utils.Dump(parser.query);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error creating tree structure for query !\n");
                            Console.WriteLine(e.Message);
                        }
                        if (treeDump != null)
                        {
                            byte[] info = new UTF8Encoding(true).GetBytes(treeDump);
                            fs.Write(info, 0, info.Length);
                            Console.WriteLine("Dumped tree structure into file: " + outputFilePath);
                        }
                    }
                }
            }
        }
        */

        private static IServiceProvider InitServices()
        {
            IServiceProvider provider = null;
            try
            {
                var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json");

                IConfiguration config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .Build();

                string s = config["connectionString"];

                IServiceCollection serviceCollection = new ServiceCollection();
                ConfigureServices(serviceCollection, config);
                provider = serviceCollection.BuildServiceProvider();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error setting up initializing services for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
            }
            return provider;
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration config)
        {
            HttpRequestFactory requestFactory = HttpRequestFactory.GetInstance;
            services.AddSingleton(requestFactory);

            //services.AddSingleton<IRuneDataService, RuneHttpDataService>();
            services.AddSingleton<IRuneDataService, RuneHttpDataService>();
            services.AddHttpClient<IRuneDataService, RuneHttpDataService>();

            services.AddSingleton<IRuneContextFactory>(x => new RuneContextFactory(
                new DbContextOptionsBuilder<RuneDBContext>().UseNpgsql(config["connectionString"])));

            services.AddTransient<IRuneDBService, RuneDbService>();
            //services.AddScoped<IRuneDBService, RuneDbService>();

            /*
            services.AddDbContext<IRuneDB, RuneDBContext>(options
                => options.UseNpgsql(config["connectionString"]),
                ServiceLifetime.Transient);
                */
        }
    }
}
