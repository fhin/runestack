﻿const uri = "https://localhost:44351/api/query";

function sendQuery() {
    $.ajax({
        type: "GET",
        url: uri + "/queryRes/" + $("#user-query").val(),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error");
        },
        success: function (result) {
            //alert("Got response");
            const tHeader = $("#queryResTHead");
            const tBody = $("#queryResTBody");
            tHeader.empty();
            tBody.empty();
            if (result.queryResultType == 1) {
                // handle errors
            }
            else if (result.queryResultType == 0) {
                const tHeaderRow = $("<tr></tr>");
                $.each(result.columns, function (idx, columnName) {
                    tHeaderRow.append($("<td></td>").text(columnName));
                    tHeader.append(tHeaderRow);
                });
                $.each(result.resultTable, function (idx, resultRow) {
                    const tResultRow = $("<tr></tr>");
                    $.each(resultRow, function (idx, resultRowEntry) {
                        if (idx == 0) {
                            console.log(resultRowEntry);
                            var img = new Image();
                            img.src = "data:image/png;base64," + resultRowEntry;
                            img.className = "img-responsive img-rounded";
                            tResultRow.append($("<td></td>")).append(img);
                        }
                        else {
                            tResultRow.append($("<td></td>").text(resultRowEntry));
                        }
                    });
                    tBody.append(tResultRow);
                });
                $("#queryResultTable").css({display: "inline"});
            }
            else {
                alert("Error parsing query response !");
            }
        },
        complete: function (jqXHR, result) {
        }
    });
}