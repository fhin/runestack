﻿using System;
using System.Diagnostics;

namespace RuneStackCompGen
{
    class Program
    {
        static void Main(string[] args)
        {
            LaunchCommandLineApp();
        }

        static void LaunchCommandLineApp()
        {
            const string frameFilesPath = @"cocoFrames";
            const string atgFilePath = @"utils\RuneStack.ATG";
            const string exeFilePath = @"utils\Coco.exe";
            const string outputFilePath = @"H:\School\Bakk\Repo-Folder\RuneComp";

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = false,
                FileName = exeFilePath,
                WindowStyle = ProcessWindowStyle.Normal,
                Arguments = atgFilePath + " -frames " + frameFilesPath + " -o " + outputFilePath
            };

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit(15000);
                }
                // TODO: Logging after exit if there where errors
            }
            catch (Exception e)
            {
                // Log error.
                Console.WriteLine(e.Message);
                return;
            }
        }
    }
}
