﻿using System;
using System.Collections.Generic;

namespace RuneComp
{
    public class Property
    {
        public string PropertyName { get; set; }
        public bool IsNumeric { get; set; }
        public Prediction Prediction { get; set; }

        public Property()
        {
            Prediction = null;
            IsNumeric = true;
            PropertyName = "";
        }

        public override bool Equals(object obj)
        {
            var property = obj as Property;
            return property != null &&
                   PropertyName == property.PropertyName &&
                   IsNumeric == property.IsNumeric &&
                   EqualityComparer<Prediction>.Default.Equals(Prediction, property.Prediction);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PropertyName, IsNumeric, Prediction);
        }

        public override string ToString()
        {
            string resultString = string.IsNullOrWhiteSpace(PropertyName) ? "" : PropertyName;
            if (Prediction != null)
            {
                resultString += Prediction.ToString();
            }
            return resultString;
        }
    }
}
