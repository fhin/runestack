﻿using System.Collections.Generic;

namespace RuneComp
{
    public class Query
    {
        public QueryOp QueryOp { get; set; }
        public List<Property> Props { get; set; }
        public string PreSelector { get; set; }
        public List<Filter> Filters { get; set; }

        public Query()
        {
            QueryOp = QueryOp.None;
            Filters = new List<Filter>();
            Props = new List<Property>();
        }

        public Query(QueryOp op) : this()
        {
            QueryOp = op;
        }

        public bool SelectsSingleItem()
        {
            if (PreSelector != null && PreSelector.Length > 0)
            {
                if(PreSelector.IndexOf(':') == -1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
