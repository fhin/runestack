﻿namespace RuneComp
{
    public class Filter
    {
        public Property Property { get; set; }
        public Condition Condition { get; set; }
        public FilterConcatinationOp FilterConcatOp { get; set; }

        public Filter()
        {
            FilterConcatOp = FilterConcatinationOp.None;
        }
    }
}
