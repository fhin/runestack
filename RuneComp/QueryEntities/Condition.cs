﻿namespace RuneComp
{
    public class Condition
    {
        private string _sVal;
        private int _iVal;
        private double _dVal;

        public Condition()
        {
            CondType = ConditionValType.None;
            Op = Operation.None;
            SVal = "";
        }

        public string SVal { get { return _sVal; } set { _sVal = value != null ? value : _sVal; } }
        public int IVal { get { return _iVal; } set { _iVal = value < 0 ? _iVal : value; } }
        public double DVal { get { return _dVal; } set { _dVal = value < 0 ? _dVal : value; } }
        public ConditionValType CondType { get; set; }
        public Operation Op { get; set; }

        public bool IsOpNumeric()
        {
            bool isNumeric = true;
            switch (Op)
            {
                case Operation.None:
                case Operation.Like:
                case Operation.Starting_With:
                case Operation.Ending_With:
                    isNumeric = false;
                    break;
                default:
                    isNumeric = true;
                    break;
            }
            return isNumeric;
        }
    }
}
