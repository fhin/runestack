﻿using System;

namespace RuneComp
{
    public class Prediction
    {
        private int _tValue;
        private DateTime _future_date;

        public PredictionTimeUnit TUnit { get; set; }
        public int TValue { get { return _tValue; } set { _tValue = value >= 0 ? value : _tValue; } }
        public DateTime FutureDate
        {
            get { return _future_date; }
            set
            {
                if (value != null)
                {
                    if (DateTime.Compare(value, _future_date) >= 0)
                    {
                        _future_date = value;
                    }
                }
            }
        }
        public PredictionTarget PredTarget { get; set; }

        public Prediction()
        {
            PredTarget = PredictionTarget.PerItem;
            TUnit = PredictionTimeUnit.None;
            _tValue = 0;
            FutureDate = DateTime.MinValue;
        }

        public override bool Equals(object obj)
        {
            var prediction = obj as Prediction;
            return prediction != null &&
                   TUnit == prediction.TUnit &&
                   TValue == prediction.TValue &&
                   FutureDate == prediction.FutureDate &&
                   PredTarget == prediction.PredTarget;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TUnit, TValue, FutureDate, PredTarget);
        }

        public override string ToString()
        {
            string resultString = "";
            if (FutureDate.CompareTo(DateTime.MinValue) == 0)
            {
                resultString = "_in_" + TValue.ToString();
                switch (TUnit)
                {
                    case PredictionTimeUnit.Day:
                        resultString += "d";
                        break;
                    case PredictionTimeUnit.Hour:
                        resultString += "h";
                        break;
                    case PredictionTimeUnit.Month:
                        resultString += "m";
                        break;
                    default:
                        throw new Exception("Cannot add prediction with no timeUnit !");
                }
                switch (PredTarget)
                {
                    case PredictionTarget.PerItem:
                        break;
                    case PredictionTarget.Total:
                        break;
                    default:
                        throw new Exception("Prediction needs to define a quantity for the result !");
                }
            }
            else
            {
                // Date format:    2008-10-01T17:04:32
                // Further infos at https://docs.microsoft.com/en-us/dotnet/api/system.datetime.tostring?view=netframework-4.8
                resultString += "_on_" + FutureDate.ToString("s");
            }
            return resultString;
        }

        public DateTime ConverToDateTime()
        {
            DateTime resultDate = DateTime.Now;
            if (FutureDate.CompareTo(DateTime.MinValue) == 0)
            {
                switch (TUnit)
                {
                    case PredictionTimeUnit.Day:
                        resultDate = resultDate.AddDays(TValue);
                        break;
                    case PredictionTimeUnit.Hour:
                        resultDate = resultDate.AddHours(TValue);
                        break;
                    case PredictionTimeUnit.Month:
                        resultDate = resultDate.AddMonths(TValue);
                        break;
                    default:
                        throw new Exception("Cannot convert prediction to date time !");
                }
            }
            else
            {
                resultDate = FutureDate;
            }
            return resultDate;
        }
    }
}
