﻿namespace RuneComp
{
    public enum Operation
    {
        Greater,
        Greater_Eq,
        Smaller,
        Smaller_Eq,
        Equal,
        Like,
        Starting_With,
        Ending_With,
        None
    }

    public enum PredictionTimeUnit
    {
        Hour,
        Day,
        Month,
        None
    }

    public enum PredictionTarget
    {
        PerItem,
        Total
    }

    public enum ConditionValType
    {
        Integer,
        Double,
        String,
        None
    }

    public enum QueryOp
    {
        show,
        compare,
        None
    }

    public enum FilterConcatinationOp
    {
        And,
        Or, 
        None
    }

    public class Enums
    {
        public static string GetOperationAsStringSymbol(Operation op)
        {
            string operationSymbol = "";
            switch (op)
            {
                case Operation.Equal:
                    operationSymbol = "=";
                    break;
                case Operation.Smaller:
                    operationSymbol = "<";
                    break;
                case Operation.Smaller_Eq:
                    operationSymbol = "<=";
                    break;
                case Operation.Greater:
                    operationSymbol = ">";
                    break;
                case Operation.Greater_Eq:
                    operationSymbol = ">=";
                    break;
                case Operation.Starting_With:
                case Operation.Ending_With:
                case Operation.Like:
                    operationSymbol = Operation.Like.ToString().ToLower();
                    break;
                default:
                    throw new System.ArgumentException("No mapping for given operation !");
            }
            return operationSymbol;
        }

        public static string GetFilterConcatOpAsAsStringSymbol(FilterConcatinationOp op)
        {
            switch (op)
            {
                case FilterConcatinationOp.And:
                    return "and";
                case FilterConcatinationOp.Or:
                    return "or";
                default:
                    throw new System.ArgumentException("No mapping for FilterConcatinationOp " + op.ToString());
            }
        }

        public static bool IsOperationNumeric(Operation op)
        {
            bool isNumeric = false;
            switch (op)
            {
                case Operation.Equal:
                case Operation.Smaller:
                case Operation.Smaller_Eq:
                case Operation.Greater:
                case Operation.Greater_Eq:
                    isNumeric = true;
                    break;
                default:
                    isNumeric = false;
                    break;
            }
            return isNumeric;
        }
    }
}
