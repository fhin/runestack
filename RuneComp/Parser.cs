using System.Globalization;
using System.Collections.Generic;
using RuneComp;



using System;



public class Parser {
	public const int _EOF = 0;
	public const int _string = 1;
	public const int _integer = 2;
	public const int _decimal = 3;
	public const int maxT = 45;

	const bool _T = true;
	const bool _x = false;
	const int minErrDist = 2;
	
	public Scanner scanner;
	public Errors  errors;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;

public Query query;
public char wildcardCharacter = '*';



	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (string msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) { ++errDist; break; }

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	bool StartOf (int s) {
		return set[s, la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}


	bool WeakSeparator(int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) {Get(); return true;}
		else if (StartOf(repFol)) {return false;}
		else {
			SynErr(n);
			while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}

	
	void RuneStack() {
		Query();
	}

	void Query() {
		query = new Query(); QueryOp op = RuneComp.QueryOp.None; string preSelector = "";
		List<Property> properties = new List<Property>();
		List<Filter> filters = new List<Filter>();
		
		QueryOp(out op);
		query.QueryOp = op; 
		if (StartOf(1)) {
			PropList(out properties);
			Expect(4);
		}
		query.Props = properties; 
		GeneralSelector(out preSelector);
		query.PreSelector = preSelector; 
		if (la.kind == 8) {
			FilterList(out filters);
		}
		query.Filters = filters; 
	}

	void QueryOp(out QueryOp queryOp) {
		queryOp = RuneComp.QueryOp.None; 
		Expect(5);
		try {
		if (!Enum.TryParse(t.val, true, out queryOp)){
		SemErr("Invalid query operation");
		queryOp = RuneComp.QueryOp.None;
		}
		} catch (ArgumentException e){
		// TODO: Logging
		SemErr("Invalid query operation");
		}
		
	}

	void PropList(out List<Property> props ) {
		props = new List<Property>(); Property currProp = new Property(); 
		Prediction currPred = null; 
		ItemProp(out currProp);
		if (StartOf(2)) {
			DateExpr(out currPred);
		}
		if (currProp.Prediction == null){
		currProp.Prediction = currPred;
		}
		props.Add(currProp); 
		while (la.kind == 6 || la.kind == 7) {
			if (la.kind == 6) {
				Get();
			} else {
				Get();
			}
			currProp = new Property(); currPred = null; 
			ItemProp(out currProp);
			if (StartOf(2)) {
				DateExpr(out currPred);
			}
			currProp.Prediction = currPred; 
			props.Add(currProp); 
		}
	}

	void GeneralSelector(out string val) {
		val = ""; 
		if (la.kind == 29) {
			Get();
		} else if (la.kind == 1 || la.kind == 30) {
			if (la.kind == 30) {
				Get();
			}
			ItemName(out val);
		} else SynErr(46);
	}

	void FilterList(out List<Filter> filters ) {
		filters = new List<Filter>(); Filter currFilter = new Filter();
		FilterConcatinationOp currFilterConcatOp = FilterConcatinationOp.None; 
		Expect(8);
		FilterExpr(out currFilter);
		filters.Add(currFilter); 
		while (StartOf(3)) {
			FilterConcatOp(out currFilterConcatOp);
			currFilter.FilterConcatOp = currFilterConcatOp; 
			currFilter = new Filter(); 
			FilterExpr(out currFilter);
			filters.Add(currFilter); 
		}
	}

	void ItemProp(out Property itemProp) {
		itemProp = new Property(); 
		if (la.kind == 21) {
			Get();
			itemProp.PropertyName = t.val; 
			itemProp.IsNumeric = false; 
		} else if (la.kind == 22) {
			Get();
			itemProp.PropertyName = t.val; 
			itemProp.IsNumeric = false; 
		} else if (la.kind == 23) {
			Get();
			itemProp.PropertyName = t.val; 
			if (la.kind == 24) {
				Get();
				itemProp.PropertyName += " " + t.val; 
			}
		} else if (la.kind == 25 || la.kind == 26) {
			if (la.kind == 25) {
				Get();
			} else {
				Get();
			}
			itemProp.PropertyName = t.val; 
			Expect(27);
			itemProp.PropertyName += " " + t.val; 
			Expect(28);
			itemProp.PropertyName += " " + t.val; 
		} else SynErr(47);
	}

	void DateExpr(out Prediction prediction) {
		prediction = new Prediction(); DateTime predDate = DateTime.Now; 
		if (la.kind == 32) {
			Get();
		} else if (la.kind == 33) {
			Get();
		} else if (la.kind == 34) {
			Get();
			Date(out predDate);
			try {
			prediction.FutureDate = predDate;
			}
			catch (Exception e){
			SemErr("Date format needs to be: year.month.dayThour[24 hour format]");
			} 
		} else if (la.kind == 35) {
			Get();
			PredDate(ref prediction);
		} else SynErr(48);
	}

	void FilterExpr(out Filter filter) {
		ItemPropRel(out filter);
	}

	void FilterConcatOp(out FilterConcatinationOp filterConcatOp) {
		filterConcatOp = FilterConcatinationOp.None; 
		if (la.kind == 7 || la.kind == 9) {
			if (la.kind == 7) {
				Get();
			} else {
				Get();
			}
			filterConcatOp = FilterConcatinationOp.And; 
		} else if (la.kind == 10 || la.kind == 11) {
			if (la.kind == 10) {
				Get();
			} else {
				Get();
			}
			filterConcatOp = FilterConcatinationOp.Or; 
		} else SynErr(49);
	}

	void ItemPropRel(out Filter filter) {
		filter = new Filter(); Property itemProp = null; 
		Condition cond = new Condition(); bool isNumericOp = true; 
		Operation condOp = Operation.None; 
		
		if (la.kind == 31) {
			Get();
		}
		ItemProp(out itemProp);
		if (itemProp.PropertyName.Equals("name") && !string.IsNullOrWhiteSpace(query.PreSelector)){
		SemErr("Cannot define a filter for item name when there is already a pre selector defined !");
		}
		filter.Property = itemProp; 
		RelOp(ref condOp);
		cond.Op = condOp; 
		if (condOp != Operation.Equal) {
		isNumericOp = cond.IsOpNumeric();
		if (isNumericOp && !itemProp.IsNumeric){
		// TODO: Logging data type mismatch
		SemErr("Numeric relation operand only valid for numeric properties");
		}
		else if (!isNumericOp && itemProp.IsNumeric){
		// TODO: Logging data type mismatch
		SemErr("String relation operand only valid for string properties");
		}
		}
		
		Value(ref cond);
		if (condOp != Operation.Equal) {
		if (isNumericOp && cond.CondType == ConditionValType.String){
		// TODO: Logging data type mismatch
		SemErr("Numeric relation operand only valid for numeric properties");
		}
		else if (!isNumericOp && cond.CondType != ConditionValType.String){
		// TODO: Logging data type mismatch
		SemErr("String relation operand only valid for string properties");
		}
		}
		filter.Condition = cond;
		
	}

	void RelOp(ref Operation op) {
		if (StartOf(4)) {
			SymbolicRelOp(ref op);
		} else if (StartOf(5)) {
			StringRelOp(ref op);
		} else SynErr(50);
	}

	void SymbolicRelOp(ref Operation op) {
		if (la.kind == 12) {
			Get();
			op = Operation.Equal; 
		} else if (la.kind == 13) {
			Get();
			op = Operation.Smaller; 
		} else if (la.kind == 14) {
			Get();
			op = Operation.Greater; 
		} else if (la.kind == 15) {
			Get();
			op = Operation.Smaller_Eq; 
		} else if (la.kind == 16) {
			Get();
			op = Operation.Greater_Eq; 
		} else SynErr(51);
	}

	void StringRelOp(ref Operation op) {
		if (la.kind == 17) {
			Get();
			op = Operation.Like; 
		} else if (la.kind == 18) {
			Get();
			Expect(8);
			op = Operation.Starting_With; 
		} else if (la.kind == 19) {
			Get();
			Expect(8);
			op = Operation.Starting_With; 
		} else if (la.kind == 20) {
			Get();
			Expect(8);
			op = Operation.Ending_With; 
		} else SynErr(52);
	}

	void ItemName(out string val) {
		Expect(1);
		if (string.IsNullOrWhiteSpace(t.val)){
		SemErr("Item name cannot be empty");
		t.val = "";
		}
		if (t.val.Contains(' ')){
		SemErr("Spaces in item name need to be replaced with _ character");
		t.val = "";
		}
		val = t.val.Replace('_', ' ');
		val = val.Replace("\"","");
		if (val.IndexOf(wildcardCharacter) != -1){
		SemErr("No wildcards allowed in itemName pre-selector");
		val = "";
		}
		
	}

	void Value(ref Condition cond) {
		if (la.kind == 2) {
			Get();
			cond.CondType = ConditionValType.Integer;
			int defaultIVal = 0;
			if (!int.TryParse(t.val, out defaultIVal)){
			// TODO: Logging
			SemErr("Cannot convert " + t.val + " to its number representation");
			}
			else {
			cond.IVal = defaultIVal;
			}
			
		} else if (la.kind == 3) {
			Get();
			cond.CondType = ConditionValType.Double;
			double defaultDVal = 0.0;
			CultureInfo numberCulturInfo = CultureInfo.CreateSpecificCulture("en-US");
			
			if (!double.TryParse(t.val, 
			(NumberStyles.AllowDecimalPoint 
			| NumberStyles.AllowThousands 
			| NumberStyles.Integer),
			numberCulturInfo, out defaultDVal)){
			// TODO: Logging
			SemErr("Cannot convert " + t.val + " to its decimal representation");
			} 
			else {
			cond.DVal = defaultDVal;
			}
			
		} else if (la.kind == 1) {
			Get();
			cond.CondType = ConditionValType.String;
			cond.SVal = t.val.Replace("\"","");
			if (string.IsNullOrWhiteSpace(cond.SVal)){
			SemErr("String value cannot be empty");
			}
			int startWildcardIdx = cond.SVal.IndexOf(wildcardCharacter);
			if (startWildcardIdx != -1){
			if ((cond.Op == Operation.Ending_With && startWildcardIdx != 0)){
			SemErr("Beginning wild card " + wildcardCharacter.ToString() + " only allowed at start of string for ENDING WITH operation");	
			}
			}
			int endWildcardIdx = cond.SVal.LastIndexOf(wildcardCharacter);
			if (endWildcardIdx != -1){
			if ((cond.Op == Operation.Starting_With && endWildcardIdx != cond.SVal.Length -1)){
			SemErr("Ending wild card " + wildcardCharacter.ToString() + " only allowed at end of string for STARTING / BEGINNING WITH operation");	
			}
			if (startWildcardIdx != -1){
			if (endWildcardIdx == startWildcardIdx){
			/*
			valid patterns: 
			<text>* for STARTING / BEGINNING op
			*<text> for ENDING op
			text with one wildcard for LIKE op
			*/
			return;
			}
			else {
			if (cond.Op != Operation.Like){
			SemErr("STARTING / BEGINNING / ENDING operations can contain at most one wildcard");
			}
			int numSymToCheck = endWildcardIdx - 1 - (startWildcardIdx + 1);
			int otherWildcardIdx = cond.SVal.IndexOf(wildcardCharacter, startWildcardIdx+1, numSymToCheck);
			if (startWildcardIdx == 0 && endWildcardIdx == cond.SVal.Length -1 && otherWildcardIdx == -1){
			return; // Valid pattern *<text>* for LIKE operation
			}
			else {
			SemErr("Like operation allows only the pattern *<text>* and <text>*<text>");
			}
			}
			}
			} 
		} else SynErr(53);
	}

	void Date(out DateTime date) {
		date = DateTime.Now; 
		int day = date.Day;
		int month = date.Month;
		int year = date.Year;
		int hour = date.Hour;
		
		Expect(2);
		day = Convert.ToInt32(t.val); 
		Expect(36);
		Expect(2);
		month = Convert.ToInt32(t.val); 
		Expect(36);
		Expect(2);
		year = Convert.ToInt32(t.val); 
		Expect(37);
		Expect(2);
		hour = Convert.ToInt32(t.val); 
		date = new DateTime(year, month, day, hour, 0, 0);
		
		if (la.kind == 38) {
			Get();
		}
	}

	void PredDate(ref Prediction pred) {
		int num = 0; PredictionTimeUnit timeUnit = PredictionTimeUnit.None; 
		Expect(2);
		if (int.TryParse(t.val, out num)){
		pred.TValue = num;
		}
		else {
		// TODO: Logging
		}
		
		TimeUnit(ref timeUnit);
		pred.TUnit = timeUnit; 
	}

	void TimeUnit(ref PredictionTimeUnit tUnit) {
		if (la.kind == 38) {
			Get();
			if (la.kind == 39) {
				Get();
			}
			tUnit = PredictionTimeUnit.Hour; 
		} else if (la.kind == 40) {
			Get();
			if (la.kind == 41) {
				Get();
				if (la.kind == 42) {
					Get();
				}
			}
			tUnit = PredictionTimeUnit.Day; 
		} else if (la.kind == 43) {
			Get();
			if (la.kind == 44) {
				Get();
			}
			tUnit = PredictionTimeUnit.Month; 
		} else SynErr(54);
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		RuneStack();
		Expect(0);

	}
	
	static readonly bool[,] set = {
		{_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_T, _x,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_T, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_T, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x}

	};
} // end Parser


public class Errors {
	public int count = 0;                                    // number of errors detected
	public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
	public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

	public virtual void SynErr (int line, int col, int n) {
		string s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "string expected"; break;
			case 2: s = "integer expected"; break;
			case 3: s = "decimal expected"; break;
			case 4: s = "\"of\" expected"; break;
			case 5: s = "\"show\" expected"; break;
			case 6: s = "\",\" expected"; break;
			case 7: s = "\"and\" expected"; break;
			case 8: s = "\"with\" expected"; break;
			case 9: s = "\"&\" expected"; break;
			case 10: s = "\"or\" expected"; break;
			case 11: s = "\"|\" expected"; break;
			case 12: s = "\"=\" expected"; break;
			case 13: s = "\"<\" expected"; break;
			case 14: s = "\">\" expected"; break;
			case 15: s = "\"<=\" expected"; break;
			case 16: s = "\">=\" expected"; break;
			case 17: s = "\"like\" expected"; break;
			case 18: s = "\"starting\" expected"; break;
			case 19: s = "\"beginning\" expected"; break;
			case 20: s = "\"ending\" expected"; break;
			case 21: s = "\"name\" expected"; break;
			case 22: s = "\"description\" expected"; break;
			case 23: s = "\"price\" expected"; break;
			case 24: s = "\"change\" expected"; break;
			case 25: s = "\"high\" expected"; break;
			case 26: s = "\"low\" expected"; break;
			case 27: s = "\"alch\" expected"; break;
			case 28: s = "\"value\" expected"; break;
			case 29: s = "\"items\" expected"; break;
			case 30: s = "\"item\" expected"; break;
			case 31: s = "\"a\" expected"; break;
			case 32: s = "\"now\" expected"; break;
			case 33: s = "\"today\" expected"; break;
			case 34: s = "\"on\" expected"; break;
			case 35: s = "\"in\" expected"; break;
			case 36: s = "\":\" expected"; break;
			case 37: s = "\"T\" expected"; break;
			case 38: s = "\"h\" expected"; break;
			case 39: s = "\"ours\" expected"; break;
			case 40: s = "\"d\" expected"; break;
			case 41: s = "\"ay\" expected"; break;
			case 42: s = "\"s\" expected"; break;
			case 43: s = "\"mon\" expected"; break;
			case 44: s = "\"ths\" expected"; break;
			case 45: s = "??? expected"; break;
			case 46: s = "invalid GeneralSelector"; break;
			case 47: s = "invalid ItemProp"; break;
			case 48: s = "invalid DateExpr"; break;
			case 49: s = "invalid FilterConcatOp"; break;
			case 50: s = "invalid RelOp"; break;
			case 51: s = "invalid SymbolicRelOp"; break;
			case 52: s = "invalid StringRelOp"; break;
			case 53: s = "invalid Value"; break;
			case 54: s = "invalid TimeUnit"; break;

			default: s = "error " + n; break;
		}
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}

	public virtual void SemErr (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}
	
	public virtual void SemErr (string s) {
		errorStream.WriteLine(s);
		count++;
	}
	
	public virtual void Warning (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
	}
	
	public virtual void Warning(string s) {
		errorStream.WriteLine(s);
	}
} // Errors


public class FatalError: Exception {
	public FatalError(string m): base(m) {}
}
