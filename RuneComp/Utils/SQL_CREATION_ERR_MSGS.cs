﻿namespace RuneComp.Utils
{
    public static class SQL_CREATION_ERR_MSGS
    {
        public static readonly string DUPLICATE_PROPERTY = "Cannot add duplicate property: {0}";
        public static readonly string EMPTY_PROP_NAME = "Cannot add property with no name !";
        public static readonly string NO_MAPPING = "No mapping found for property {0}";
        public static readonly string NO_TABLE_MAPPING = "No mapping found for given table name: {0}";
        public static readonly string ERR_ADD_PROP = "Cannot add property {0} to query";
        public static readonly string ERR_ADD_PROP_RES = "Cannot add property {0} to final result query";
        public static readonly string INVALID_FILTER_CONCAT_OP = "No valid sql representation found for given filterConcatOp {0}";
        public static readonly string INVALID_FILTER_PROP = "Cannot add filter for non existing property";
        public static readonly string NO_FILTER_CONDITION = "Cannot add filter with no condition";
        public static readonly string FILTER_TYPE_MISSMATCH = "Cannot apply filter (IsNumeric: {0}) for property {1} (IsNumeric: {2})";
        public static readonly string FILTER_NUM_OP_ON_STRING = "Cannot apply numerical operator on string property";
        public static readonly string FILTER_STRING_OP_ON_NUM = "Cannot apply string operator on numerical property";
        public static readonly string NO_MAPPING_FOR_OP = "No mapping for defined filter operation {0}";
        public static readonly string EMPTY_STRING = "String filter value cannot be empty";
        public static readonly string MISSPLACED_ENDING_WITH_WILDCARD = "Beginning wild card {0} only allowed at start of string for ENDING WITH operation";
        public static readonly string MISSPLACED_STARTING_WITH_WILDCARD = "Ending wild card {0} only allowed at end of string for STARTING / BEGINNING WITH operation";
        public static readonly string MORE_THAN_ONE_WILDCARD = "STARTING / BEGINNING / ENDING operations can contain at most one wildcard";
        public static readonly string TOO_MANY_WILDCARDS = "Like operation allows only the pattern *<text>* and <text>*<text>";
        public static readonly string INVALID_STRING_VALUE_FOR_OP = "Invalid string value {0} for defined operation {1}";
        public static readonly string INVALID_PRED_DATE_PAST = "Cannot make predictions for the past";
        public static readonly string INVALID_TIME_DIFF = "Can only make predictions for dates atleast 1h in the future of the current time, check inputs for prop {0}";
    }
}
