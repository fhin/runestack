﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Runnable
{
    public class Utils
    {
        private static char propIdent = '\u251C';
        private static char other = '\u2514';

        public static string Dump(object obj)
        {
            return Dump(obj, 0, 0);
        }

        private static string Dump(object obj, int identLevel, int printLevel)
        {
            if (obj == null) return " null\n";
            var objType = obj.GetType();
            StringBuilder sb = new StringBuilder();
            if (printLevel <= 1)
            {
                sb.Append('\t', identLevel);
                sb.Append(objType.Name);
            }
            sb.Append('\n');

            var fieldInfos = objType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            for (var i = 0; i < fieldInfos.Length; i++)
            {
                var fieldType = fieldInfos[i].PropertyType;
                sb.Append('\t', identLevel + 1);
                sb.Append(i + 1 == fieldInfos.Length ? other : propIdent);
                sb.Append(" " + fieldInfos[i].Name);

                if (IsNotPrimitiveOrStringOrEnum(fieldType))
                {
                    if (IsTypeEnumerable(fieldType))
                    {
                        sb.Append("<Type: " + GetEnumerableTypeName(fieldType) + ">:");
                        var listEntries = (IEnumerable)fieldInfos[i].GetValue(obj, null);
                        if (listEntries != null)
                        {
                            sb.Append("\t[\n");
                            foreach (var entry in listEntries)
                            {
                                sb.Append(Dump(entry, identLevel + 2, printLevel + 1));
                            }
                            sb.Append('\t', identLevel + 1);
                            sb.Append("  ]\n");
                        }
                    }
                    else
                    {
                        sb.Append("<Type: " + fieldType.Name + ">:");
                        sb.Append(Dump(fieldInfos[i].GetValue(obj), identLevel + 1, printLevel + 1));
                    }
                }
                else
                {
                    sb.Append("<Type: " + fieldType.Name + ">:");
                    sb.Append(" " + PrintPrimitiveValue(fieldInfos[i].GetValue(obj), fieldType));
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }

        private static bool IsTypeEnumerable(Type type)
        {
            if (type == null) return false;
            return (type.IsGenericType && type.GetInterface(typeof(IEnumerable<>).FullName) != null);
        }

        private static bool IsNotPrimitiveOrStringOrEnum(Type type)
        {
            if (type == null) return true;
            return !type.IsPrimitive && !type.IsEnum && (type != typeof(DateTime)) && (type != typeof(string));
        }

        private static string PrintPrimitiveValue(object value, Type type)
        {
            if (value == null || type == null || IsNotPrimitiveOrStringOrEnum(type)) return "";
            if (type == typeof(string))
            {
                return '"' + value.ToString() + '"';
            }
            else
            {
                return value.ToString();
            }
        }

        private static string GetEnumerableTypeName(Type type)
        {
            if (type == null) return "NULL";
            if (IsTypeEnumerable(type))
            {
                if (type.IsArray)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(type.GetElementType().Name);
                    for (var i = 0; i < type.GetArrayRank(); i++)
                    {
                        sb.Append("[]");
                    }
                    return sb.ToString();
                }
                else
                {
                    try
                    {
                        return "IEnumerable<" + type.GetGenericArguments()[0].Name + ">";
                    }
                    catch (Exception)
                    {
                        return "NO_TYPE_FOUND";
                    }
                }
            }
            return "NO_TYPE_FOUND";
        }
    }
}
