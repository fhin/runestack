﻿using System;
using System.Collections.Generic;

namespace RuneComp
{
    public class RuneSqlQuery
    {
        private Dictionary<string, (Type, object)> _queryParams;

        public string SqlQuery { get; set; }
        public int ExpectedQueryParams
        {
            get
            {
                return QueryParams != null ? QueryParams.Count : 0;
            }
        }
        public Dictionary<string, (Type, object)> QueryParams
        {
            get { return _queryParams; }
            private set
            {
                if (value != null)
                {
                    _queryParams = value;
                }
                else
                {
                    _queryParams.Clear();
                }
            }
        }
        public bool IsPrediction { get; private set; }
        public Property SubstitutedProperty { get; private set; }
        public IList<Prediction> SubstitutedPropertyPredictions { get; set; }
        public ISet<string> PropertiesToFetch { get; set; }

        public RuneSqlQuery()
        {
            _queryParams = new Dictionary<string, (Type, object)>();
            QueryParams = new Dictionary<string, (Type, object)>();
            PropertiesToFetch = new HashSet<string>();
            SubstitutedPropertyPredictions = new List<Prediction>();
            SqlQuery = "";
        }

        public RuneSqlQuery(Property substitutedProp)
            : this()
        {
            if (substitutedProp.Prediction != null && string.IsNullOrWhiteSpace(substitutedProp.PropertyName))
            {
                throw new ArgumentException("Cannot add query for prediction with no substituted property name !");
            }
            SubstitutedProperty = substitutedProp;
            IsPrediction = true;
        }

        public string GetPlainQuery()
        {
            return SqlQuery;
        }

        public bool AddPropertytoFetch(Property prop)
        {
            if (prop == null) return false;
            if (PropertiesToFetch == null) PropertiesToFetch = new HashSet<string>();
            string propStringRepresentation = prop.ToString();
            if (string.IsNullOrWhiteSpace(propStringRepresentation) || PropertiesToFetch.Contains(propStringRepresentation))
            {
                return false;
            }
            PropertiesToFetch.Add(propStringRepresentation);
            return true;
        }

        public bool AddQueryParam(string paramName, Type type, object value)
        {
            if (string.IsNullOrWhiteSpace(paramName) || type == null || value == null)
            {
                throw new ArgumentException("Parameter name, type and value must not be null !");
            }
            if (!QueryParams.ContainsKey(paramName))
            {
                QueryParams.Add(paramName, (type, value));
            }
            return true;
        }

        public void UpdateQueryParam(string paramName, object value)
        {
            if (string.IsNullOrWhiteSpace(paramName) || value == null)
            {
                throw new ArgumentException("Could not update query parameter as parameter name and value must not be null !");
            }
            if (!QueryParams.ContainsKey(paramName))
            {
                throw new ArgumentException("Could not update query parameter because the query has no defined parameter with name " + paramName);
            }
            QueryParams[paramName] = (QueryParams[paramName].Item1, value);
        }

        public void WriteToConsole()
        {
            Console.WriteLine("PLAIN SQL QUERY: " + GetPlainQuery());
            Console.Write("PROPERTIES TO FETCH: ");
            if (PropertiesToFetch.Count == 0)
            {
                Console.Write("*");
            }
            IEnumerator<string> propEnumerator = PropertiesToFetch.GetEnumerator();
            while (propEnumerator.MoveNext())
            {
                Console.Write(propEnumerator.Current.ToString() + ",");

            }
            Console.WriteLine("");
            propEnumerator.Dispose();
            Console.Write("SUBSTITUTED PROPERTY: ");
            if (SubstitutedProperty == null || string.IsNullOrWhiteSpace(SubstitutedProperty.ToString()))
            {
                Console.WriteLine("<none>");
            }
            else
            {
                Console.WriteLine(SubstitutedProperty.ToString());
            }
            Console.WriteLine("IS PREDICTION: " + IsPrediction.ToString());
            if (QueryParams != null && QueryParams.Count > 0)
            {
                Console.WriteLine("QUERY PARAMETERS:");
                Console.WriteLine(string.Format("{0, -50}", "NAME") + " | " + string.Format("{0,-50}", " TYPE")
                    + " | " + string.Format("{0,50}", "VALUE"));
                foreach ((string queryParamName, (Type t, object value)) in QueryParams)
                {
                    Console.Write(string.Format("{0,-50}", queryParamName) + " | " + string.Format("{0,-50}", t.ToString()) + " | ");
                    if (t.IsPrimitive || t == typeof(string))
                    {
                        if (value != null)
                        {
                            Console.Write(string.Format("{0,50}", value.ToString()) + "\n");
                        }
                        else
                        {
                            Console.Write(string.Format("{0,50}", "<null>") + "\n");
                        }
                    }
                }
            }
            Console.WriteLine("\n");
        }
    }
}
