﻿using RuneComp.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace RuneComp
{
    public class SqlBuilder
    {
        private readonly Dictionary<string, string> _validParameterMappings;
        private readonly Dictionary<string, string> _tableMappingDict;
        private Dictionary<string, int> _filterPropNameEntries;
        private RuneSqlExecutionPlan _executionPlan;
        private string _dbSchemaName;
        private DateTime _referenceTime;
        private readonly char _dbWildcardCharacter;
        private readonly char _parserWildcardCharacter = '*';

        public SqlBuilder(string dbSchemaName, Dictionary<string, string> validParameterMappings,
            Dictionary<string, string> tableMappingDict, char dbWildcardCharacter)
        {
            if (dbSchemaName == null || validParameterMappings == null || tableMappingDict == null)
            {
                throw new ArgumentException("Cannot create sql builder, inputs cannot be null !");
            }
            _dbSchemaName = dbSchemaName;
            _validParameterMappings = validParameterMappings;
            _tableMappingDict = tableMappingDict;
            _executionPlan = new RuneSqlExecutionPlan();
            _referenceTime = DateTime.Now;
            _filterPropNameEntries = new Dictionary<string, int>();
            _dbWildcardCharacter = dbWildcardCharacter;
        }

        public void SetDBSchema(string schemaName)
        {
            _dbSchemaName = schemaName ?? throw new ArgumentException("");
        }

        public void ClearTableMappings()
        {
            if (_tableMappingDict == null) return;
            _tableMappingDict.Clear();
        }

        public RuneSqlExecutionPlan BuildExecutionPlanFromQuery(Query query)
        {
            ClearFilterPropNames();
            _referenceTime = DateTime.Now;
            if (query == null) throw new ArgumentException("");
            try
            {
                string itemIdMapping = _validParameterMappings["item id"];
                if (_executionPlan != null)
                {
                    _executionPlan = new RuneSqlExecutionPlan();
                }
                RuneSqlQuery sqlQuery = new RuneSqlQuery();
                StringBuilder sqlStmt = new StringBuilder();
                sqlStmt.Append(QueryOpToSql(query.QueryOp));

                string nonPredictiveProps = AppendQueryProperties(query.Props, ref sqlQuery);
                sqlStmt.Append(nonPredictiveProps);

                string itemTableName = "";
                if (!_tableMappingDict.TryGetValue("items", out itemTableName))
                {
                    // TODO
                    throw new ArgumentException("");
                }
                string tableMapping = " from ";
                if (!string.IsNullOrWhiteSpace(_dbSchemaName))
                {
                    tableMapping += _dbSchemaName + ".";
                }
                tableMapping += itemTableName;
                sqlStmt.Append(tableMapping);

                sqlStmt.Append(AppendQueryFiltersToStmt(query.Filters, ref sqlQuery));
                sqlQuery.SqlQuery = sqlStmt.ToString();

                /* Preselection is needed the user query makes both predictions and defines filters for the items
                 * and will create a query fetching the ids of all items matching the defined filters.
                 * All subsequent queries will select the properties and predictions of the items as defined in the user query.
                 * The ids of all relevant items have been fetch by the preselection query.
                 * 
                 * No preselection is required if:
                 * 
                 * I. A query with no filters and only predictions is defined
                 */
                bool isPreSelectionRequired = !(_executionPlan.Queries.Count > 0 && nonPredictiveProps.Equals(itemIdMapping)
                    && query.Filters.Count == 0);
                Type preselectedItemIdType = typeof(List<int>);

                for (int i = 0; i < _executionPlan.Queries.Count; i++)
                {
                    _executionPlan.Queries[i].SqlQuery = string.Format(_executionPlan.Queries[i].SqlQuery,
                        !string.IsNullOrWhiteSpace(_dbSchemaName) ? _dbSchemaName + "." : "");
                    if (isPreSelectionRequired)
                    {
                        // Add preselected item ids filter to queries selecting properties and predictions
                        _executionPlan.Queries[i].SqlQuery += " and " + itemIdMapping + " = any(@" + itemIdMapping + ")";
                        _executionPlan.Queries[i].AddQueryParam(itemIdMapping, preselectedItemIdType, new List<int>());
                    }
                }
                if (isPreSelectionRequired)
                {
                    // Add preselection query
                    _executionPlan.Queries.Insert(0, sqlQuery);
                }
                return _executionPlan;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ArgumentException(e.Message);
            }
        }

        private void ClearFilterPropNames()
        {
            if (_filterPropNameEntries != null)
            {
                _filterPropNameEntries.Clear();
            }
        }

        private string AppendQueryProperties(List<Property> props, ref RuneSqlQuery query)
        {
            if (props == null || props.Count == 0) return "*";
            string propString = "";
            bool hasPrediction = false;
            IDictionary<string, RuneSqlQuery> predictionQueries = new Dictionary<string, RuneSqlQuery>();

            if (!_validParameterMappings.ContainsKey("icon"))
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.ERR_ADD_PROP, "icon");
            }
            // Add item icon property by default
            Property itemIconProp = new Property()
            {
                IsNumeric = false,
                PropertyName = _validParameterMappings["icon"]
            };
            query.AddPropertytoFetch(itemIconProp);
            if (!_executionPlan.AddPropertyForResult(itemIconProp.ToString()))
            {
                throw new ArgumentException("Cannot add property to fetch for query !");
            }

            foreach (Property prop in props)
            {
                if (!IsPropertyValid(prop)) break;

                string mappedPropName = _validParameterMappings[prop.PropertyName];
                if (prop.Prediction != null)
                {
                    hasPrediction = true;
                    if (!_validParameterMappings.ContainsKey("item id"))
                    {
                        throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_MAPPING, "item id"));
                    }
                    if (!predictionQueries.ContainsKey(prop.PropertyName))
                    {
                        RuneSqlQuery predictionQuery = CreateQueryForPrediction(prop);
                        predictionQuery.SubstitutedPropertyPredictions.Add(prop.Prediction);
                        predictionQueries.Add(prop.PropertyName, predictionQuery);
                    }
                    else
                    {
                        predictionQueries[prop.PropertyName].SubstitutedPropertyPredictions.Add(prop.Prediction);
                    }
                }
                else
                {
                    propString += mappedPropName + ",";
                    prop.PropertyName = mappedPropName;
                    if (!query.AddPropertytoFetch(prop))
                    {
                        throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.ERR_ADD_PROP, prop));
                    }
                }
                prop.PropertyName = mappedPropName;
                if (!_executionPlan.AddPropertyForResult(prop.ToString()))
                {
                    throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.ERR_ADD_PROP_RES, prop));
                }
            }
            if (hasPrediction)
            {
                foreach (RuneSqlQuery predicitonQuery in predictionQueries.Values)
                {
                    _executionPlan.AddQuery(predicitonQuery);
                }

                propString = _validParameterMappings["item id"] + "," + propString;
                Property itemIdProp = new Property()
                {
                    IsNumeric = true,
                    PropertyName = _validParameterMappings["item id"]
                };
                query.AddPropertytoFetch(itemIdProp);
            }
            // Add icon property to final sql statement
            propString = _validParameterMappings["icon"] + "," + propString;

            if (propString.Length > 0 && propString[propString.Length - 1].Equals(','))
            {
                propString = propString.Remove(propString.Length - 1, 1); // Remove last comma
            }
            return propString;
        }

        private bool IsPropertyValid(Property prop)
        {
            if (prop == null || string.IsNullOrWhiteSpace(prop.PropertyName))
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.EMPTY_PROP_NAME);
            }
            if (!_validParameterMappings.TryGetValue(prop.PropertyName, out _))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_MAPPING, prop));
            }
            if (_executionPlan.ResultProps.Contains(prop.ToString()))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.DUPLICATE_PROPERTY, prop));
            }
            return true;
        }

        private string AppendQueryFiltersToStmt(List<Filter> filters, ref RuneSqlQuery query)
        {
            if (filters == null || filters.Count == 0) return "";
            string filterStmt = " where";
            for (int i = 0; i < filters.Count; i++)
            {
                filterStmt += " " + FilterToSqlClause(filters[i], ref query);
                if (i + 1 < filters.Count)
                {
                    filterStmt += " " + AddFilterConcatOp(filters[i].FilterConcatOp);
                }
            }
            return filterStmt;
        }

        private string AddFilterConcatOp(FilterConcatinationOp filterConcatOp)
        {
            switch (filterConcatOp)
            {
                case FilterConcatinationOp.And:
                    return "and";
                case FilterConcatinationOp.Or:
                    return "or";
                default:
                    throw new ArgumentException(SQL_CREATION_ERR_MSGS.INVALID_FILTER_CONCAT_OP, filterConcatOp.ToString());
            }
        }

        private string FilterToSqlClause(Filter filter, ref RuneSqlQuery query)
        {
            if (filter.Property == null)
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.INVALID_FILTER_PROP);
            }
            if (string.IsNullOrWhiteSpace(filter.Property.PropertyName))
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.EMPTY_PROP_NAME);
            }
            if (filter.Condition == null)
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.NO_FILTER_CONDITION);
            }
            if (filter.Property.IsNumeric && filter.Condition.CondType == ConditionValType.String ||
                !filter.Property.IsNumeric && filter.Condition.CondType != ConditionValType.String)
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.FILTER_TYPE_MISSMATCH, 
                    new object[] { filter.Condition.CondType == ConditionValType.Integer || filter.Condition.CondType == ConditionValType.Double,
                    filter.Property, filter.Property.IsNumeric}));
            }
            if (!_validParameterMappings.ContainsKey(filter.Property.PropertyName))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_MAPPING, filter.Property.PropertyName));
            }
            return _validParameterMappings[filter.Property.PropertyName] + ConditionToSqlClause(filter.Property, filter.Condition, ref query);
        }

        private string OperationToSqlOp(Operation op, ConditionValType valType)
        {
            if (Enums.IsOperationNumeric(op))
            {
                if (op != Operation.Equal && (valType == ConditionValType.String || valType == ConditionValType.None))
                {
                    throw new ArgumentException(SQL_CREATION_ERR_MSGS.FILTER_NUM_OP_ON_STRING);
                }
            }
            if (!Enums.IsOperationNumeric(op) && (valType != ConditionValType.String))
            {
                throw new ArgumentException(SQL_CREATION_ERR_MSGS.FILTER_STRING_OP_ON_NUM);
            }
            string sqlOp;
            switch (op)
            {
                case Operation.Equal:
                    sqlOp = "=";
                    break;
                case Operation.Greater:
                    sqlOp = ">";
                    break;
                case Operation.Greater_Eq:
                    sqlOp = ">=";
                    break;
                case Operation.Smaller:
                    sqlOp = "<";
                    break;
                case Operation.Smaller_Eq:
                    sqlOp = "<=";
                    break;
                case Operation.Like:
                case Operation.Starting_With:
                case Operation.Ending_With:
                    sqlOp = "like";
                    break;
                default:
                    throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_MAPPING_FOR_OP, op.ToString()));
            }
            sqlOp = " " + sqlOp + " ";
            return sqlOp;
        }

        private string QueryOpToSql(QueryOp op)
        {
            if (op == QueryOp.show)
            {
                return "select ";
            }
            else
            {
                throw new ArgumentException("");
            }
        }

        private string ConditionToSqlClause(Property prop, Condition c, ref RuneSqlQuery query)
        {
            if (c == null || prop == null) throw new ArgumentException("Property and Condition cannot be null !");
            if (string.IsNullOrWhiteSpace(prop.PropertyName)) throw new ArgumentException(SQL_CREATION_ERR_MSGS.EMPTY_PROP_NAME);
            if (!_validParameterMappings.ContainsKey(prop.PropertyName))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_MAPPING, prop.PropertyName));
            }
            // Build parameter for prepared statement for given filter:
            // Parameter name will be built with the following scheme:
            // @[propertyName]_[curr_num_occurence_prop_in_filters]
            // e.g. Prop: name , Filter: like, "Sword*" -> @name_0 like "Sword*"

            string propNameBackup = prop.PropertyName;
            prop.PropertyName = _validParameterMappings[prop.PropertyName];
            string propStringRepresentation = prop.ToString();
            // Count occurences of given filter property
            if (!_filterPropNameEntries.ContainsKey(propStringRepresentation))
            {
                _filterPropNameEntries.Add(propStringRepresentation, 0);
            }
            propStringRepresentation += "_" + _filterPropNameEntries[propStringRepresentation]++;
            // Build parameter name
            string sqlClause = OperationToSqlOp(c.Op, c.CondType) + "@" + propStringRepresentation;

            Type conditionValType;
            object conditionVal;
            switch (c.CondType)
            {
                case ConditionValType.Integer:
                    conditionVal = c.IVal;
                    conditionValType = typeof(int);
                    break;
                case ConditionValType.Double:
                    conditionVal = c.DVal;
                    conditionValType = typeof(double);
                    break;
                case ConditionValType.String:
                    // Replace parser wildcard with database wildcards
                    c.SVal = c.SVal.Replace(_parserWildcardCharacter, _dbWildcardCharacter);
                    conditionVal = ValidateStringValue(c.SVal, c.Op);
                    conditionValType = typeof(string);
                    break;
                default:
                    throw new ArgumentException("Cannot add value for condition with no type defined");
            }
            if (conditionVal == null) throw new ArgumentException("Cannot add value of condition for query !");
            // Add parameter, type and given filter value
            query.AddQueryParam(propStringRepresentation, conditionValType, conditionVal);
            prop.PropertyName = propNameBackup;
            return sqlClause;
        }

        private bool IsValidStringValue(string sVal, Operation op)
        {
            if (string.IsNullOrWhiteSpace(sVal)) throw new ArgumentException("A string value cannot be empty !");

            int startWildcardIdx = sVal.IndexOf(_dbWildcardCharacter);
            if (startWildcardIdx != -1)
            {
                if ((op == Operation.Ending_With && startWildcardIdx != 0) || op == Operation.Equal)
                {
                    throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.MISSPLACED_ENDING_WITH_WILDCARD, _parserWildcardCharacter.ToString()));
                }
            }
            int endWildcardIdx = sVal.LastIndexOf(_dbWildcardCharacter);
            if (endWildcardIdx != -1)
            {
                if ((op == Operation.Starting_With && endWildcardIdx != sVal.Length - 1) || op == Operation.Equal)
                {
                    throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.MISSPLACED_STARTING_WITH_WILDCARD, _parserWildcardCharacter.ToString()));
                }
                if (startWildcardIdx != -1)
                {
                    if (endWildcardIdx == startWildcardIdx)
                    {
                        /*
                            valid patterns: 
                                *<text> for STARTING / BEGINNING op
                                <text>* for ENDING op
                                text with one wildcard for LIKE op
                        */
                        return true;
                    }
                    else
                    {
                        if (op != Operation.Like)
                        {
                            throw new ArgumentException(SQL_CREATION_ERR_MSGS.MORE_THAN_ONE_WILDCARD);
                        }
                        int numSymToCheck = endWildcardIdx - 1 - (startWildcardIdx + 1);
                        int otherWildcardIdx = sVal.IndexOf(_dbWildcardCharacter, startWildcardIdx + 1, numSymToCheck);
                        if (startWildcardIdx == 0 && endWildcardIdx == sVal.Length - 1 && otherWildcardIdx == -1)
                        {
                            return true; // Valid pattern *<text>* for LIKE operation
                        }
                        else
                        {
                            throw new ArgumentException(SQL_CREATION_ERR_MSGS.TOO_MANY_WILDCARDS);
                        }
                    }
                }
            }
            return true;
        }

        private string ValidateStringValue(string sVal, Operation op)
        {
            if (!IsValidStringValue(sVal, op))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.INVALID_STRING_VALUE_FOR_OP, new object[]
                {
                    sVal, op
                }));
            }
            int startWildcardIdx = sVal.IndexOf(_dbWildcardCharacter);
            int endWildcardIdx = sVal.LastIndexOf(_dbWildcardCharacter);
            // Append wildcards to string if they are missing
            if (startWildcardIdx == -1 && endWildcardIdx == -1)
            {
                if (op == Operation.Starting_With || op == Operation.Like)
                {
                    sVal = sVal.Insert(sVal.Length, _dbWildcardCharacter.ToString());
                }
                if (op == Operation.Ending_With || op == Operation.Like)
                {
                    sVal = sVal.Insert(0, _dbWildcardCharacter.ToString());
                }
            }
            if (sVal.Contains('_'))
            {
                sVal = sVal.Replace("_", " ");
            }
            return sVal;
        }

        private RuneSqlQuery CreateQueryForPrediction(Property prop)
        {
            if (prop == null) return null;
            DateTime dateToCmp = DateTime.Now;

            int tVal = prop.Prediction.TValue;
            if (tVal < 0) throw new ArgumentException(SQL_CREATION_ERR_MSGS.INVALID_PRED_DATE_PAST);
            switch (prop.Prediction.TUnit)
            {
                case PredictionTimeUnit.Hour:
                    dateToCmp = dateToCmp.AddHours(tVal);
                    break;
                case PredictionTimeUnit.Day:
                    dateToCmp = dateToCmp.AddDays(tVal);
                    break;
                case PredictionTimeUnit.Month:
                    dateToCmp = dateToCmp.AddMonths(tVal);
                    break;
                default:
                    dateToCmp = prop.Prediction.FutureDate;
                    break;
            }

            // Prediction must be atleast 1h in the future
            if (Convert.ToInt32(Math.Ceiling(dateToCmp.Subtract(_referenceTime).TotalHours)) < 0)
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.INVALID_TIME_DIFF, prop.PropertyName));
            }
            if (!_tableMappingDict.ContainsKey(prop.PropertyName + " regression models"))
            {
                throw new ArgumentException(string.Format(SQL_CREATION_ERR_MSGS.NO_TABLE_MAPPING, prop.PropertyName + "_regression_models"));
            }

            Prediction pred = new Prediction()
            {
                FutureDate = prop.Prediction.FutureDate,
                PredTarget = prop.Prediction.PredTarget,
                TUnit = prop.Prediction.TUnit,
                TValue = prop.Prediction.TValue
            };
            prop.Prediction = null;
            RuneSqlQuery query = new RuneSqlQuery(prop)
            {
                SqlQuery = "select * from {0}regression_models where predictable_property_id = @predictable_property_id"
            };
            query.QueryParams.Add("predictable_property_id", (typeof(int), -1));
            prop.Prediction = pred;
            return query;
        }
    }
}
