﻿using System.Collections.Generic;

namespace RuneComp
{
    public class RuneSqlExecutionPlan
    {
        public List<RuneSqlQuery> Queries { get; private set; }
        bool HasPredictions { get; set; }
        public ISet<string> ResultProps { get; private set; }

        public RuneSqlExecutionPlan()
        {
            Queries = new List<RuneSqlQuery>();
            HasPredictions = false;
            ResultProps = new HashSet<string>();
        }

        public void AddQuery(RuneSqlQuery query)
        {
            if (query == null) return;
            if (query.IsPrediction)
            {
                HasPredictions = true;
            }
            Queries.Add(query);
        }

        public bool AddPropertyForResult(string propStringRepresentation)
        {
            if (string.IsNullOrWhiteSpace(propStringRepresentation)) return false;
            if (ResultProps == null)
            {
                ResultProps = new HashSet<string>();
            }
            if (ResultProps.Contains(propStringRepresentation))
            {
                return false;
            }
            ResultProps.Add(propStringRepresentation);
            return true;
        }
    }
}
