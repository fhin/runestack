﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RuneComp
{
    public class UserQueryParser
    {
        public string UserQueryString { get; private set; }
        public Query UserQuery { get; private set; }
        public bool IsValidQuery { get; private set; }
        public IList<QueryError> Errors { get; private set; }

        public UserQueryParser()
        {
            UserQueryString = "";
            IsValidQuery = false;
            Errors = new List<QueryError>();
        }

        public void ParseQuery(string queryString)
        {
            if (string.IsNullOrWhiteSpace(queryString)) throw new ArgumentException("");
            try
            {
                UserQueryString = queryString;
                using (var stream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(UserQueryString);
                        writer.Flush();
                        stream.Position = 0;

                        Scanner s = new Scanner(stream);

                        using(var parserOutputRedirect = new ParserOutputRedirect())
                        {
                            Console.SetOut(parserOutputRedirect);
                            Parser p = new Parser(s);
                            p.Parse();
                            Errors = parserOutputRedirect.QueryErrors;
                            UserQuery = p.query;
                        }

                        if (Errors.Count == 0)
                        {
                            IsValidQuery = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Recover the standard output stream so that a 
                // completion message can be displayed.
                var standardOutput = new StreamWriter(Console.OpenStandardOutput())
                {
                    AutoFlush = true
                };
                Console.SetOut(standardOutput);

                Console.WriteLine("Error parsing query for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                IsValidQuery = false;
            }
            finally
            {
                // Recover the standard output stream so that a 
                // completion message can be displayed.
                var standardOutput = new StreamWriter(Console.OpenStandardOutput())
                {
                    AutoFlush = true
                };
                Console.SetOut(standardOutput);
            }
        }
    }
}
