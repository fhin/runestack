﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RuneComp;
using RuneDB.Services;
using RuneStackWebApi.Models;

namespace RuneStackWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private static readonly Dictionary<string, string> _propDict = new Dictionary<string, string>()
        {
            { "item id", "item_id" },
            { "name", "item_name" },
            { "description", "description" },
            { "price", "curr_ge_price" },
            { "price change", "curr_price_change" },
            { "high alch value", "high_alch_val" },
            { "low alch value", "low_alch_val" },
            { "const price", "const_price" },
            { "buying limit", "buying_limit" },
            { "icon", "icon" }
        };

        private static readonly Dictionary<string, string> _tableMapping = new Dictionary<string, string>()
        {
            { "items", "items" },
            { "price regression models", "price_regression_models" }
        };

        private SqlBuilder sqlBuilder = new SqlBuilder("runeSchema", _propDict, _tableMapping, '%');
        private IRuneDBService _dbService;

        public QueryController(IRuneDBService dbService)
        {
            _dbService = dbService;
        }

        [HttpGet]
        public string Get()
        {
            return "Hallo";
        }

        // https://localhost:44351/api/query/queryRes/{query}
        // https://localhost:44351/api/query/queryRes/show%20name%20and%20price%20of%20items
        // https://localhost:44351/api/query/queryRes/show%20name%20and%20price%20of%20items%20with%20a%20name%20like%20%22Pirate*%22
        [HttpGet("queryRes/{query}")]
        public async Task<ActionResult<QueryResult>> GetQueryResult(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return BadRequest();
            }
            try
            {
                UserQueryParser parser = new UserQueryParser();
                parser.ParseQuery(query);
                if (!parser.IsValidQuery)
                {
                    return BadRequest(new QueryResult()
                    {
                        Errors = parser.Errors
                    }
                    );
                }
                QueryResult result = await FetchQueryResults(parser.UserQuery);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private Task<QueryResult> FetchQueryResults(Query query)
        {
            return Task.Run(() =>
            {
                if (query == null)
                {
                    throw new ArgumentException("Cannot etch query results for invalid query");
                }
                RuneSqlExecutionPlan plan = sqlBuilder.BuildExecutionPlanFromQuery(query);
                (var propDict, var results) = _dbService.RunExecutionPlan(plan);
                int iconColumnIdx = propDict[_propDict["icon"]];
                QueryResult qResult = new QueryResult(propDict.Count, results.Count);

                foreach (KeyValuePair<string, int> kvPair in propDict)
                {
                    qResult.Columns[propDict[kvPair.Key]] = kvPair.Key;
                }
                for (int i = 0; i < results.Count; i++)
                {
                    string[] resultRowArray = new string[propDict.Count];
                    for (int currCol = 0; currCol < propDict.Count; currCol++)
                    {
                        if (currCol == iconColumnIdx)
                        {
                            resultRowArray[currCol] = Convert.ToBase64String((byte[])results[i][currCol]);
                        }
                        else
                        {
                            resultRowArray[currCol] = results[i][currCol].ToString();
                        }
                    }
                    qResult.ResultTable[i] = resultRowArray;
                }
                return qResult;
            });
        }
    }
}