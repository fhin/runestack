using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RuneDB;
using RuneDB.Services;

namespace RuneStackWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            HttpRequestFactory requestFactory = HttpRequestFactory.GetInstance;
            services.AddSingleton(requestFactory);

           // services.AddSingleton<IRuneDataService, RuneHttpDataService>();
            services.AddHttpClient<IRuneDataService, RuneHttpDataService>();

            services.AddSingleton<IRuneContextFactory>(x => new RuneContextFactory(
                new DbContextOptionsBuilder<RuneDBContext>().UseNpgsql(Configuration.GetSection("ConnectionString").Value)));

            services.AddTransient<IRuneDBService, RuneDbService>();
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowLocalHostPolicy", builder =>
                {
                    builder.WithOrigins("https://localhost:44344").AllowAnyMethod().AllowAnyHeader();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseCors("AllowLocalHostPolicy");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
