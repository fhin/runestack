﻿using RuneComp;
using System.Collections.Generic;

public enum QueryResultTypes
{
    SUCCESS,
    ERROR,
    NONE
}

namespace RuneStackWebApi.Models
{
    public class QueryResult
    {
        private IEnumerable<QueryError> _errors;
        public QueryResultTypes QueryResultType { get; set; }
        public IEnumerable<QueryError> Errors
        {
            get { return _errors; }
            set
            {
                if (value == null)
                {
                    _errors = new List<QueryError>();
                }
                else
                {
                    _errors = value;
                }
                QueryResultType = QueryResultTypes.ERROR;
            }
        }
        public string[] Columns { get; set; }
        public string[][] ResultTable { get; set; }

        public QueryResult()
        {
            QueryResultType = QueryResultTypes.NONE;
            Errors = new List<QueryError>();
        }

        public QueryResult(int numCols, int numRows)
            : this()
        {
            Columns = new string[numCols];
            ResultTable = new string[numRows][];
            QueryResultType = QueryResultTypes.SUCCESS;
        }
    }
}
