﻿using System;
using System.IO;
using System.Text;

namespace Runnable
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter your query: ");
            string query = Console.ReadLine();
            SqlBuilder builder = new SqlBuilder(query);
            /*
            string query = "show name and price of items with a high alch value = 1000 and a price <= 100 and a price > 20";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(query);
            writer.Flush();
            stream.Position = 0;
            //Buffer b = new Buffer(stream, true);
            Scanner s = new Scanner(stream);
            Parser p = new Parser(s);
            p.Parse();
            Query q = p.query;
            */

            /*
            if (args.Length > 0)
            {
                if (!File.Exists(args[0]))
                {
                    Console.WriteLine("File " + args[0] + " does not exist");
                    Console.WriteLine("\n Press any key to stop...");
                    Console.ReadKey();
                    return;
                }

                int dumpTreeIdx = FindArg("-t", args);
                string outputFilePath = "";
                if (dumpTreeIdx != -1 && dumpTreeIdx < args.Length)
                {
                    if (dumpTreeIdx + 1 < args.Length)
                    {
                        outputFilePath = args[dumpTreeIdx + 1] != null ? args[dumpTreeIdx + 1] : "";
                    }
                    if (outputFilePath.Length > 0)
                    {
                        try
                        {
                            using (FileStream fs = File.Open(outputFilePath, FileMode.Open))
                            {
                                fs.SetLength(0); // Clear previous file contents
                            }
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("Error truncating outputfile for the reason below !");
                            Console.WriteLine(e.Message);
                        }
                    }
                    for(var i = 0; i < dumpTreeIdx; i++)
                    {
                        try
                        {   
                            ParseInput(args[i], outputFilePath);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error parsing input in file" + args[i]);
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("-- No source file/s specified");
                }
            }
            */
            Console.WriteLine("\n Press any key to stop...");
            Console.ReadKey();
        }

        /*
        private static int FindArg(string argName, string[] argArr)
        {
            if (string.IsNullOrEmpty(argName) || argArr.Length == 0) return -1;
            for (int i = 0; i < argArr.Length; i++)
            {
                if (argName.Equals(argArr[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        private static void ParseInput(string inputFilePath, string outputFilePath)
        {
            if (string.IsNullOrEmpty(inputFilePath)) return;
            if (string.IsNullOrEmpty(outputFilePath))
            {
                Console.WriteLine("Warning no output file specified !");
                return;
            }

            Scanner scanner = new Scanner(inputFilePath);
            Parser parser = new Parser(scanner);
            parser.Parse();
            if (parser.errors.count == 0)
            {
                if (File.Exists(outputFilePath))
                {
                    using (FileStream fs = File.Open(outputFilePath, FileMode.Append))
                    {
                        string treeDump = null;
                        try
                        {
                            treeDump = Utils.Dump(parser.query);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error creating tree structure for query !\n");
                            Console.WriteLine(e.Message);
                        }
                        if (treeDump != null)
                        {
                            byte[] info = new UTF8Encoding(true).GetBytes(treeDump);
                            fs.Write(info, 0, info.Length);
                            Console.WriteLine("Dumped tree structure into file: " + outputFilePath);
                        }
                    }
                }
                else
                {
                    using (FileStream fs = File.Create(outputFilePath))
                    {
                        string treeDump = null;
                        try
                        {
                            treeDump = Utils.Dump(parser.query);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error creating tree structure for query !\n");
                            Console.WriteLine(e.Message);
                        }
                        if (treeDump != null)
                        {
                            byte[] info = new UTF8Encoding(true).GetBytes(treeDump);
                            fs.Write(info, 0, info.Length);
                            Console.WriteLine("Dumped tree structure into file: " + outputFilePath);
                        }
                    }
                }
            }
        }
        */
    }
}
